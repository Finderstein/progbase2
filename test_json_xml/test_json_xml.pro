QT += core
QT -= gui
QT += xml

TARGET = test_json_xml
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    storagemanager.cpp \
    xmlcashierstorage.cpp \
    jsoncashierstorage.cpp

CONFIG += c++14

HEADERS += \
    cashier.h \
    cashierstorage.h \
    storagemanager.h \
    messageexception.h \
    xmlcashierstorage.h \
    jsoncashierstorage.h

