#include "storagemanager.h"
#include <messageexception.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>





CashierStorage * StorageManager::createStorage(std::string & storageFileName)
{
    int i = 0;
    std::string format;
    for(auto tmp : storageFileName)
    {
        if(tmp == '.')
        {
            format = storageFileName.substr(i);
        }
        i++;
    }

    if(format == ".xml")
    {
        XmlCashierStorage *xmlSt = new XmlCashierStorage(storageFileName);
        return xmlSt;
    }
    else if(format == ".json")
    {
        JsonCashierStorage *jsonSt = new JsonCashierStorage(storageFileName);
        return jsonSt;
    }
    else
    {
        std::string er = "Invalid format!  [" + storageFileName + "]";
        throw MessageException(er);
    }
}
