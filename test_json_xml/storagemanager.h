#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H
#include <cashier.h>
#include <cashierstorage.h>
#include <xmlcashierstorage.h>
#include <jsoncashierstorage.h>


class StorageManager
{
public:
    static CashierStorage * createStorage(std::string & storageFileName);
};
#endif // STORAGEMANAGER_H
