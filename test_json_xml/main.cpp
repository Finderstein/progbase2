#include <QDebug>
#include <QtXml>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>

#include <storagemanager.h>
#include <jsoncashierstorage.h>
#include <xmlcashierstorage.h>
#include <cashier.h>
#include <messageexception.h>


using namespace std;

void printCashier(Cashier & c) {
    cout << "cashier: " << c.name.toStdString() << endl << "workHours: " << c.workHours << endl << "salary: " << c.salary << endl << "workDays: " << endl;
    for (QString & d : c.workDays) {
          cout << "  day: " << d.toStdString() << endl;
    }
}

void printCashiers(vector<Cashier> & cs) {
    cout << "All cashiers: " << endl << endl;
    for (Cashier & c: cs) {
        printCashier(c);
        cout << endl;
    }
}

int main(void)
{
    vector<Cashier> cashiers;

    Cashier cashier;
    cashier.name = "David";
    cashier.workHours = 35;
    cashier.salary = 1335.35;
    cashier.workDays.push_back("Monday");
    cashier.workDays.push_back("Tuesday");
    cashier.workDays.push_back("Wednesday");
    cashier.workDays.push_back("Friday");


    cashiers.push_back(cashier);

    for(auto i = cashier.workDays.begin(); i != cashier.workDays.end();)
    {
        i = cashier.workDays.erase(i);
    }


    cashier.name = "Joe";
    cashier.workHours = 16;
    cashier.salary = 600.95;
    cashier.workDays.push_back("Monday");
    cashier.workDays.push_back("Wednesday");
    cashier.workDays.push_back("Friday");
    cashier.workDays.push_back("Saturday");

    cashiers.push_back(cashier);

    for(auto i = cashier.workDays.begin(); i != cashier.workDays.end();)
    {
        i = cashier.workDays.erase(i);
    }

    cashier.name = "Alex";
    cashier.workHours = 40;
    cashier.salary = 1600.00;
    cashier.workDays.push_back("Monday");
    cashier.workDays.push_back("Tuesday");
    cashier.workDays.push_back("Wednesday");
    cashier.workDays.push_back("Friday");
    cashier.workDays.push_back("Saturday");

    cashiers.push_back(cashier);

    for(auto i = cashier.workDays.begin(); i != cashier.workDays.end();)
    {
        i = cashier.workDays.erase(i);
    }

    cashier.name = "Mayhem";
    cashier.workHours = 32;
    cashier.salary = 1005.25;
    cashier.workDays.push_back("Monday");
    cashier.workDays.push_back("Tuesday");
    cashier.workDays.push_back("Friday");
    cashier.workDays.push_back("Saturday");

    cashiers.push_back(cashier);

    vector<CashierStorage *> cashierSt;

    string dataXml = "data_test.xml";
    string dataJson = "data_test.json";
    string dataFake = "data_test.fake";


    try
    {
        cashierSt.push_back(StorageManager::createStorage(dataXml));
    }
    catch(invalid_argument & error)
    {
        cerr << error.what() << endl;
    }

    try
    {
        cashierSt.push_back(StorageManager::createStorage(dataJson));
    }
    catch(invalid_argument & error)
    {
        cerr << error.what() << endl;
    }

    try
    {
        cashierSt.push_back(StorageManager::createStorage(dataFake));
    }
    catch(MessageException & error)
    {
        cerr << error.what() << endl;
    }

    for(auto *i : cashierSt)
    {
        try
        {
            i->save(cashiers);
        }
        catch(invalid_argument & error)
        {
            cerr << error.what();
        }
    }

    cout << "Please enter anything: " << endl;

    cin.get();



    for(CashierStorage *i : cashierSt)
    {
        vector<Cashier> newCashiers = i->load();
        cout << endl << "=====================================" << endl;
        cout << i->name() << endl;
        cout << "=====================================" << endl;
        printCashiers(newCashiers);
    }






    return 0;
}

