#include "xmlcashierstorage.h"
#include <messageexception.h>
#include <storagemanager.h>
#include <stdexcept>
#include <QDebug>
#include <QtXml>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>

using namespace std;

static int fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    fclose(f);
    return 1;  // true: exists
}

static long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

static int readFileToBuffer(const char *fileName, char *buffer, int bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  // number of bytes read
}

static int readBufferToFile(char *fileName, const char *buffer)
{
    FILE *f = fopen(fileName, "w");
    if (!f) return 0;  // read 0 bytes from file
    long writeBytes = fwrite(buffer, strlen(buffer), 1, f);
    fclose(f);
    return writeBytes;  // number of bytes read
}

void XmlCashierStorage::save(vector<Cashier> & entities) {
    //перевірити на наявність файлу...
    char *file = (char *)name().c_str();
    if(!fileExists(file))
    {
        throw std::invalid_argument("File not exists!!");
    }


    QDomDocument doc;
    QDomElement allCashiersEl = doc.createElement("All_cashiers");

    for (Cashier & c: entities) {
        QDomElement cashierEl = doc.createElement("cashier");
        cashierEl.setAttribute("name", c.name);
        cashierEl.setAttribute("workHours", c.workHours);
        cashierEl.setAttribute("salary", c.salary);

        for (const QString & d: c.workDays) {
              QDomElement workDaysEl = doc.createElement("workDay");
              workDaysEl.setAttribute("day", d);
              cashierEl.appendChild(workDaysEl);
        }

        allCashiersEl.appendChild(cashierEl);
    }

    doc.appendChild(allCashiersEl);

    QString xmlString = doc.toString();

    //зберегти xmlString в файл
    readBufferToFile(file, xmlString.toStdString().c_str());


}

vector<Cashier> XmlCashierStorage::load(void) {
    //перевірити на наявність файлу...
    char *file = (char *)name().c_str();
    if(!fileExists(file))
    {
        throw std::invalid_argument("File not exists!!");
    }

    long lenOfFile = getFileSize(file) + 1;
    char text[lenOfFile];
    int bytesRead = readFileToBuffer(file, &text[0], (int)lenOfFile);
    text[bytesRead] = '\0';

    string str = string(text);

    QDomDocument doc;

    if (!doc.setContent(QString::fromStdString(str))) {
        throw std::invalid_argument("Invalid text!!");
    }

    vector<Cashier> cs;

    QDomElement root = doc.documentElement();

    for (int i = 0; i < root.childNodes().length(); i++) {
        QDomNode cashierNode = root.childNodes().at(i);
        QDomElement cashiertEl = cashierNode.toElement();
        Cashier c;
        c.name = cashiertEl.attribute("name");
        c.workHours = cashiertEl.attribute("workHours").toInt();
        c.salary = cashiertEl.attribute("salary").toDouble();

        for (int j = 0; j < cashiertEl.childNodes().length(); j++) {
              QDomNode dayNode = cashiertEl.childNodes().at(j);
              QDomElement dayEl = dayNode.toElement();
              QString d;
              d = dayEl.attribute("day");
              c.workDays.push_back(d);
        }

        cs.push_back(c);
    }

    return cs;

}
