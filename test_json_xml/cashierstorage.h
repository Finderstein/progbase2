#ifndef CASHIERSTORAGE_H
#define CASHIERSTORAGE_H
#include <cashier.h>


class CashierStorage
{
    std::string _name;
protected:
    CashierStorage(std::string & name) { this->_name = name; }
public:
    std::string & name() { return this->_name; }

    virtual std::vector<Cashier> load() = 0;
    virtual void save(std::vector<Cashier> & entities) = 0;
};

#endif // CASHIERSTORAGE_H
