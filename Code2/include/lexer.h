#pragma once 

#include <list.h>

typedef enum 
{
    TokenType_LPAR,
    TokenType_RPAR,
    TokenType_NUMBER,
    TokenType_PLUS,
    TokenType_MINUS,
    TokenType_MULT,
    TokenType_DIV
} TokenType;

typedef struct
{
    TokenType type;
    char * lexeme;
} Token;

Token * Token_new(TokenType type, char *lexeme);
void Token_free(Token *self);

int Lexer_splitTokens(char* input, List *tokens);