#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <list.h>
#include <lexer.h>
#include <string_buffer.h>
#include <bintree.h>

#define foreach(VARTYPE, VARNAME, LIST)   \
for (int VARNAME##_i = 0, VARNAME##_length = List_count(LIST); !VARNAME##_i; ++VARNAME##_i) \
    for (VARTYPE VARNAME = (VARTYPE)List_at(LIST, VARNAME##_i); \
        VARNAME##_i < VARNAME##_length; \
        VARNAME = (VARTYPE)List_at(LIST, ++VARNAME##_i))



void printTokens(List * tokens)
{
    foreach(Token *, token, tokens)
    {
        if(token->type == TokenType_NUMBER)
        {
            printf("<NUM,%s>", token->lexeme);
        }
        else
        {
            printf("<%s>", token->lexeme);
        }
    }
}


void build(BinTree * node, List *tokens, int * index)
{
    int len = List_count(tokens);
    while(*index < len)
    {
        Token * token = List_at(tokens, *index);
        (*index)++;
        //
        if(token->type == TokenType_LPAR)
        {
            node->left = BinTree_new(NULL);
            build(node->left, tokens, index);
        }
        else if(token->type == TokenType_RPAR)
        {
            return;
        }
        else if(token->type == TokenType_NUMBER)
        {
            node->value = token;
            return;
        }
        else
        {
            node->value = token;
            node->right = BinTree_new(NULL);
            build(node->right, tokens, index);
        }
    
    }
    
    //buid(node, tokens, index);

}

int eval(BinTree * node)
{
    Token * token = node->value;
    if(token->type == TokenType_NUMBER)
    {
        int number = atoi(token->lexeme);
        return number;
    }
    else
    {
        int leftVal = eval(node->left);
        int rightVal = eval(node->right);  
        
        switch(token->type)
        {
            case TokenType_PLUS: return leftVal + rightVal;
            case TokenType_MINUS: return leftVal - rightVal;
            case TokenType_MULT: return leftVal * rightVal;
            case TokenType_DIV: return leftVal / rightVal;
            default: return 0;
            
        }
    }
}


int main(void)
{
   /*  Dict * variables = Dict_new();

    Dict_set(variables, "_x", Var_new());
    Dict_set(variables, "a1", Var_new());
    Dict_set(variables, "main", Var_new());

    Var * varmain = Dict_get(variables, "main");    */


    //
    char * expr = "((7 + 3)*(5 - 3))";
    List * tokens = List_new();
    if(0 != Lexer_splitTokens(expr, tokens))
    {
        return 1;
    }

    printTokens(tokens);
    puts("\n--------------------------");

    BinTree * root = BinTree_new(NULL);
    int index = 0;
    build(root, tokens, &index);

    printf("%i \n", eval(root));

    //@todo free each token
    List_free(tokens);

    return 0;
}

