#include <stdio.h>
#include <progbase.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include <fs.h>

int fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    fclose(f);
    return 1;  // true: exists
}

long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  // number of bytes read
}

int readBufferToFile(const char *fileName, char *buffer)
{
    FILE *f = fopen(fileName, "w");
    if (!f) return 0;  // read 0 bytes from file    
    long writeBytes = fwrite(buffer, strlen(buffer), 1, f);
    fclose(f);
    return writeBytes;  // number of bytes read
}