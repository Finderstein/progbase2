#include <bintree.h>
#include <stdlib.h>

BinTree *BinTree_new(void* value)
{
    BinTree *self = malloc(sizeof(BinTree));
    self->value = value;
    self->left = NULL;
    self->right = NULL;
    return self;
}

void BinTree_free(BinTree * self)
{
    free(self);
}