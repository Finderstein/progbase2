#include <lexer.h>
#include <ctype.h>
#include <stdio.h>
#include <string_buffer.h>

Token * Token_new(TokenType type, char *lexeme)
{
    Token *self = malloc(sizeof(Token));
    self->type = type;
    self->lexeme = lexeme;
    
    return self;
}

void Token_free(Token *self)
{
    free(self);
}

int Lexer_splitTokens(char* input, List *tokens)
{
    StringBuffer * lexeme = StringBuffer_new();

    while(1)
    {
        char ch = *input;
        if(ch == '\0')
        {
            break;
        }
        else if(ch == '(')
        {
            List_add(tokens, Token_new(TokenType_LPAR, "("));
        }
        else if(ch == ')')
        {
            List_add(tokens, Token_new(TokenType_RPAR, ")"));            
        }
        else if(ch == '+')
        {
            List_add(tokens, Token_new(TokenType_PLUS, "+"));            
        }
        else if(ch == '-')
        {
            List_add(tokens, Token_new(TokenType_MINUS, "-"));            
        }
        else if(ch == '*')
        {
            List_add(tokens, Token_new(TokenType_MULT, "*"));            
        }
        else if(ch == '/')
        {
            List_add(tokens, Token_new(TokenType_DIV, "/"));            
        }
        else if(isspace(ch))
        {
            //skip
        }
        else
        {
            if(!isdigit(ch))
            {
                fprintf(stderr, "LEXEME ERR");
                return 1;
            }
            while(isdigit(ch))
            {
                StringBuffer_appendChar(lexeme, ch);
                ch = *(++input);
            }
            input--;

            char * lex = StringBuffer_toNewString(lexeme);
            List_add(tokens, Token_new(TokenType_NUMBER, lex));
            StringBuffer_clear(lexeme);
        }

        

        input++;
    }

    StringBuffer_free(lexeme);
    return 0;
}