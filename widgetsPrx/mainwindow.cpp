#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QListWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString value = ui->lineEdit->text();
    ui->listWidget->addItem(value);
}

void MainWindow::on_pushButton_5_clicked()
{
    for(int i = 0; i < ui->listWidget_2->count(); i++)
    ui->listWidget->addItem(ui->listWidget_2->item(i)->text());
}

void MainWindow::on_pushButton_4_clicked()
{
    for(int i = 0; i < ui->listWidget->count(); i++)
    ui->listWidget_2->addItem(ui->listWidget->item(i)->text());

}

void MainWindow::on_pushButton_3_clicked()
{
    for(int i = ui->listWidget->count() - 1; i >= 0; i--)
    delete ui->listWidget->item(i);
}

void MainWindow::on_listWidget_clicked(const QModelIndex &index)
{
    return;
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->remove->setEnabled(true);
}

void MainWindow::on_remove_clicked()
{
    ui->deleted->setText(ui->listWidget->currentItem()->text());
    delete ui->listWidget->currentItem();
    ui->remove->setEnabled(false);
}

void MainWindow::on_pushButton_6_clicked()
{
    for(int i = ui->listWidget_2->count() - 1; i >= 0; i--)
    delete ui->listWidget_2->item(i);
}
