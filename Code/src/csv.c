#include <stdio.h>
#include <stdlib.h>

#include <csv.h>
#include <string_buffer.h>


struct __CsvTable 
{
    List *rows;
};

struct __CsvRow 
{
    List *value;
};


CsvRow * CsvRow_new(void)
{
    CsvRow * self = malloc(sizeof(CsvRow));
    self->value = List_new();

    return self;
}

void CsvRow_free(CsvRow * self)
{
    free(self->value);
    free(self);
}

void CsvRow_add(CsvRow * self, char * value)
{
    List_add(self->value, value);
}

static void List_copyTo(List *self, List * dest)
{
    List * vals = self;
    for(int i = 0; i < List_count(vals); i++)
    {
        //char???
        void * value = List_at(vals, i);
        List_add(dest, value);
    }
}

void CsvRow_values(CsvRow * self, List * values)
{
    List_copyTo(self->value, values);
}


CsvTable * CsvTable_new(void)
{
    CsvTable * self = malloc(sizeof(CsvTable));
    self->rows = List_new();

    return self;
}

void CsvTable_free(CsvTable * self)
{
    free(self->rows);
    free(self);
}

void CsvTable_add (CsvTable * self, CsvRow * row)
{
    List_add(self->rows, row);    
}

void CsvTable_rows(CsvTable * self, List * rows)
{
    List_copyTo(self->rows, rows);
}


CsvTable * CsvTable_newFromString(const char * csvString)
{
    CsvTable* t = CsvTable_new();
    CsvRow *row = CsvRow_new();
    StringBuffer * value = StringBuffer_new();
    while(1)
    {
        char ch = *csvString;
        if(ch == '\0')
        {
            CsvRow_add(row, StringBuffer_toNewString(value));
            StringBuffer_clear(value);
            //
            CsvTable_add(t, row);

            break;
        }
        else if(ch == ',')
        {
            CsvRow_add(row, StringBuffer_toNewString(value));
            StringBuffer_clear(value);
        }
        else if(ch == '\n')
        {
            CsvRow_add(row, StringBuffer_toNewString(value));
            StringBuffer_clear(value);
            //
            CsvTable_add(t, row);
            row = CsvRow_new();
        }
        else
        {
            StringBuffer_appendChar(value, ch);
        }

        csvString++;
    }
    StringBuffer_free(value);


    return t;
}

char * CsvTable_toNewString  (CsvTable * self)
{
    StringBuffer *sb = StringBuffer_new();

    for(int ri = 0; ri < List_count(self->rows); ri++)
    {
        if(ri != 0)
        {
            StringBuffer_appendChar(sb, '\n');
        }
        CsvRow *row = List_at(self->rows, ri);
        int nvalues = List_count(row->value);

        for(int vi = 0; vi < nvalues; vi++)
        {
            if(vi != 0)
            {
                StringBuffer_appendChar(sb, ',');
            }
            char * value = List_at(row->value, vi);
            StringBuffer_append(sb, value);
        }
    }

    char * result = StringBuffer_toNewString(sb);    
    return result;
}
