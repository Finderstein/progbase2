#include <stdio.h>
#include <stdlib.h>

#include <csv.h>
#include <list.h>
#include <string_buffer.h>



#define foreach(VARTYPE, VARNAME, LIST)   \
for (int VARNAME##_i = 0, VARNAME##_length = List_count(LIST); !VARNAME##_i; ++VARNAME##_i) \
    for (VARTYPE VARNAME = (VARTYPE)List_at(LIST, VARNAME##_i); \
        VARNAME##_i < VARNAME##_length; \
        VARNAME = (VARTYPE)List_at(LIST, ++VARNAME##_i))




void printCsvTable(CsvTable *t);

int main(void)
{
   CsvTable * t = CsvTable_new();

   CsvRow * ira = CsvRow_new();
   CsvRow_add(ira, "1");
   CsvRow_add(ira, "Ira");
   CsvRow_add(ira, "4.5");   
   CsvTable_add(t, ira);  
   
   printCsvTable(t);
   puts("------------------------");

   CsvRow * igor = CsvRow_new();
   CsvRow_add(igor, "2");
   CsvRow_add(igor, "Igor");
   CsvRow_add(igor, "3.4");   
   CsvTable_add(t, igor);   
   
   char * csvString = CsvTable_toNewString(t);
   
   puts(csvString);
   puts("------------------------");
   

    CsvTable * t2 = CsvTable_newFromString(csvString);
    printCsvTable(t2);
    puts("------------------------");
   
    CsvTable_free(t2);

   //@todo free rows
   free(csvString);
   CsvTable_free(t);


    return 0;
}

void printCsvTable(CsvTable *t)
{
    List * rows = List_new();
    CsvTable_rows(t, rows);
//    for(int i = 0; i < )
    foreach(CsvRow *, row, rows)
    {
        List * values = List_new();
        CsvRow_values(row, values);
        foreach(char *, value, values)
        {
            printf("%s, ", value);
        }
        puts("");
    }


}
