#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString scoreToGrade(int score)
{
    return QString::number(score);
}


void MainWindow::on_scoreEl_valueChanged(int score)
{
    QString grade = scoreToGrade(score);
    ui->gradeLbl->setText(grade);
}

void MainWindow::on_checkBox_toggled(bool checked)
{
    ui->prjTxt->setEnabled(checked);
}


void MainWindow::on_pushButton_clicked()
{
    //@todo check for empty fields
    if(ui->nameEd->text().isEmpty() || ui->surnEd->text().isEmpty() || ui->prjTxt->document()->toPlainText().isEmpty())
    QMessageBox::critical(this, "Warning window", "Error",
                             QMessageBox::Ok);
}

void MainWindow::on_gradeLbl_windowIconTextChanged(const QString &iconText)
{
    return;
}
