#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <progbase.h>
#include <progbase/events.h>
#include <progbase/console.h>

/* custom constant event type ids*/
enum {
	KeyInputEventTypeId,
	TimerEventTypeId,	
	EndTimerEventTypeId,
	SpaceHitEventTypeId,
	RemoveTimerEventTypeId
};

/* event handler functions prototypes */
void UpdatePrintHandler_update(EventHandler * self, Event * event);
void KeyInputHandler_update(EventHandler * self, Event * event);
void CustomHandler_handleEvent(EventHandler * self, Event * event);
void Timer_handleEvent(EventHandler * self, Event * event);
void Space_handleEvent(EventHandler * self, Event * event);

typedef struct Timer Timer;
struct Timer {
	int stop;
	int timeCounter;
};

int main(void) {
	// startup event system
	EventSystem_init();

	// add event handlers
	EventSystem_addHandler(EventHandler_new(NULL, NULL, UpdatePrintHandler_update));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, KeyInputHandler_update));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, CustomHandler_handleEvent));

	// start infinite event loop
	EventSystem_loop();
	// cleanup event system
	EventSystem_cleanup();
	return 0;
}

/* event handlers functions implementations */

void UpdatePrintHandler_update(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			puts("");
			puts("Створити обробник-таймер зворотнього відліку. Нажаття на кнопку [Enter] стартує таймер на 7 секунд."
			"\nНажаття на кнопку [Space] може призупинити/відновити відлік таймеру. Кнопка [Esc] відміняє таймер."
			"\nПісля закінчення відліку таймер зупиняється і генерується відповідна подія."
			"\n\nТекст варіанту завдання має бути завжди видимий у консолі програми.");
			puts("\nPut [q] to EXIT the program.");
			puts("Put [Enter] to START the timer.");
			puts("Put [Space] to STOP the timer.");
			puts("Put [Esc] to REMOVE the timer.");			
			puts("\n<<<START!>>>");
			
			puts("");			
			break;
		}
		case UpdateEventTypeId: {
			break;
		}
		case ExitEventTypeId: {
			puts("");
			puts("<<<EXIT!>>>");
			puts("");
			break;
		}
	}
}

void KeyInputHandler_update(EventHandler * self, Event * event) {
	if (conIsKeyDown()) {  // non-blocking key input check
		char * keyCode = malloc(sizeof(char));
		*keyCode = getchar();
		puts("");
		EventSystem_emit(Event_new(self, KeyInputEventTypeId, keyCode, free));
	}
}

void Space_handleEvent(EventHandler * self, Event * event) {
	switch (event->type) {
		case SpaceHitEventTypeId: {
			Timer * timer = (Timer *)self->data;
			
			if(timer->stop %2 == 1)
			{
				Timer * newtimer = malloc(sizeof(Timer));
				newtimer->timeCounter = timer->timeCounter;
				newtimer->stop = timer->stop;
				printf(">>> Continue timer on %i\n", newtimer->timeCounter);
				EventSystem_removeHandler(self);
				EventSystem_addHandler(EventHandler_new(newtimer, free, Timer_handleEvent));
			}
			timer->stop++;
			
			break;
		}
		case RemoveTimerEventTypeId: {
			printf("\nDelete timer!\n"); 
			EventSystem_removeHandler(self);
			break;
		}
	}
}

void CustomHandler_handleEvent(EventHandler * self, Event * event) {
	switch (event->type) {
		case KeyInputEventTypeId: {
			char * keyCodePtr = (char *)event->data;
			char keyCode = *keyCodePtr;
			if (keyCode == ' ') {
				EventSystem_emit(Event_new(self, SpaceHitEventTypeId, NULL, NULL));
			}
			else if ((int)keyCode == 10) {
				Timer * timer = malloc(sizeof(Timer));
				timer->stop = 0;
				timer->timeCounter = 7;
				puts("Start timer!");
				EventSystem_addHandler(EventHandler_new(timer, free, Timer_handleEvent));
			}
			else if ((int)keyCode == 27) {
				EventSystem_emit(Event_new(self, RemoveTimerEventTypeId, NULL, NULL));
			}
			else if (keyCode == 'q') {
				EventSystem_exit();
			}
			else
			{
				printf("Key pressed `%c` [%i]\n", keyCode, keyCode);
			}


			break;
		}
	} 
}

void Timer_handleEvent(EventHandler * self, Event * event) {
	switch(event->type) {
		case UpdateEventTypeId: {
			Timer * timer = (Timer *)self->data;
			double elapsedMillis = *(double *)event->data;
			
			if (timer->timeCounter == 0) {
				EventSystem_emit(Event_new(self, EndTimerEventTypeId, NULL, NULL));
			}
			else
			{
				printf("\nTimer %i {%.1lf}\n", timer->timeCounter, elapsedMillis);
				fflush(stdout);   /* force console output */
				sleepMillis(1000);
			}
			timer->timeCounter -= 1;

			break;
		}
		case EndTimerEventTypeId: {
			printf("\nTimer COMPLETED!\n"); 
			EventSystem_removeHandler(self);
			break;
		}
		case RemoveTimerEventTypeId: {
			printf("\nDelete timer!\n"); 
			EventSystem_removeHandler(self);
			break;
		}
		
		case SpaceHitEventTypeId: {
			Timer * timer = (Timer *)self->data;
			if(timer->stop % 2 == 0)
			{
				Timer * newtimer = malloc(sizeof(Timer));
				newtimer->timeCounter = timer->timeCounter;
				newtimer->stop = timer->stop;			
				printf("\n>>> Stop timer on %i\n", timer->timeCounter);
				EventSystem_removeHandler(self);
				EventSystem_addHandler(EventHandler_new(newtimer, free, Space_handleEvent));
			}
			timer->stop++;
			
			break;
		}
	}
}
