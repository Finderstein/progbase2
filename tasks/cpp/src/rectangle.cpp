#include <rectangle.h>

Rectangle :: Rectangle(string name, int wid, int len)
{
    this->name = name;
    this->wid = wid;
    this->len = len;
}

int Rectangle :: square()
{
    return this->wid * this->len;
}

void Rectangle :: print_Rectangle()
{
    cout <<"Rectangle name:" 
        << this->name << " | width:" 
        << this->wid << " | length:"
        << this->len << endl;
}
