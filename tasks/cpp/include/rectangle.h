#pragma once

#include <string>
#include <iostream>
#include <math.h>

using namespace std;

class Rectangle
{
private:
    int wid;
    int len;    
    string name;
public:
    Rectangle(string, int, int);
    int square();
    void print_Rectangle();
};