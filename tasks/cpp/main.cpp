#include <vector>
#include <limits>
#include <rectangle.h>

int main(void)
{
    system("clear");

    vector<Rectangle *> rectangles;

    int check = 1;
    while(1)
    {
        cout << "1.Print all rectangles.\n" 
            << "2.Add rectangle with new values.\n" 
            << "3.Print rectangles with square smaller than S.\n"
            << "4.Quit the program.\n" 
            << endl;

        if(check != 1 && check != 2 && check != 3 && check != 4)
        {
            cout << "There is no such choice!" << endl;
        }

        cout << "Make a choice: ";
        cin >> check;
        if (cin.fail())
        {
            cin.clear(); // clears error flags
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        cout << endl;

        if(check == 1)
        {
            for(auto iii : rectangles)
            {
                iii->print_Rectangle();
            }
        }
        if(check == 2)
        {
            cout << "Put the name of rectangle: ";
            string name;
            cin.get();
            getline(cin, name);

            cout << "Put the width of rectangle: ";
            int wid;
            cin >> wid;
            if (cin.fail())
            {
                cin.clear(); // clears error flags
                cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            cout << "Put the length of rectangle: ";
            int len;
            cin >> len;
            if (cin.fail())
            {
                cin.clear(); // clears error flags
                cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            if(len <= 0 || wid <= 0)
            {
                cout << "Invalid number!" << endl;
            }
            else
            {
                rectangles.push_back(new Rectangle(name, wid, len));
            }
        }
        if(check == 3)
        {
            cout << "Put the S: ";
            int s;
            cin >> s;
            if (cin.fail())
            {
                cin.clear(); // clears error flags
                cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }

            int sq = 0;
            for(auto iii : rectangles)
            {
                sq = iii->square();
                if(sq < s)
                {
                    iii->print_Rectangle();
                }
            }
        }
        if(check == 4)
        {
            break;
        }

        cout << "==================================================================" << endl << endl;
    }

    for(auto iii : rectangles)
    {
        delete iii;
    }

    return 0;
}