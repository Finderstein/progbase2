#ifndef XMLCASHIERSTORAGE_H
#define XMLCASHIERSTORAGE_H
#include <cashier.h>
#include <cashierstorage.h>


class XmlCashierStorage : public CashierStorage
{
public:
    XmlCashierStorage(std::string & name):CashierStorage(name){}
    std::vector<Cashier> load();
    void save(std::vector<Cashier> & entities);
};

#endif // XMLCASHIERSTORAGE_H
