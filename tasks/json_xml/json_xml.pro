QT += core
QT -= gui
QT += xml

TARGET = json_xml
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    jsoncashierstorage.cpp \
    xmlcashierstorage.cpp \
    storagemanager.cpp

CONFIG += c++14

HEADERS += \
    cashier.h \
    cashierstorage.h \
    jsoncashierstorage.h \
    xmlcashierstorage.h \
    messageexception.h \
    storagemanager.h

