#ifndef CASHIER_H
#define CASHIER_H
#include <QDebug>
#include <QtXml>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>


class Cashier
{
public:
    QString name;
    int workHours;
    double salary;
    std::vector<QString> workDays;
};

#endif // CASHIER_H
