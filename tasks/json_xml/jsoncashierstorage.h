#ifndef JSONCASHIERSTORAGE_H
#define JSONCASHIERSTORAGE_H
#include <cashier.h>
#include <cashierstorage.h>

class JsonCashierStorage : public CashierStorage
{
public:
    JsonCashierStorage(std::string & name):CashierStorage(name){}
    std::vector<Cashier> load();
    void save(std::vector<Cashier> & entities);
};

#endif // JSONCASHIERSTORAGE_H
