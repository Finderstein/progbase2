#ifndef MESSAGEEXCEPTION_H
#define MESSAGEEXCEPTION_H
#include <cashier.h>
#include <cashierstorage.h>
#include <xmlcashierstorage.h>
#include <jsoncashierstorage.h>


class MessageException : public std::exception
{
    std::string _error;
public:
    MessageException(std::string & error) {
        this->_error = error;
    }
    const char * what() const noexcept {
        return this->_error.c_str();
    }
};

#endif // MESSAGEEXCEPTION_H
