#include "jsoncashierstorage.h"
#include <messageexception.h>
#include <storagemanager.h>
#include <stdexcept>
#include <QDebug>
#include <QtXml>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>

using namespace std;

static int fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    fclose(f);
    return 1;  // true: exists
}

static long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

static int readFileToBuffer(const char *fileName, char *buffer, int bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  // number of bytes read
}

static int readBufferToFile(char *fileName, const char *buffer)
{
    FILE *f = fopen(fileName, "w");
    if (!f) return 0;  // read 0 bytes from file
    long writeBytes = fwrite(buffer, strlen(buffer), 1, f);
    fclose(f);
    return writeBytes;  // number of bytes read
}

void JsonCashierStorage::save(vector<Cashier> & entities)
{
    //перевірити на наявність файлу...
    char *file = (char *)name().c_str();
    if(!fileExists(file))
    {
        throw std::invalid_argument("File not exists!!");
    }

    QJsonDocument doc;
    QJsonObject allCashiersEl;

    QJsonArray cashiersArr;
    for (Cashier & c: entities) {
        QJsonObject cashierObj;
        cashierObj.insert("name", c.name);
        cashierObj.insert("workHours", c.workHours);
        cashierObj.insert("salary", c.salary);

        QJsonArray studentsArr;
        for (QString & d: c.workDays) {
            QJsonObject so;
            so.insert("day", d);
            studentsArr.append(so);
        }
        cashierObj.insert("workDays", studentsArr);

        cashiersArr.append(cashierObj);
    }
    allCashiersEl.insert("cashiers", cashiersArr);

    doc.setObject(allCashiersEl);

    QString jsonString = doc.toJson();

    //зберегти jsonString в файл

    readBufferToFile(file, jsonString.toStdString().c_str());


}


vector<Cashier> JsonCashierStorage::load(void) {
    //перевірити на наявність файлу...
    char *file = (char *)name().c_str();
    if(!fileExists(file))
    {
        throw std::invalid_argument("File not exists!!");
    }

    long lenOfFile = getFileSize(file) + 1;
    char text[lenOfFile];
    int bytesRead = readFileToBuffer(file, &text[0], (int)lenOfFile);
    text[bytesRead] = '\0';

    string str = string(text);

    vector<Cashier> cs;

    QJsonParseError err;

    QJsonDocument doc = QJsonDocument::fromJson(
        QByteArray::fromStdString(str),
        &err);

    if (err.error != QJsonParseError::NoError) {
        throw std::invalid_argument("Invalid text!!");
    }

    QJsonObject cashiersObj = doc.object();

    QJsonArray cashiersArr = cashiersObj.value("cashiers").toArray();

    for (int i = 0; i < cashiersArr.size(); i++) {
          QJsonValue value1 = cashiersArr.at(i);
          QJsonObject cashierObj = value1.toObject();
          Cashier c;
          c.name = cashierObj.value("name").toString();
          c.workHours = cashierObj.value("workHours").toInt();
          c.salary = cashierObj.value("salary").toDouble();

          QJsonArray workDaysArr = cashierObj.value("workDays").toArray();
          for (int j = 0; j < workDaysArr.size(); j++) {
                QJsonValue value2 = workDaysArr.at(j);
                QJsonObject dayObj = value2.toObject();
                QString d;
                d = dayObj.value("day").toString();
                c.workDays.push_back(d);
          }

          cs.push_back(c);
    }

    return cs;

}
