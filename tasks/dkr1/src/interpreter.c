#include <interpreter.h>
#include <tree.h>
#include <ast.h>
#include <string_ext.h>
#include <dict.h>
#include <list.h>
#include <std_functions.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <math.h>


StdFunction * StdFunction_new(Function ptr)
{
    StdFunction * self = malloc(sizeof(StdFunction));
    self->ptr = ptr;

    return self;
}

void StdFunction_free(StdFunction * self)
{
    free(self); 
}
 
Value * Value_new(ValueType type, void * value)
{
    Value * self = malloc(sizeof(Value));
    self->type = type;
    self->value = value;

    return self;
}

void Value_free(Value * self)
{
    free(self->value);
    free(self);
}

void ValueType_print(ValueType self)
{
    switch(self)
    {
        case ValueType_NUMBER: printf("number"); break;
        case ValueType_BOOL: printf("bool"); break;
        case ValueType_STRING: printf("string"); break;
        case ValueType_UNDEFINED: printf("undefined"); break;        
        default: printf("<?>"); break;
    }
}

void Value_print(Value * self)
{
    switch(self->type)
    {
        case ValueType_NUMBER: 
        {
            double val = *(double *)self->value;
            printf("%lf ", val);
            break;
        }
        case ValueType_BOOL: 
        {
            bool val = *(bool *)self->value;
            printf("%s ", val == true ? "TRUE" : "FALSE");
            break;
        }
        case ValueType_STRING: 
        {
            char * val = (char *)self->value;
            printf("\"%s\" ", val);
            break;
        }
        case ValueType_UNDEFINED:  
        {
            //printf("undefined |");
            break;
        }       
        default: printf("<?>"); break;
    }
    //ValueType_print(self->type);
}



void Program_setError(Program * self, char * error)
{
    self->error = error;
}

Value * Value_newNumber(double number)
{
    double * numberMen = malloc(sizeof(double));
    *numberMen = number;
    return Value_new(ValueType_NUMBER, numberMen);
}

Value * Value_newArray(List * arr)
{
    return Value_new(ValueType_ARR, arr);
}

Value * Value_newString(char * str)
{
    return Value_new(ValueType_STRING, strdup(str));
}

Value * Value_newBool(bool item)
{
    bool * men = malloc(sizeof(bool));
    *men = item;
    return Value_new(ValueType_BOOL, men);
}

Value * Value_newUndefined(void)
{
    return Value_new(ValueType_UNDEFINED, NULL);
}

double Value_number(Value * self)
{
    assert(self->type == ValueType_NUMBER);
    return *((double *)self->value);
}

List * Value_arr(Value * self)
{
    assert(self->type == ValueType_ARR);
    return ((List *)self->value);
}

bool Value_bool(Value * self)
{
    assert(self->type == ValueType_BOOL);
    return *((bool *)self->value);
}

char * Value_string(Value * self)
{
    assert(self->type == ValueType_STRING);
    return ((char *)self->value);
}

Value * Value_newCopy(Value * origin)
{
    switch(origin->type)
    {
        case ValueType_BOOL: return Value_newBool(Value_bool(origin))  ;
        case ValueType_NUMBER: return Value_newNumber(Value_number(origin));
        case ValueType_STRING: return Value_newString(Value_string(origin));
        case ValueType_UNDEFINED: assert(0 && "Implement undefined"); return NULL; 
        default: assert(0 && "Not supported"); return NULL;
    }
}

bool Value_equals(Value * a, Value * b)
{
    if(a->type != b->type) return false;
    switch(a->type)
    {
        case ValueType_BOOL: return Value_bool(a) == Value_bool(b)  ;
        case ValueType_NUMBER:  return fabs(fabs(Value_number(a)) - fabs(Value_number(b))) < 1e-6;
        case ValueType_STRING: return 0 == strcmp(Value_string(a), Value_string(b));
        case ValueType_UNDEFINED: return true;
        default: assert(0 && "Not supported");
    }
}

bool Value_asBool(Value * self)
{
    switch(self->type)
    {
        case ValueType_BOOL: return Value_bool(self);
        case ValueType_NUMBER: return fabs(Value_number(self)) > 1e-6;
        case ValueType_STRING: return Value_string(self) != NULL; //@todo empty string are true or false?
        case ValueType_UNDEFINED: return false;
        default: assert(0 && "Not supported");
    }
}


Value * Program_getVariableValue(Program * self, char * varId)
{
    if(!Dict_contains(self->variables, varId))
    {
        self->error = strdup("Var id not found");
        return NULL;
    }
    return Dict_get(self->variables, varId);
}

Value * numberArr_at(Program * program, Tree * node);

bool Ast_eq(Program * program, Tree * node);

Value * eval(Program * program, Tree * node)
{
    AstNode * astNode = node->value;
    switch(astNode->type)
    {
        case AstNodeType_NUMBER:
        {
            double number =  atof(astNode->name);
            return Value_newNumber(number);
        }
        case AstNodeType_STRING:
        {
            return Value_newString(astNode->name);
        } 
        case AstNodeType_BOOL:
        {
            bool boolean =  0 == strcmp(astNode->name, "true") ? true : false;
            return Value_newBool(boolean);
        }    
        case AstNodeType_ARRBR:
        {
            List * newList = List_new();
            return Value_newArray(newList);
        }    
        case AstNodeType_ID:
        {
            char * varId = astNode->name;
            int at = 0;

            if(!strcmp(varId, "at"))
            at = 1;

            if(!strcmp(varId, "count"))
            at = 1;

            if(!strcmp(varId, "push"))
            at = 1;

            if(List_count(node->children) != 0)
            {
                Tree * argListNode = List_at(node->children, 0);
                AstNode * argList = argListNode->value;
                assert(argList->type == AstNodeType_ARGLIST);
                //
                if(!Dict_contains(program->functions, varId))
                {
                     program->error = strdup("Call unknown function");
                     return NULL;
                }
                //
                List * arguments = List_new();
                for(int i = 0; i < List_count(argListNode->children); i++)
                {
                    Tree * argListChildNode = List_at(argListNode->children, i);
                    if(at == 1 && i == 0)
                    {
                        Value * argumentValue = numberArr_at(program, argListChildNode);
                        if(program->error)
                        {
                            List_free(arguments);
                            return NULL;
                        }
                        List_add(arguments, argumentValue);
                    }
                    else
                    {
                        Value * argumentValue = eval(program, argListChildNode);
                        if(program->error)
                        {
                            List_free(arguments);
                            return NULL;
                        }
                        List_add(arguments, argumentValue);
                    }
                    
                }
                //call function
                StdFunction * func = Dict_get(program->functions, varId);
                Value * retValue = func->ptr(program, arguments);
                List_free(arguments);

                return retValue;
            }
            Value * varValue = Program_getVariableValue(program, varId);
            if(program->error) return NULL;
            return Value_newCopy(varValue);

        }  
        case AstNodeType_ADD:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation1");
                Value_free(firstValue);
                return NULL;
            }
            int nchild = List_count(node->children);
            if(nchild == 1)
            {
                return firstValue;
            }
            else
            {
                Tree * secondChild = List_at(node->children, 1);
                Value * secondValue = eval(program, secondChild);
                if(program->error) 
                {
                    Value_free(firstValue);                    
                    return NULL;
                }
                if(secondValue->type != ValueType_NUMBER)
                {
                    program->error = strdup("Invalid opperation2");
                    Value_free(firstValue); 
                    Value_free(secondValue);                   
                    return NULL;
                }
                double res = Value_number(firstValue) + Value_number(secondValue);
                Value_free(firstValue);
                Value_free(secondValue);                
                return Value_newNumber(res);
            }
        }
        case AstNodeType_SUB:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation3");
                Value_free(firstValue);
                return NULL;
            }
            int nchild = List_count(node->children);
            if(nchild == 1)
            {
                return firstValue;
            }
            else
            {
                Tree * secondChild = List_at(node->children, 1);
                Value * secondValue = eval(program, secondChild);
                if(program->error) 
                {
                    Value_free(firstValue);                    
                    return NULL;
                }
                if(secondValue->type != ValueType_NUMBER)
                {
                    program->error = strdup("Invalid opperation4");
                    Value_free(firstValue); 
                    Value_free(secondValue);                   
                    return NULL;
                }
                double res = Value_number(firstValue) - Value_number(secondValue);
                Value_free(firstValue);
                Value_free(secondValue);                
                return Value_newNumber(res);
            }
        }
        case AstNodeType_MULT:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation5");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation6");
                Value_free(firstValue); 
                Value_free(secondValue);                   
                return NULL;
            }
            double res = Value_number(firstValue) * Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newNumber(res);
        }
        case AstNodeType_DIV:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation7");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation8");
                Value_free(firstValue); 
                Value_free(secondValue);                   
                return NULL;
            }
            if(fabs(Value_number(secondValue)) < 1e-6)
            {
                program->error = strdup("Invalid opperation9");
                Value_free(firstValue); 
                Value_free(secondValue);                   
                return NULL;
            }
            double res = Value_number(firstValue) / Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newNumber(res);
        }
        case AstNodeType_AND:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            bool res = Value_asBool(firstValue) && Value_asBool(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newBool(res);
        }
        case AstNodeType_OR:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            bool res = Value_asBool(firstValue) || Value_asBool(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newBool(res);
        }
        case AstNodeType_EQUAL:
        {
            bool eq = Ast_eq(program, node);             
            return Value_newBool(eq);
        }
        case AstNodeType_NOT:
        {
            Tree * exprNode = List_at(node->children, 0);
            Value * testValue = eval(program, exprNode);
            if(program->error) return NULL;
            bool testBool = Value_asBool(testValue);
            return Value_newBool(!testBool);
        }
        case AstNodeType_NOTEQUAL:
        {
            bool eq = Ast_eq(program, node);             
            return Value_newBool(!eq);
        }
        case AstNodeType_MORE:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation10");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation11");
                Value_free(firstValue); 
                Value_free(secondValue);                   
                return NULL;
            }
            bool res = Value_number(firstValue) > Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newBool(res);
        }
        case AstNodeType_LESS:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation12");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation13");
                Value_free(firstValue); 
                Value_free(secondValue);                   
                return NULL;
            }
            bool res = Value_number(firstValue) < Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newBool(res);
        }
        case AstNodeType_EQMORE:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation14");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation15");
                Value_free(firstValue); 
                Value_free(secondValue);                   
                return NULL;
            }
            bool res = Value_number(firstValue) >= Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newBool(res);
        }
        case AstNodeType_EQLESS:
        {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation16");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) 
            {
                Value_free(firstValue);                    
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid opperation17");
                Value_free(firstValue); 
                Value_free(secondValue);                   
                return NULL;
            }
            bool res = Value_number(firstValue) <= Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);                
            return Value_newBool(res);
        }
        default:
        {
            assert(0 && "Not implemented");
        }
    }
    return NULL;
}

Value * numberArr_at(Program * program, Tree * node)
{
    AstNode * astNode = node->value;
    char * varId = astNode->name;
    Value * varValue = Program_getVariableValue(program, varId);
    if(program->error) return NULL;
    return Value_newArray(Value_arr(varValue));

}

bool Ast_eq(Program * program, Tree * node)
{
    Tree * firstChild = List_at(node->children, 0);
    Value * firstValue = eval(program, firstChild);
    if(program->error) return false;
    Tree * secondChild = List_at(node->children, 1);
    Value * secondValue = eval(program, secondChild);
    if(program->error) 
    {
        Value_free(firstValue);                    
        return false;
    }
    bool res = Value_equals(firstValue, secondValue);
    Value_free(firstValue);
    Value_free(secondValue); 
    return res;
}

void executeStatement(Program * program, Tree * childNode);

void executeIf(Program * program, Tree * ifNode)
{
    Tree * exprNode = List_at(ifNode->children, 0);
    Value * testValue = eval(program, exprNode);
    if(program->error) return;
    bool testBool = Value_asBool(testValue);
    Value_free(testValue);
    if(testBool)
    {
        executeStatement(program, List_at(ifNode->children, 1));
    }
    else if(List_count(ifNode->children) > 2)
    {
        executeStatement(program, List_at(ifNode->children, 2));
    }
    
}

void executeWhile(Program * program, Tree * whileNode)
{
    Tree * exprNode = List_at(whileNode->children, 0);
    while(true)
    {
        Value * testValue = eval(program, exprNode);
        if(program->error) return;
        bool testBool = Value_asBool(testValue);
        Value_free(testValue);
        if(!testBool) return;
        //
        executeStatement(program, List_at(whileNode->children, 1));
        if(program->error) return;
    }
}

void executeBlock(Program * program, Tree * blockNode)
{
    for(int i = 0; i < List_count(blockNode->children); i++)
    {
        Tree * childNode = List_at(blockNode->children, i);
        //
        executeStatement(program, childNode);
        if(program->error) return;
    }
}


void executeStatement(Program * program, Tree * childNode)
{
    AstNode * child = childNode->value;
    switch(child->type)
    {
        case AstNodeType_ASSIGN:
        {     
            Tree * fChildNode = List_at(childNode->children, 0);
            AstNode * fChild = fChildNode->value;
            Tree * sChildNode = List_at(childNode->children, 1);   
            //AstNode * sChild = sChildNode->value;
            //
            char * varName = strdup(fChild->name);
            Value * varValue = eval(program, sChildNode);
            if(program->error) return; 
            //
            if(!Dict_contains(program->variables, varName))
            {
                program->error = strdup("Duplicate variable id");
                free(varName);
                Value_free(varValue);
            }
            Value * oldValue = Dict_set(program->variables, varName, varValue);
            Value_free(oldValue);
            break;
        }
        case AstNodeType_IF:
        {
            executeIf(program, childNode);
            if(program->error) return;
            
            break;
        }
        case AstNodeType_WHILE:
        {
            executeWhile(program, childNode);
            if(program->error) return;
            
            break;                
        }
        case AstNodeType_BLOCK:
        {
            executeBlock(program, childNode);
            if(program->error) return;
            break;                
        }
        default:
        {
            Value * val = eval(program, childNode);
            if(program->error) return;
            Value_free(val);
        }
    }
}

int Interpreter_execute(Tree * astTree)
{
    AstNode * astNode = astTree->value;
    assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };
    Dict_add(program.functions, "print", StdFunction_new(std_print));
    Dict_add(program.functions, "strlen", StdFunction_new(std_strlen));
    Dict_add(program.functions, "sqrt", StdFunction_new(std_sqrt));
    Dict_add(program.functions, "pow", StdFunction_new(std_pow));      
    Dict_add(program.functions, "substr", StdFunction_new(std_substr));
    Dict_add(program.functions, "scan", StdFunction_new(std_scan));    
    Dict_add(program.functions, "strtonum", StdFunction_new(std_strtonum));    
    Dict_add(program.functions, "numtostr", StdFunction_new(std_numtostr));
    Dict_add(program.functions, "push", StdFunction_new(std_push));        
    Dict_add(program.functions, "at", StdFunction_new(std_at));        
    Dict_add(program.functions, "count", StdFunction_new(std_count));            
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    free(varName);                
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    free(varName);
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
            case AstNodeType_IF:
            {
                executeIf(&program, childNode);
                if(program.error) break;
                break;
            }
            case AstNodeType_WHILE:
            {
                executeWhile(&program, childNode);
                if(program.error) break;
                break;                
            }
            case AstNodeType_BLOCK:
            {
                executeBlock(&program, childNode);
                if(program.error) break;
                break;                
            }
            default:
            {
                Value * val = eval(&program, childNode);
                if(program.error) break;
                Value_free(val);
            }
        }
    }

    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_clear(program.functions);
    Dict_free(program.functions);  
      
    if(program.error)
    {
        fprintf(stderr, "Run-time error: %s\n", program.error);
        free(program.error);
        return 1;
    }
    return 0;
}