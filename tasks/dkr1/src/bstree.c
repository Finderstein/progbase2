#include <bstree.h>
#include <tree.h>

#include <interpreter.h>


typedef struct
{
     int key;
     void * value;
} KeyValuePair;


struct __BSTree
{
    Tree * root;
    size_t count;
};

static KeyValuePair * Kvp_new(int key, void * value)
{
    KeyValuePair * self = malloc(sizeof(KeyValuePair));
    self->key = key;
    self->value = value;

    return self;
}

static void Kvp_free(KeyValuePair * self)
{
    free(self);
}

static Tree * newNode(int key, void * value)
{
    KeyValuePair * pair = Kvp_new(key, value);
    Tree * node = Tree_newDegree(pair, 2);
    List_add(node->children, NULL);
    List_add(node->children, NULL);    

    return node;
}

static void freeNode(Tree * self)
{
    free(((KeyValuePair *)(((KeyValuePair *)self->value)->value))->value);
    Kvp_free(((KeyValuePair *)self->value)->value);
    Kvp_free(self->value);
    Tree_free(self);
}

BSTree * BSTree_new(void)
{
    BSTree * self = malloc(sizeof(BSTree));
    self->root = NULL;

    return self;
}

void BSTree_free(BSTree * self)
{
    free(self);
}

static void * getLeft(Tree * node)
{
    return List_at(node->children, 0);
}

static void * getRight(Tree * node)
{
    return List_at(node->children, 1);
}

static void setLeft(Tree * node, Tree * value)
{
    List_set(node->children, 0, value);
}

static void setRight(Tree * node, Tree * value)
{
    List_set(node->children, 1, value);
}

static void insert(Tree * node, Tree * child)
{
    int nodeKey = ((KeyValuePair *)node->value)->key;
    int childKey = ((KeyValuePair *)child->value)->key;
    if(childKey < nodeKey)
    {
        if(getLeft(node))
        {
            insert(getLeft(node), child);
        }
        else
        {
            setLeft(node, child);
        }
    }
    if(childKey > nodeKey)
    {
        if(getRight(node))
        {
            insert(getRight(node), child);
        }
        else
        {
            setRight(node, child);
        }
    }
}

void   BSTree_insert   (BSTree * self, int key, void * value)
{
    self->count++;
    Tree * newChild = newNode(key, value);
    if(self->root == NULL)
    {
        self->root = newChild;
    }
    else
    {
        insert(self->root, newChild);
    }
}

static bool lookup(Tree * node, int key)
{
    if(node == NULL) return false;
    int nodeKey = ((KeyValuePair *)node->value)->key;
    if(key < nodeKey)
    {
        return lookup(getLeft(node), key);
    }
    else if(key > nodeKey)
    {
        return lookup(getRight(node), key);
    }
    else 
    {
        return true;
    }
}

bool   BSTree_lookup   (BSTree * self, int key)
{
    return lookup(self->root, key);
}

static void * search(Tree * node, int key)
{
    if(node == NULL)
    {
        return NULL;
    }
    
    int nodeKey = ((KeyValuePair *)node->value)->key;
    if(key < nodeKey)
    {
        return search(getLeft(node), key);
    }
    else if(key > nodeKey)
    {
        return search(getRight(node), key);
    }
    else 
    {
        return ((KeyValuePair *)node->value)->value;
    }
}

void * BSTree_search   (BSTree * self, int key)
{
    return search(self->root, key);
}

static void * delete(Tree * node, int key)
{
    if(node == NULL)
    {
        return NULL;
    }
    
    int nodeKey = ((KeyValuePair *)node->value)->key;
    if(key < nodeKey)
    {
        return delete(getLeft(node), key);
    }
    else if(key > nodeKey)
    {
        return delete(getRight(node), key);
    }
    else 
    {
        void * oldValue = ((KeyValuePair *)node->value)->value;
        return oldValue;
    }
}

void * BSTree_delete   (BSTree * self, int key)
{
    self->count--;
    return delete(self->root, key);
}

static void clear(Tree * node)
{
    
    if (node->children->tokens[0] != NULL)
    {
        Tree * tree1 = node->children->tokens[0];    
        clear(tree1);
    }

    if (node->children->tokens[1] != NULL)
    {
        Tree * tree2 = node->children->tokens[1];      
        clear(tree2);
    }
    freeNode(node);
}

void   BSTree_clear    (BSTree * self)
{
    self->count = 0;
    clear(self->root);
}

size_t BSTree_count    (BSTree * self)
{
    return self->count;
}

/* static void inOrderTraverseCopyKeys(Tree * node, List * keys)
{
    if(node == NULL) return;
    inOrderTraverseCopyKeys(getLeft(node), values);
    void * key = ((KeyValuePair *)node->value)->key;
    List_add(keys, key);
    inOrderTraverseCopyKeys(getRight(node), values);    
}

void   BSTree_keys     (BSTree * self, List * keys)
{
    inOrderTraverseCopyKeys(self->root, keys);
} */

static void inOrderTraverseCopyValues(Tree * node, List * values)
{
    if(node == NULL) return;
    inOrderTraverseCopyValues(getLeft(node), values);
    void * value = ((KeyValuePair *)node->value)->value;
    List_add(values, value);
    inOrderTraverseCopyValues(getRight(node), values);    
}

void   BSTree_values   (BSTree * self, List * values)
{
    inOrderTraverseCopyValues(self->root, values);
}



static void clear1(Tree * node)
{
    
    if (node->children->tokens[0] != NULL)
    {
        Tree * tree1 = node->children->tokens[0];    
        clear1(tree1);
    }

    if (node->children->tokens[1] != NULL)
    {
        Tree * tree2 = node->children->tokens[1];      
        clear1(tree2);
    }
    free(((KeyValuePair1 *)node->value)->value);
    Kvp_free(node->value);
    Tree_free(node);
}

void   BSTree_clear1    (BSTree * self)
{
    self->count = 0;
    clear1(self->root);
}
