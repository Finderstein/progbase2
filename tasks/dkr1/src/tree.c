#include <list.h>
#include <tree.h>
#include <ast.h>


Tree * Tree_new(void * value)
{
    Tree * self = malloc(sizeof(Tree));
    self->value = value;
    self->children = List_new();
    
    return self;
}

Tree * Tree_newDegree(void * value, size_t degree)
{
    Tree * self = malloc(sizeof(Tree));
    self->value = value;
    self->children = List_newCapacity(degree);
    
    return self;
}

void Tree_free(Tree * self)
{
    List_free(self->children);
    free(self);
}


void Tree_clear(Tree * self)
{
    if(self == NULL)
    {
        return;
    }
    if(self->children != NULL)
    {
        int count = List_count(self->children);
        for(int i = 0; i < count; i++)
        {
            Tree * tree = List_at(self->children, i);
            Tree_clear(tree);
        }
        
    }
    
    if(self->value != NULL)
    {
        AstNode_free_name(self->value); 
    }
    else
    {
        free(self->value);
    } 
    List_free(self->children);
    free(self);
}