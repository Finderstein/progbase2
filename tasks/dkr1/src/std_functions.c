#include <std_functions.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <string_ext.h>



Value * std_print(Program * program, List * values)
{
    for(int i = 0; i < List_count(values); i++)
    {
        Value * value = List_at(values, i);
        Value_print(value);
    }

    return Value_newUndefined();        
}

Value * std_strlen(Program * program, List * values)
{
    if(List_count(values) != 1)
    {
        Program_setError(program, strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * strVal = List_at(values, 0);
    if(strVal->type != ValueType_STRING)
    {
        Program_setError(program, strdup("Invalid strlen argument type"));
        return NULL;
    }
    char * str = Value_string(strVal);
    int len = strlen(str);

    return Value_newNumber(len);    
}

Value * std_pow(Program * program, List * values)
{
    if(List_count(values) != 2)
    {
        Program_setError(program, strdup("Invalid number of pow arguments"));
        return NULL;
    }
    Value * numVal1 = List_at(values, 0);
    if(numVal1->type != ValueType_NUMBER)
    {
        Program_setError(program, strdup("Invalid pow argument type"));
        return NULL;
    }
    Value * numVal2 = List_at(values, 1);
    if(numVal2->type != ValueType_NUMBER)
    {
        Program_setError(program, strdup("Invalid pow argument type"));
        return NULL;
    }
    double num1 = Value_number(numVal1);
    double num2 = Value_number(numVal2);
    double res = pow(num1, num2);


    return Value_newNumber(res);    
}

Value * std_sqrt(Program * program, List * values)
{
    if(List_count(values) != 1)
    {
        Program_setError(program, strdup("Invalid number of sqrt arguments"));
        return NULL;
    }
    Value * numVal = List_at(values, 0);
    if(numVal->type != ValueType_NUMBER)
    {
        Program_setError(program, strdup("Invalid sqrt argument type"));
        return NULL;
    }
    double num = Value_number(numVal);
    double res = sqrt(num);

    return Value_newNumber(res);    
}

Value * std_scan(Program * program, List * values)
{
    if(List_count(values) == 0)
    {
        char buf[10000];
        buf[0] = '\0';
        int i = 0;
        while(1)
        {
            char c = getchar();
            if(c == '\n')
            {
                buf[i] = '\0';
                return Value_newString(buf); 
            }    
            else
            {
                buf[i] = c;
    
                i++;               
            }
        }
    }
    else
    {
        Program_setError(program, strdup("Invalid number of scan arguments"));
        return NULL;
    }
    
       
}

Value * std_substr(Program * program, List * values)
{
    if(List_count(values) != 3)
    {
        Program_setError(program, strdup("Invalid number of substr arguments"));
        return NULL;
    }
    Value * strVal = List_at(values, 0);
    if(strVal->type != ValueType_STRING)
    {
        Program_setError(program, strdup("Invalid substr argument type"));
        return NULL;
    }
    Value * startIndex = List_at(values, 1);
    if(startIndex->type != ValueType_NUMBER)
    {
        Program_setError(program, strdup("Invalid substr argument type"));
        return NULL;
    }
    Value * endIndex = List_at(values, 2);
    if(endIndex->type != ValueType_NUMBER)
    {
        Program_setError(program, strdup("Invalid substr argument type"));
        return NULL;
    }
    int start = Value_number(startIndex);
    int end = Value_number(endIndex);
    char buf[end - start + 2];
    char * str = Value_string(strVal);
    int index = 0;
    for(int i = start; i <= end; i++)
    {
        buf[index] = str[i];
        index++;
    }
    buf[index] = '\0';

    return Value_newString(buf);    
}

Value * std_strtonum(Program * program, List * values)
{
    if(List_count(values) != 1)
    {
        Program_setError(program, strdup("Invalid number of strtonum arguments"));
        return NULL;
    }
    Value * strVal = List_at(values, 0);
    if(strVal->type != ValueType_STRING)
    {
        Program_setError(program, strdup("Invalid strtonum argument type"));
        return NULL;
    }
    char * str = Value_string(strVal);
    char buf[1000];
    int i = 0;
    int count = 0;
    int index = 0;
    while(i < strlen(str))
    {
        if(isdigit(str[i]) || str[i] == '-')
        {
            while(i < strlen(str) && isdigit(str[i]) || str[i] == '.' || str[i] == '-')
            {
                i++;
            }
            count++;
        }
        i++;
    }


    double arr[count];
    int arrIndex = 0;
    i = 0;
    while(i < strlen(str))
    {
        if(isdigit(str[i]) || str[i] == '-')
        {
            index = 0;
            while(i < strlen(str) && (isdigit(str[i]) || str[i] == '.' || str[i] == '-'))
            {
                buf[index] = str[i];
                buf[index + 1] = '\0';
                index++;
                i++;
            }
            arr[arrIndex] = atof(buf);
            arrIndex++;
        }
        i++;
    }
    List * numArr = List_new();
    for(int i = 0; i < count; i++)
    {
        double * a = malloc(sizeof(double));
        *a = arr[i];
        List_add(numArr, a);
    }

    return Value_newArray(numArr);    
}

Value * std_numtostr(Program * program, List * values)
{
    if(List_count(values) != 1)
    {
        Program_setError(program, strdup("Invalid number of numtostr arguments"));
        return NULL;
    }
    Value * numVal = List_at(values, 0);
    if(numVal->type != ValueType_NUMBER)
    {
        Program_setError(program, strdup("Invalid numtostr argument type"));
        return NULL;
    }
    double num = Value_number(numVal);
    char buf[100];
    sprintf(buf, "%lf", num);

    return Value_newString(buf); 
}

Value * std_push(Program * program, List * values)
{
    if(List_count(values) != 2)
    {
        Program_setError(program, strdup("Invalid number of push arguments"));
        return NULL;
    }
    Value * numVal1 = List_at(values, 0);
    if(numVal1->type != ValueType_ARR)
    {
        Program_setError(program, strdup("Invalid push argument type"));
        return NULL;
    }
    List * numArr = Value_arr(numVal1);
    Value * numVal2 = List_at(values, 1);
    if(numVal2->type == ValueType_NUMBER)
    {
        double num2 = Value_number(numVal2);
        printf("num2 %lf\n", num2);
        double * a = malloc(sizeof(double));
        *a = num2;
        List_add(numArr, a);

        return Value_newArray(numArr); 
    }
    else if(numVal2->type == ValueType_STRING)
    {
        char * num2 = Value_string(numVal2);
        List_add(numArr, num2);

        return Value_newArray(numArr); 
    }
    else if(numVal2->type == ValueType_BOOL)
    {
        bool num2 = Value_bool(numVal2);
        List_add(numArr, &num2);

        return Value_newArray(numArr); 
    }
    else 
    {
        Program_setError(program, strdup("Invalid push argument type"));
        return NULL;
    }
}

Value * std_count(Program * program, List * values)
{
    if(List_count(values) != 1)
    {
        Program_setError(program, strdup("Invalid number of count arguments"));
        return NULL;
    }
    Value * numVal1 = List_at(values, 0);
    if(numVal1->type != ValueType_ARR)
    {
        Program_setError(program, strdup("Invalid count argument type"));
        return NULL;
    }
   
    List * numArr = Value_arr(numVal1);

    return Value_newNumber(numArr->length); 
}

Value * std_at(Program * program, List * values)
{
    if(List_count(values) != 2)
    {
        Program_setError(program, strdup("Invalid number of at arguments"));
        return NULL;
    }
    Value * numVal1 = List_at(values, 0);
    if(numVal1->type != ValueType_ARR)
    {
        Program_setError(program, strdup("Invalid at argument type"));
        return NULL;
    }
    Value * numVal2 = List_at(values, 1);
    if(numVal2->type != ValueType_NUMBER)
    {
        Program_setError(program, strdup("Invalid at argument type"));
        return NULL;
    }
    List * numArr = Value_arr(numVal1);
    int index = Value_number(numVal2);
    if(index >= numArr->length)
    return NULL;

    double num2 = *((double *)List_at(numArr, index));
    return Value_newNumber(num2); 

}