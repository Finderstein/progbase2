#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#include <lexer.h>
#include <list.h>


Token * Token_new(TokenType type, char * name)
{
    Token *new = malloc(sizeof(Token));
    new->type = type;
    new->name = malloc(strlen(name) + 1);
    strcpy(new->name, name);

    return new;
}


void Token_free(Token * self)
{
    free(self->name);
    free(self);
}

bool Token_equals(Token * self, Token * other)
{
    if(self->type == other->type && strlen(self->name) == strlen(other->name))
    {
        int num = strlen(self->name);
        if(!strncmp(self->name, other->name, num - 1))
        {
            return true;
        }
    }

    return false;
}


int Lexer_splitTokens(const char * input, List * self)
{
    char buf[100000];
    int index = 0;
    int i = 0;
    for(i = 0; i < strlen(input); i++)
    {
        index = 0;
        buf[index] = '\0';

        if(isalpha(input[i]) || input[i] == '_')
        {
            while(i < strlen(input) && isalpha(input[i]) || isdigit(input[i]) || input[i] == '_')
            {
                buf[index] = input[i];
                index++;
                i++;
            }

            i = i - 1;
            buf[index] = '\0';

            if(!strncmp(buf, "print", 100))
            {
                
                List_add(self, Token_new(TokenType_ID, buf));                
            }
            else if(!strncmp(buf, "scan", 100))
            {
              
                List_add(self, Token_new(TokenType_ID, buf));               
            }
            else if(!strncmp(buf, "let", 100))
            {
               
                List_add(self, Token_new(TokenType_LET, buf));              
            }
            else if(!strncmp(buf, "if", 100))
            {
               
                List_add(self, Token_new(TokenType_IF, buf));      
                       
            }
            else if(!strncmp(buf, "else", 100))
            {
                
                List_add(self, Token_new(TokenType_ELSE, buf));   
                       
            }
            else if(!strncmp(buf, "while", 100))
            {
                
                List_add(self, Token_new(TokenType_WHILE, buf));   
                   
            }
            else if(!strncmp(buf, "strtonum", 100))
            {
               
                List_add(self, Token_new(TokenType_ID, buf));  
                      
            }
            else if(!strncmp(buf, "numtostr", 100))
            {
                
                List_add(self, Token_new(TokenType_ID, buf));     
                        
            }
            else if(!strncmp(buf, "count", 100))
            {
                
                List_add(self, Token_new(TokenType_ID, buf));      
                       
            }
            else if(!strncmp(buf, "strlen", 100))
            {
                
                List_add(self, Token_new(TokenType_ID, buf));        
                
            }
            else if(!strncmp(buf, "pow", 100))
            {
               
                List_add(self, Token_new(TokenType_ID, buf));         
               
            }
            else if(!strncmp(buf, "sqrt", 100))
            {
               
                List_add(self, Token_new(TokenType_ID, buf));     
                        
            }
            else if(!strncmp(buf, "true", 100))
            {
                
                List_add(self, Token_new(TokenType_BOOL, buf));     
           
            }
            else if(!strncmp(buf, "false", 100))
            {
               
                List_add(self, Token_new(TokenType_BOOL, buf));     
                         
            }
            else
            {
                
                List_add(self, Token_new(TokenType_ID, buf));
          
            }
        }
        else if(isspace(input[i])){}
        else if(isdigit(input[i]))
        {
            int id = 0;
            while(i < strlen(input) && isdigit(input[i]) || input[i] == '.' || isalpha(input[i]) || input[i] == '_')
            {
                if(isalpha(input[i]) || input[i] == '_')
                {
                    id = 1;
                }
                buf[index] = input[i];
                index++;
                i++; 
            }
            i--;
            buf[index] = '\0';

            if(id == 1)
            {
                
                List_add(self, Token_new(TokenType_ID, buf));
            
            }
            else
            {
               
                List_add(self, Token_new(TokenType_NUMBER, buf));
          
            }

            
        }
        else if(input[i] == ',')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_COMMA, buf));

        }
        else if(input[i] == ';')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_SEMICOLON, buf));

        }
        else if(input[i] == '(')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
           
            List_add(self, Token_new(TokenType_OPBRACKET, buf));
 
        }
        else if(input[i] == ')')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
           
            List_add(self, Token_new(TokenType_CLBRACKET, buf));
 
        }
        else if(input[i] == '{')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
           
            List_add(self, Token_new(TokenType_OPBLOCK, buf));
        
        }
        else if(input[i] == '}')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
           
            List_add(self, Token_new(TokenType_CLBLOCK, buf));
        
        }
        else if(input[i] == '[')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_OPSQBRACKET, buf));
         
        }
        else if(input[i] == ']')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_CLSQBRACKET, buf));
         
        }
        else if(input[i] == '=')
        {
            if(input[i + 1] == '=')
            {
                buf[index] = '=';
                buf[index + 1] = '=';
                buf[index + 2] = '\0';
                
             
                List_add(self, Token_new(TokenType_EQUAL, buf));
                i++;
             
            }
            else
            {
                buf[index] = '=';
                buf[index + 1] = '\0';
                
                List_add(self, Token_new(TokenType_ASSIGN, buf));
             
            }
            
        }
        else if(input[i] == '!')
        {
            if(input[i + 1] == '=')
            {
                buf[index] = '!';
                buf[index + 1] = '=';
                buf[index + 2] = '\0';
                
             
                List_add(self, Token_new(TokenType_NOTEQUAL, buf));
                i++;
             
            }
            else
            {
                buf[index] = '!';
                buf[index + 1] = '\0';
                
                List_add(self, Token_new(TokenType_NOT, buf));
            }
        }
        else if(input[i] == '<')
        {
            if(input[i + 1] == '=')
            {
                buf[index] = '<';
                buf[index + 1] = '=';
                buf[index + 2] = '\0';
                
               
                List_add(self, Token_new(TokenType_EQLESS, buf));
                i++;
               
            }
            else
            {
                buf[index] = '<';
                buf[index + 1] = '\0';
                
                List_add(self, Token_new(TokenType_LESS, buf));
             
            }
        }
        else if(input[i] == '>')
        {
            if(input[i + 1] == '=')
            {
                buf[index] = '>';
                buf[index + 1] = '=';
                buf[index + 2] = '\0';
                
                
                List_add(self, Token_new(TokenType_EQMORE, buf));
                i++;
             
            }
            else
            {
                buf[index] = '>';
                buf[index + 1] = '\0';
                
                List_add(self, Token_new(TokenType_MORE, buf));
              
            }
        }
        else if(input[i] == '&')
        {
            if(input[i + 1] != '&') return 1;
            buf[index] = '&';
            buf[index + 1] = '&';
            buf[index + 2] = '\0';
            
            List_add(self, Token_new(TokenType_AND, buf));
            i++;
         
        }
        else if(input[i] == '|')
        {
            if(input[i + 1] != '|') return 1;
            buf[index] = '|';
            buf[index + 1] = '|';
            buf[index + 2] = '\0';
            
            List_add(self, Token_new(TokenType_OR, buf));
            i++;
          
        }
        else if(input[i] == '+')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_PLUS, buf));
    
        }
        else if(input[i] == '-')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_MINUS, buf));
     
        }
        else if(input[i] == '*')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_MULT, buf));
       
        }
        else if(input[i] == '/')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_DIV, buf));
       
        }
        else if(input[i] == '%')
        {
            buf[index] = input[i];
            buf[index + 1] = '\0';
            
            List_add(self, Token_new(TokenType_MOD, buf));
     
        }
        else if(input[i] == '"')
        {
            i++;
            while(input[i] != '"' && i < strlen(input))
            {
                buf[index] = input[i];
                index++;
                i++; 
            }
            if(i == strlen(input)) return 1;
            buf[index] = '\0';

           
            List_add(self, Token_new(TokenType_STR, buf)); 
    
        }
        else
        {
            Token *tok = Token_new(TokenType_UNKNOWN, buf);
            List_add(self, tok);
           
        }
    }

    return 0;
}

void Lexer_clearTokens(List * self)
{
    for(int i = 0; i < self->length; i++)
    {
        Token_free(self->tokens[i]);
    }
}


void Lexer_printTokensToFile(List * self)
{
    const char *fileName = "../main_tokens.txt";
    FILE *f = fopen(fileName, "w");
    if (!f) return;  // read 0 bytes from file

    for(int i = 0; i < self->length; i++)
    {
        Token *tok = self->tokens[i];
        if(tok->type == TokenType_NEWLINE)
        {
            fwrite("\n", strlen("\n"), 1, f);
        }
        else
        {
            fwrite("<", strlen("<"), 1, f);
            if(tok->type == TokenType_ID)
            {
                fwrite("ID,", strlen("ID,"), 1, f);
            }
            if(tok->type == TokenType_STR)
            {
                fwrite("STR,", strlen("STR,"), 1, f);
            }
            if(tok->type == TokenType_NUMBER)
            {
                fwrite("NUM,", strlen("NUM,"), 1, f);
            }
            fwrite(tok->name, strlen(tok->name), 1, f);
            fwrite(">", strlen(">"), 1, f);    
            fwrite("  ", strlen("  "), 1, f);  
        }

         
    }

    fclose(f);
}

void Lexer_printInConsoleTokens(List * self)
{
    Iterator * begin = List_getNewBeginIterator(self);
    Iterator * end = List_getNewEndIterator(self);    

    while(!Iterator_equals(begin, end))
    {
        
        Token * token = Iterator_value(begin);

        if(token->type == TokenType_NEWLINE)
        {
            printf("\n");
        }
        else
        {
            printf("<");
            if(token->type == TokenType_ID)
            {
                printf("ID,");
            }
            if(token->type == TokenType_STR)
            {
                printf("STR,");
            }
            if(token->type == TokenType_NUMBER)
            {
                printf("NUM,");
            }
            printf("%s", token->name);
            printf(">");    
            printf("  "); 
            
        }
        

        Iterator_next(begin);
    }

    Iterator_free(begin);
    Iterator_free(end);    

}

