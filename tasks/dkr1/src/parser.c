#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <string_ext.h>

#include <list.h>
#include <lexer.h>
#include <parser.h>
#include <iterator.h>
#include <string_buffer.h>
#include <ast.h>



typedef struct {
    Iterator * tokens;
    Iterator * tokensEnd;
    char * error;
    // optional extra fields
} Parser;

// check current token type
static Tree * accept(Parser * self, TokenType tokenType);
// expect exactly that token type or fail
static Tree * expect(Parser * self, TokenType tokenType);

//

static void TokenType_buf(TokenType type, char *buf)
{
    switch(type)
    {
        case TokenType_PLUS: strcpy(buf, "PLUS"); break;
        case TokenType_MINUS: strcpy(buf, "MINUS"); break;
        case TokenType_MULT: strcpy(buf, "MULT"); break;
        case TokenType_DIV: strcpy(buf, "DIV"); break;
        case TokenType_POW: strcpy(buf, "POW"); break;
        case TokenType_LET: strcpy(buf, "LET"); break;                
        case TokenType_ID: strcpy(buf, "ID"); break;        
        case TokenType_STR: strcpy(buf, "STR"); break;                
        case TokenType_NUMBER: strcpy(buf, "NUMBER"); break;        
        case TokenType_OPBRACKET: strcpy(buf, "OPBRACKET"); break;
        case TokenType_CLBRACKET: strcpy(buf, "CLBRACKET"); break;        
        case TokenType_OPSQBRACKET: strcpy(buf, "OPSQBRACKET"); break;
        case TokenType_CLSQBRACKET: strcpy(buf, "CLSQBRACKET"); break;                
        case TokenType_OPBLOCK: strcpy(buf, "OPBLOCK"); break;
        case TokenType_CLBLOCK: strcpy(buf, "CLBLOCK"); break; 
        case TokenType_COMMA: strcpy(buf, "COMMA"); break;               
        case TokenType_SEMICOLON: strcpy(buf, "SEMICOLON"); break;
        case TokenType_NEWLINE: strcpy(buf, "NEWLINE"); break;        
        case TokenType_WS: strcpy(buf, "WS"); break;
        case TokenType_IF: strcpy(buf, "IF"); break;
        case TokenType_WHILE: strcpy(buf, "WHILE"); break;
        case TokenType_ELSE: strcpy(buf, "ELSE"); break;
        case TokenType_SQRT: strcpy(buf, "SQRT"); break;
        case TokenType_STRLEN: strcpy(buf, "STRLEN"); break;
        case TokenType_COUNT: strcpy(buf, "COUNT"); break;
        case TokenType_PRINT: strcpy(buf, "PRINT"); break;
        case TokenType_SCAN: strcpy(buf, "SCAN"); break;
        case TokenType_STRTONUM: strcpy(buf, "STRTONUM"); break;
        case TokenType_NUMTOSTR: strcpy(buf, "NUMTOSTR"); break;
        case TokenType_AND: strcpy(buf, "AND"); break;
        case TokenType_OR: strcpy(buf, "OR"); break;
        case TokenType_EQUAL: strcpy(buf, "EQUAL"); break;
        case TokenType_MORE: strcpy(buf, "MORE"); break;
        case TokenType_LESS: strcpy(buf, "LESS"); break;
        case TokenType_NOT: strcpy(buf, "NOT"); break;
        case TokenType_ASSIGN: strcpy(buf, "ASSIGN"); break;
        case TokenType_EQMORE: strcpy(buf, "EQMORE"); break;
        case TokenType_EQLESS: strcpy(buf, "EQLESS"); break;
        case TokenType_MOD: strcpy(buf, "MOD"); break;
        case TokenType_UNKNOWN: strcpy(buf, "UNKNOWN"); break;
        default: break;
    }
}
//


static Tree * prog       (Parser * parser);
static Tree * var_decl   (Parser * parser);
static Tree * st         (Parser * parser);
static Tree * expr       (Parser * parser);
static Tree * expr_st    (Parser * parser);
static Tree * block_st   (Parser * parser);
static Tree * select_st  (Parser * parser);
static Tree * iter_st    (Parser * parser);
static Tree * assign     (Parser * parser);
static Tree * assign_ap  (Parser * parser);
static Tree * log_or     (Parser * parser);
static Tree * log_or_ap  (Parser * parser);
static Tree * log_and    (Parser * parser);
static Tree * log_and_ap (Parser * parser);
static Tree * eq         (Parser * parser);
static Tree * eq_ap      (Parser * parser);
static Tree * rel        (Parser * parser);
static Tree * rel_ap     (Parser * parser);
static Tree * add        (Parser * parser);
static Tree * add_ap     (Parser * parser);
static Tree * mult       (Parser * parser);
static Tree * mult_ap    (Parser * parser);
static Tree * unary      (Parser * parser);
static Tree * primary    (Parser * parser);
static Tree * ID         (Parser * parser);
static Tree * NUMBER     (Parser * parser);
static Tree * STRING     (Parser * parser);
static Tree * var_or_call(Parser * parser);
static Tree * parentheses(Parser * parser);
static Tree * fn_call    (Parser * parser);
static Tree * arg_list   (Parser * parser);


//

Tree * Parser_buildNewAstTree(List * tokens)
{
    Parser parser =
    {
        .tokens = List_getNewBeginIterator(tokens),
        .tokensEnd = List_getNewEndIterator(tokens),
        .error = NULL
    };

    Tree * progNode = prog(&parser);

    if(parser.error)
    {
        fprintf(stderr,">>>> ERROR: %s\n", parser.error);
        free((void*)parser.error);
        Iterator_free(parser.tokens);
        Iterator_free(parser.tokensEnd);  
        //Tree_clear(progNode);
        return progNode;
    }
    else if(!Iterator_equals(parser.tokens, parser.tokensEnd))
    {
        Token * token = Iterator_value(parser.tokens);
        char type[100];
        TokenType_buf(token->type, type);
        fprintf(stderr,">>>> ERROR: unexpected token '%s'\n", type);        
    }

    Iterator_free(parser.tokens);
    Iterator_free(parser.tokensEnd);    
    return progNode;
}




static bool eoi(Parser * self)
{
    return Iterator_equals(self->tokens, self->tokensEnd);
}


static AstNodeType TokenType_toAstNodeType(TokenType type)
{
    switch (type)
    {
        case TokenType_ASSIGN: return AstNodeType_ASSIGN;
        case TokenType_PLUS: return AstNodeType_ADD;
        case TokenType_MINUS: return AstNodeType_SUB;
        case TokenType_MULT: return AstNodeType_MULT;
        case TokenType_DIV: return AstNodeType_DIV;
        case TokenType_MOD: return AstNodeType_MOD;
        case TokenType_EQUAL: return AstNodeType_EQUAL;
        case TokenType_NOTEQUAL: return AstNodeType_NOTEQUAL;
        case TokenType_NOT: return AstNodeType_NOT;
        case TokenType_LESS: return AstNodeType_LESS;
        case TokenType_MORE: return AstNodeType_MORE;
        case TokenType_EQLESS: return AstNodeType_EQLESS;
        case TokenType_EQMORE: return AstNodeType_EQMORE;
        case TokenType_AND: return AstNodeType_AND;
        case TokenType_OR: return AstNodeType_OR;   
        //     
        case TokenType_ID: return AstNodeType_ID;
        case TokenType_NUMBER: return AstNodeType_NUMBER;
        case TokenType_STR: return AstNodeType_STRING;  
        case TokenType_BOOL: return AstNodeType_BOOL;      
        //  
        default: return AstNodeType_UNKNOWN;
    }
}





// check current token type
static Tree * accept(Parser * self, TokenType tokenType)
{
    if(eoi(self)) return NULL;
    Token * token = Iterator_value(self->tokens);
    if(token->type == tokenType)
    {
        AstNodeType astType = TokenType_toAstNodeType(tokenType);
        char * astName = strdup((char *)token->name);
        AstNode * node = AstNode_new(astType, astName);
        Tree * tree = Tree_new(node);
        Iterator_next(self->tokens);
        return tree;
    }
    return NULL;
}






// expect exactly that token type or fail
static Tree * expect(Parser * parser, TokenType tokenType)
{
    Tree * tree = accept(parser , tokenType);
    if(tree != NULL)
    {
        
        return tree;
    }
    char currentTokenType[100];

    if(eoi(parser))
    {
        strcpy(currentTokenType, "end-of-input");
    }
    else
    {
        TokenType type = ((Token *)Iterator_value(parser->tokens))->type;
        TokenType_buf(type, currentTokenType);
    }
    StringBuffer * sb = StringBuffer_new();
    char expTokenType[100];    
    TokenType_buf(tokenType, expTokenType);
    StringBuffer_appendFormat(sb, "expected '%s' got '%s'.\n", expTokenType, currentTokenType);
    parser->error = StringBuffer_toNewString(sb);
    StringBuffer_free(sb);
    return NULL;
}



//ebnf

typedef Tree * (*GrammarRule)(Parser * parser);
    //fprintf(stderr, "expected '%s' got '%s'.\n", TokenType_buf(tokenType), currentTokenType);
// EBNF: A = { B };
static bool ebnf_multiple      (Parser * parser, List * nodes, GrammarRule rule)
{
    Tree * node = NULL;
    while((node = rule(parser)) && !parser->error)
    {
        List_add(nodes, node);
    }

    return parser->error == NULL ? true : false;
}

static Tree * ebnf_select        (Parser * parser, GrammarRule rules[], size_t rulesLen)
{ 
    Tree * node = NULL;
    for(int i = 0; i < rulesLen && !node; i++)
    {
        GrammarRule rule = rules[i];
        node = rule(parser);
        if(parser->error) return NULL;
    }
    return node;
}

static Tree * ebnf_selectLexemes (Parser * parser, TokenType types[], size_t typesLen)
{
    Tree * node = NULL;
    for(int i = 0; i < typesLen && !node; i++)
    {
        node = accept(parser, types[i]);
    }

    return node;
}

// EBNF: A = bA';
static Tree * ebnf_ap_main_rule(Parser * parser, GrammarRule next, GrammarRule ap)
{
    Tree * nextNode = next(parser); 
    if(nextNode)
    {
        Tree * apNode = ap(parser);
        if(apNode)
        {
            List_insert(apNode->children, nextNode, 0);
            return apNode;
        }
        return nextNode;
    }
    return NULL;
}
// EBNF: A' = aA' | e;
static Tree * ebnf_ap_recursive_rule(Parser * parser, TokenType types[], size_t typesLen, GrammarRule next, GrammarRule ap)
{
    Tree * opNode = ebnf_selectLexemes(parser, types, typesLen);
    if(opNode == NULL) return NULL;
    Tree * node = NULL;
    Tree * nextNode = next(parser);
    Tree * apNode = ap(parser);
    if(apNode)
    {
        List_insert(apNode->children, nextNode, 0);
        node = apNode;
    }
    else
    {
        node = nextNode;
    }
    List_add(opNode->children, node);

    return opNode;
}



static Tree * ID         (Parser * parser)
{
    return accept(parser, TokenType_ID);
}

static Tree * NUMBER     (Parser * parser)
{
    return accept(parser, TokenType_NUMBER);
}

static Tree * STRING     (Parser * parser)
{ 
    return accept(parser, TokenType_STR);    
}

static Tree * BOOL       (Parser * parser)
{
    return accept(parser, TokenType_BOOL);    
}



static Tree * ID_expect         (Parser * parser)
{
    return expect(parser, TokenType_ID);
}

static Tree * NUMBER_expect     (Parser * parser)
{
    return expect(parser, TokenType_NUMBER);
}

static Tree * STRING_expect     (Parser * parser)
{
    return expect(parser, TokenType_STR);    
}

static Tree * BOOL_expect     (Parser * parser)
{
    return expect(parser, TokenType_BOOL);    
}



static Tree * prog       (Parser * parser)
{

    Tree * progNode = Tree_new(AstNode_new(AstNodeType_PROGRAM, strdup("program")));
    (void)ebnf_multiple(parser, progNode->children, var_decl);
    (void)ebnf_multiple(parser, progNode->children, st);
    return progNode;
}
static Tree * var_decl   (Parser * parser)
{
 
    Tree * letNode = accept(parser, TokenType_LET);
    if(!letNode) 
    {
        
        return NULL; 
    }
    Tree_clear(letNode);

    Tree * idNode = ID_expect(parser);
    if(!idNode)
    {        
        return NULL;
    }

    Tree * opSqBrNode = accept(parser, TokenType_OPSQBRACKET);
    if(opSqBrNode)
    {
        Tree_clear(opSqBrNode);
        Tree * clSqBrNode = expect(parser, TokenType_CLSQBRACKET);
        if(!clSqBrNode)
        {
            Tree_clear(idNode);                
            //@todo error
            return NULL;
        }
        Tree_clear(clSqBrNode);

        Tree * semNode = expect(parser, TokenType_SEMICOLON);
        if(!semNode)
        {
            Tree_clear(idNode); 
                           
            //@todo error
            return NULL;
        }
        Tree_clear(semNode);

        AstNodeType astType = AstNodeType_ARRBR;
        char * astName = strdup("[]");
        AstNode * node = AstNode_new(astType, astName);
        Tree * arr = Tree_new(node);

        Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strdup("declareVar")));
        List_add(varDecl->children, idNode);  
        List_add(varDecl->children, arr);
    
        return varDecl;
    }

    Tree * asNode = expect(parser, TokenType_ASSIGN);
    if(!asNode)
    {
        Tree_clear(idNode);                
        return NULL;
    }
    Tree_clear(asNode);

    Tree * exprNode = expr(parser);
    if(exprNode == NULL)
    {   
        Tree_clear(idNode);                  
        return NULL;
    }
    Tree * semNode = expect(parser, TokenType_SEMICOLON);
    if(!semNode)
    {
        Tree_clear(idNode); 
        Tree_clear(exprNode);
                       
        return NULL;
    }
    Tree_clear(semNode); 

    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strdup("declareVar")));
    List_add(varDecl->children, idNode);
    List_add(varDecl->children, exprNode);    

    return varDecl;
}
static Tree * st         (Parser * parser)
{
  
    return ebnf_select(parser, 
    (GrammarRule[])
    {
        block_st,
        select_st,
        iter_st,
        expr_st
    }, 
    4);
}

static Tree * expr       (Parser * parser)
{
   
    return assign(parser);
}

static Tree * expr_st    (Parser * parser)
{
  
    Tree * exprNode = expr(parser);
    if(exprNode)
    {
        
        Tree * semNode = expect(parser, TokenType_SEMICOLON);
        if(semNode) 
        {
            Tree_clear(semNode);
        }
    }
    else
    {
       
        Tree * semNode = accept(parser, TokenType_SEMICOLON);
        if(semNode) 
        {
            Tree_clear(semNode);
        }
    }

    return exprNode;
}

static Tree * block_st   (Parser * parser)
{

    Tree * opblNode = accept(parser, TokenType_OPBLOCK);
    if(!opblNode) 
    {
        return NULL;
    }
    Tree_clear(opblNode);

    Tree * blockNode = Tree_new(AstNode_new(AstNodeType_BLOCK, strdup("block")));
    if(ebnf_multiple(parser, blockNode->children, st))
    {
        Tree * clblNode = expect(parser, TokenType_CLBLOCK);
        if(clblNode) 
        {
            Tree_clear(clblNode);
        }
        
    }
    return blockNode;
}

static Tree * select_st  (Parser * parser)
{
  
    Tree * if1Node = accept(parser, TokenType_IF);
    
    if(!if1Node) 
    {
        
        return NULL;
    }
    Tree_clear(if1Node);

    Tree * brNode = expect(parser, TokenType_OPBRACKET);
    if(!brNode) 
    {     
        return NULL;
    }
    Tree_clear(brNode);

    Tree * testExprNode = expr(parser);
    if(!testExprNode)
    {             
        return NULL;
    }
    Tree * clbrNode = expect(parser, TokenType_CLBRACKET);
    if(!clbrNode)
    {
        Tree_clear(testExprNode); 
        return NULL;
    } 
    Tree_clear(clbrNode);

    Tree * stNode = st(parser);
    if(stNode == NULL)
    {
        Tree_clear(testExprNode); 
        return NULL;
    }
    Tree * ifNode = Tree_new(AstNode_new(AstNodeType_IF, strdup("if")));
    List_add(ifNode->children, testExprNode);
    List_add(ifNode->children, stNode);        

    Tree * elNode = accept(parser, TokenType_ELSE);
    if(elNode)
    {
        Tree_clear(elNode); 
        Tree * elseNode = st(parser);
        if(elseNode == NULL || parser->error)
        {
            return NULL;
        }
        List_add(ifNode->children, elseNode);
    }

    return ifNode;
}

static Tree * iter_st    (Parser * parser)
{

    Tree * whNode = accept(parser, TokenType_WHILE);
    
    if(!whNode) 
    {
        
        return NULL;
    }
    Tree_clear(whNode);

    Tree * brNode = expect(parser, TokenType_OPBRACKET);
    if(!brNode) 
    {
        
        return NULL;
    }
    Tree_clear(brNode);

    Tree * testExprNode = expr(parser);
    if(!testExprNode)
    {             
        return NULL;
    }
    Tree * clbrNode = expect(parser, TokenType_CLBRACKET);
    if(!clbrNode)
    {
        Tree_clear(testExprNode); 
        return NULL;
    } 
    Tree_clear(clbrNode);

    Tree * stNode = st(parser);
    if(stNode == NULL)
    {
        Tree_clear(testExprNode); 
        return NULL;
    }
    Tree * whileNode = Tree_new(AstNode_new(AstNodeType_WHILE, strdup("while")));
    List_add(whileNode->children, testExprNode);
    List_add(whileNode->children, stNode);        

    return whileNode;

}

static Tree * assign     (Parser * parser)
{
    return ebnf_ap_main_rule(parser, log_or, assign_ap);
}

static Tree * assign_ap  (Parser * parser)
{

    return ebnf_ap_recursive_rule(parser, 
        (TokenType[])
        {
            TokenType_ASSIGN, 
        }, 1,
        log_or, assign_ap);
}

static Tree * log_or     (Parser * parser)
{
    return ebnf_ap_main_rule(parser, log_and, log_or_ap);
}

static Tree * log_or_ap  (Parser * parser)
{
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[])
        {
            TokenType_OR, 
        }, 1,
        log_and, log_or_ap);
}
static Tree * log_and    (Parser * parser)
{
    return ebnf_ap_main_rule(parser, eq, log_and_ap);
}

static Tree * log_and_ap (Parser * parser)
{
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[])
        {
            TokenType_AND, 
        }, 1,
        eq, log_and_ap);
}
static Tree * eq         (Parser * parser)
{
    return ebnf_ap_main_rule(parser, rel, eq_ap);
}

static Tree * eq_ap      (Parser * parser)
{
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[])
        {
            TokenType_EQUAL,
            TokenType_NOTEQUAL
        }, 2,
        rel, eq_ap);
}

static Tree * rel        (Parser * parser)
{
    return ebnf_ap_main_rule(parser, add, rel_ap);
}

static Tree * rel_ap     (Parser * parser)
{
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[])
        {
            TokenType_LESS,
            TokenType_EQLESS,
            TokenType_MORE,
            TokenType_EQMORE,
        }, 4,
        add, rel_ap);
}

static Tree * add        (Parser * parser)
{
    return ebnf_ap_main_rule(parser, mult, add_ap);
}

static Tree * add_ap     (Parser * parser)
{
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[])
        {
            TokenType_PLUS,
            TokenType_MINUS,
        }, 2,
        mult, add_ap);
}

static Tree * mult       (Parser * parser)
{
    return ebnf_ap_main_rule(parser, unary, mult_ap);
}

static Tree * mult_ap    (Parser * parser)
{
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[])
        {
            TokenType_MULT,
            TokenType_DIV,
            TokenType_MOD,
        }, 3,
        unary, mult_ap);
}

static Tree * unary      (Parser * parser)
{
    Tree * opNode = ebnf_selectLexemes(parser, 
        (TokenType[])
        {
            TokenType_PLUS,
            TokenType_MINUS,
            TokenType_NOT,
        }, 3);

        Tree * primNode = primary(parser);
        if(opNode)
        {
            List_add(opNode->children, primNode);
            return opNode;
        }
        return primNode;
}

static Tree * primary    (Parser * parser)
{
    return ebnf_select(parser, 
        (GrammarRule[])
        {
            NUMBER,
            STRING,
            BOOL,
            var_or_call,
            parentheses
        }, 5);
}

static Tree * var_or_call(Parser * parser)
{
    Tree * varNode = ID(parser);
    Tree * argListNode = fn_call(parser);
    if(argListNode)
    {
        List_add(varNode->children, argListNode);        
    }
    return varNode;
}

static Tree * parentheses(Parser * parser)
{
    Tree * opbrNode = accept(parser, TokenType_OPBRACKET);
    if(!opbrNode) return NULL;
    Tree_clear(opbrNode);

    Tree * exprNode = arg_list(parser);

    Tree * clbrNode = expect(parser, TokenType_CLBRACKET);
    if(clbrNode) Tree_clear(clbrNode);
    return exprNode;
}

static Tree * fn_call    (Parser * parser)
{
    Tree * opbrNode = accept(parser, TokenType_OPBRACKET);
    if(!opbrNode) return NULL;
    Tree_clear(opbrNode);

    Tree * argListNode = arg_list(parser);
    Tree * clbrNode = expect(parser, TokenType_CLBRACKET);
    if(clbrNode) Tree_clear(clbrNode); 
    return argListNode;
}
static Tree * arg_list   (Parser * parser)
{
    Tree * exprNode =  expr(parser);
    Tree * argListNode = Tree_new(AstNode_new(AstNodeType_ARGLIST, strdup("arglist")));
    if(exprNode)
    {
        List_add(argListNode->children, exprNode);
        while(true)
        {
            Tree * commaNode = accept(parser, TokenType_COMMA);
            if(!commaNode) 
            {
                break;
            }
            Tree_clear(commaNode);

            exprNode = expr(parser);
            if(exprNode)
            {
                List_add(argListNode->children, exprNode);
            }
            else
            {
                break;
            }
        }
    }
    return argListNode;
}













