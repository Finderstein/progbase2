#include <prettyPrint.h>

#include <ast.h>
#include <ctype.h>
#include <string.h>
#include <string_ext.h>
#include <assert.h>
#include <stdbool.h>

//Print to console


void AstTree_prettyPrint(Tree * astTree)
{
    char * indent = strdup("");
    PrintPretty(astTree, indent, 1, 1);
    free((void *)indent);
}

static char * str_append(const char * str, const char * append)
{
    size_t newLen = strlen(str) + strlen(append) + 1;
    char * buf = malloc(sizeof(char) * newLen);
    buf[0] = '\0';
    sprintf(buf, "%s%s", str, append);
    return buf;
}


static void PrintPretty(Tree * node, const char * indent, int root, int last)
{
    printf("%s", indent);
    char * newIndent = NULL;
    if(last)
    {
        if(!root)
        {
            printf("└─");
            newIndent = str_append(indent, "►►");
        }
        else
        {
            newIndent = str_append(indent, "");
        }
    }
    else
    {
        printf("├─");
        newIndent = str_append(indent, "│►");
    }
    AstNode * astNode = node->value ;
    printf("%s\n", astNode->name);
    List * children = node->children;
    size_t count = List_count(children);
    for(int i = 0; i < count; i++)
    {
        void * child = List_at(children, i);
        PrintPretty(child, newIndent, 0, i == count - 1);
    }
    free((void *)newIndent);
}







//Write to File

void AstTree_prettyWriteToFile(Tree * astTree, const char * newFile)
{
    FILE * f = fopen(newFile, "w");
    if (!f) return; 

    char * indent = strdup("");
    WritePrettyToFile(astTree, indent, 1, 1, f);
    free((void *)indent);

    fclose(f);
}

char * str_appendToFile(const char * str, const char * append)
{
    size_t newLen = strlen(str) + strlen(append) + 1;
    char * buf = malloc(sizeof(char) * newLen);
    buf[0] = '\0';
    strcat(buf, str);
    strcat(buf, append);

    return buf;
}

void WritePrettyToFile(Tree * node, const char * indent, int root, int last, FILE *f)
{
    fprintf(f,"%s",indent);
    char * newIndent = NULL;

    if(last)
    {
        if(!root)
        {
            fprintf(f,"└─");
            newIndent = str_appendToFile(indent, "►►");
        }
        else
        {
            newIndent = str_appendToFile(indent, "");
        }
    }
    else
    {
        fprintf(f,"├─");
        newIndent = str_appendToFile(indent, "│►");
    }
    AstNode * astNode = node->value ;
    fprintf(f,"%s\n", astNode->name);
    List * children = node->children;
    size_t count = List_count(children);

    for(int i = 0; i < count; i++)
    {
        Tree * child = List_at(children, i);
        WritePrettyToFile(child, newIndent, 0, i == count - 1, f);
    }


    free((void *)newIndent);
}