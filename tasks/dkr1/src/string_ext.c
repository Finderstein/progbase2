#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char *strdup (const char *s)  //https://stackoverflow.com/questions/252782/strdup-what-does-it-do-in-c
{
    char *d = malloc (strlen (s) + 1);   // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy (d,s);                        // Copy the characters
    return d;                            // Return the new string
}