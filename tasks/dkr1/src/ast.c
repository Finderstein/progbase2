#include <ast.h>
#include <stdlib.h>

AstNode * AstNode_new(AstNodeType type, char * name)
{
    AstNode * self = malloc(sizeof(AstNode));
    self->type = type;
    self->name = name;

    return self;
}

void AstNode_free(AstNode * self)
{
    free(self);
}

void AstNode_free_name(AstNode * self)
{
    if(self == NULL) return;
    if(self->name != NULL)
    {
        free(self->name);
    }
    
    free(self);
}