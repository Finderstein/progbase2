#include <dict.h>
#include <bstree.h>



KeyValuePair1 * Kvp_new(char * key, void * value)
{
    KeyValuePair1 * self = malloc(sizeof(KeyValuePair1));
    self->key = key;
    self->value = value;

    return self;
}

void Kvp_free(KeyValuePair1 * self)
{
    free(self);
}

struct __Dict 
{
    BSTree * bst;
};

Dict * Dict_new(void)
{
    Dict * self = malloc(sizeof(Dict));
    self->bst = BSTree_new();
    
    return self;
}

void Dict_free(Dict * self)
{
    BSTree_free(self->bst);
    free(self);
}

static int hashString(char *str)
{
   int hash = 5381;
   int c;

   while (c = *str++)
       hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

   return hash;
}


void   Dict_add      (Dict * self, char * key, void * value)
{
    KeyValuePair1 * pair = Kvp_new(key, value);
    int intKey = hashString(key);
    BSTree_insert(self->bst, intKey, pair);    
}

bool   Dict_contains (Dict * self, char * key)
{
    int intKey = hashString(key);
    return BSTree_lookup(self->bst, intKey);
}

void * Dict_get      (Dict * self, char * key)
{
    int intKey = hashString(key);
    KeyValuePair1 * pair = BSTree_search(self->bst, intKey);
    return pair->value;
}

void * Dict_set      (Dict * self, char * key, void * value)
{
    int intKey = hashString(key);
    free(key);
    KeyValuePair1 * pair = BSTree_search(self->bst, intKey);
    void * oldValue = pair->value; 
    pair->value = value;
    return oldValue;
}

void * Dict_remove   (Dict * self, char * key)
{
    int intKey = hashString(key);
    KeyValuePair1 * pair = BSTree_delete(self->bst, intKey);
    return pair->value; 
}

void   Dict_clear    (Dict * self)
{
    BSTree_clear(self->bst);
} 

void   Dict_clear1    (Dict * self)
{
    BSTree_clear1(self->bst);
} 

size_t Dict_count    (Dict * self)
{
    return BSTree_count(self->bst);
}


void   Dict_keys     (Dict * self, List * keys)
{
    List * pairs = List_new();
    BSTree_values(self->bst, pairs);
    for(int i = 0; i < List_count(pairs); i++)
    {
        KeyValuePair1 * pair = List_at(pairs, i);
        List_add(keys, pair->key);
    }
    List_free(pairs);
}

void   Dict_values   (Dict * self, List * values)
{
    List * pairs = List_new();
    BSTree_values(self->bst, pairs);
    for(int i = 0; i < List_count(pairs); i++)
    {
        KeyValuePair1 * pair = List_at(pairs, i);
        List_add(values, pair->value);
    }
    List_free(pairs);
}
