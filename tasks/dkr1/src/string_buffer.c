#include <string_buffer.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string_ext.h>
#include <stdarg.h>
#include <assert.h>


struct __StringBuffer 
{
    char* array;
    size_t capacity;
    size_t length;
};

static const size_t  INITIAL_CAPACITY = 256;

StringBuffer * StringBuffer_new(void)
{
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->array = malloc(sizeof(char) * self->capacity);    
    StringBuffer_clear(self);
    
    return self;
}

void StringBuffer_free(StringBuffer * self)
{
    free(self->array);
    free(self);
}

static void ensureCapacity(StringBuffer *self, size_t appendLength)
{
    while(self->length + appendLength > self->capacity)
    {
        size_t newCapacity = self->capacity * 2;
        char *newMem = realloc(self->array, sizeof(char) * newCapacity);
        self->array = newMem;
        self->capacity = newCapacity;
    }
}


void StringBuffer_append(StringBuffer * self, const char * str)
{
    size_t strLen = strlen(str);
    ensureCapacity(self, strLen);
    strcat(self->array + self->length - 1, str);
    self->length += strLen;

}

void StringBuffer_appendChar(StringBuffer * self, char ch)
{
    ensureCapacity(self, 1);    
    self->array[self->length - 1] = ch;
    self->array[self->length] = '\0';  
    self->length++;  
}

void StringBuffer_appendFormat(StringBuffer * self, const char * fmt, ...)
{
    va_list vlist;
    va_start(vlist, fmt);
    //https://stackoverflow.com/questions/29087129/how-to-calculate-the-length-of-output-that-sprintf-will-generate/29087251#29087251
    size_t bufsz = vsnprintf(NULL, 0, fmt, vlist);
    char* buf = malloc(bufsz + 1);
    va_start(vlist, fmt);    
    vsnprintf(buf, bufsz + 1, fmt, vlist);
    va_end(vlist);
    StringBuffer_append(self, buf);
    free(buf);
}

void StringBuffer_clear(StringBuffer * self)
{
    self->array[0] = '\0';
    self->length = 1;    
}

static char *str_dup (const char *s)  //https://stackoverflow.com/questions/252782/strdup-what-does-it-do-in-c
{
    char *d = malloc (strlen (s) + 1);   // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy (d,s);                        // Copy the characters
    return d;                            // Return the new string
}

char * StringBuffer_toNewString(StringBuffer * self)
{
    return str_dup(self->array);
}


