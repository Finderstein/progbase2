#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <check.h>

#include <list.h>
#include <lexer.h>
#include <parser.h>
#include <ast.h>

START_TEST (split_parser_notNull)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 5);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    ck_assert(root != NULL);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST

START_TEST (split_parser_lenOne)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 5);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    ck_assert_int_eq(List_count(root->children), 1);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST


START_TEST (split_parser_program)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 5);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    AstNode * node = root->value;
    ck_assert(!strcmp(node->name, "program"));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST

START_TEST (split_parser_firstDeclareVar)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 5);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    Tree * decNode = List_at(root->children, 0);
    AstNode * node = decNode->value;
    ck_assert(!strcmp(node->name, "declareVar"));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST

START_TEST (split_parser_secondDeclareVar)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;\nlet br = 3 + 5;", tokens);
    ck_assert_int_eq(status, 0);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    Tree * decNode = List_at(root->children, 1);
    AstNode * node = decNode->value;
    ck_assert(!strcmp(node->name, "declareVar"));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST

START_TEST (split_parser_assign)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;\ntr = 3 + 5;", tokens);
    ck_assert_int_eq(status, 0);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    Tree * decNode = List_at(root->children, 1);
    AstNode * node = decNode->value;
    ck_assert(!strcmp(node->name, "="));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST

START_TEST (split_parser_print)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;\nprint(tr);", tokens);
    ck_assert_int_eq(status, 0);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    Tree * decNode = List_at(root->children, 1);
    AstNode * node = decNode->value;
    ck_assert(!strcmp(node->name, "print"));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST

START_TEST (split_parser_firstID)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 5);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * root = Parser_buildNewAstTree(tokens);
    ck_assert_int_eq(List_count(root->children), 1);
    Tree * decNode = List_at(root->children, 0);
    Tree * trNode = List_at(decNode->children, 0);    
    AstNode * node = trNode->value;
    ck_assert(!strcmp(node->name, "tr"));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(root);
}
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Lexer");
    TCase *tc_sample = tcase_create("SplitTest");
    //
    tcase_add_test(tc_sample, split_parser_notNull);  
    tcase_add_test(tc_sample, split_parser_program);
    tcase_add_test(tc_sample, split_parser_firstDeclareVar);  
    tcase_add_test(tc_sample, split_parser_secondDeclareVar); 
    tcase_add_test(tc_sample, split_parser_assign);  
    tcase_add_test(tc_sample, split_parser_print);        
    tcase_add_test(tc_sample, split_parser_firstID); 
    tcase_add_test(tc_sample, split_parser_lenOne);           
    
    
    //
    suite_add_tcase(s, tc_sample);
    return s;
}
   
int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);
   
    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}
   