#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <check.h>

#include <list.h>
#include <lexer.h>
#include <parser.h>
#include <ast.h>

START_TEST (split_empty_empty)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 0);
   List_free(tokens);
}
END_TEST

START_TEST (split_oneNumber_oneNumberToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("13", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_NUMBER, "13" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_onePow_onePowToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("pow", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_ID, "pow" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_twoChar_oneToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("&&", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Lexer_clearTokens(tokens);   
   List_free(tokens);
}
END_TEST

START_TEST (split_string_stringToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("dasdasd", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_ID, "dasdasd" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_twoTokensSeparate_twoTokenTrue)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("dasdasd", tokens);
   status = Lexer_splitTokens("12.2", tokens);   
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 2);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token * secondToken = (Token *)List_at(tokens, 1);   
   Token testToken1 = { TokenType_ID, "dasdasd" };
   Token testToken2 = { TokenType_NUMBER, "12.2" };   
   ck_assert(Token_equals(firstToken, &testToken1));
   ck_assert(Token_equals(secondToken, &testToken2)); 
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_threeTokensTogether_threeTokenTrue)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("dasdasd 12.2", tokens);  
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 2);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token * secondToken = (Token *)List_at(tokens, 1);      
   Token testToken1 = { TokenType_ID, "dasdasd" };  
   Token testToken2 = { TokenType_NUMBER, "12.2" };
   ck_assert(Token_equals(firstToken, &testToken1));
   ck_assert(Token_equals(secondToken, &testToken2));   
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_print_threeTokenTrue)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("print()", tokens);  
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 3);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token * secondToken = (Token *)List_at(tokens, 1);
   Token * thirdToken = (Token *)List_at(tokens, 2);      
   Token testToken1 = { TokenType_ID, "print" };
   Token testToken2 = { TokenType_OPBRACKET, "(" };  
   Token testToken3 = { TokenType_CLBRACKET, ")" };
   ck_assert(Token_equals(firstToken, &testToken1));
   ck_assert(Token_equals(secondToken, &testToken2));
   ck_assert(Token_equals(thirdToken, &testToken3));    
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_eq_oneToken)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("=", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_ASSIGN, "=" };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_equal_oneToken)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("==", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_EQUAL, "==" };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Lexer");
    TCase *tc_sample = tcase_create("SplitTest");
    //
    tcase_add_test(tc_sample, split_empty_empty);
    tcase_add_test(tc_sample, split_oneNumber_oneNumberToken);
    tcase_add_test(tc_sample, split_onePow_onePowToken);
    tcase_add_test(tc_sample, split_twoChar_oneToken);
    tcase_add_test(tc_sample, split_string_stringToken);  
    tcase_add_test(tc_sample, split_twoTokensSeparate_twoTokenTrue);
    tcase_add_test(tc_sample, split_threeTokensTogether_threeTokenTrue);    
    tcase_add_test(tc_sample, split_print_threeTokenTrue);     
    tcase_add_test(tc_sample, split_eq_oneToken); 
    tcase_add_test(tc_sample, split_equal_oneToken);            
    
    
    //
    suite_add_tcase(s, tc_sample);
    return s;
}
   
int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);
   
    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}
   