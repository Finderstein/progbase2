#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <check.h>

#include <lexer.h>
#include <parser.h>
#include <interpreter.h>
#include <tree.h>
#include <ast.h>
#include <string_ext.h>
#include <dict.h>
#include <list.h>
#include <std_functions.h>

START_TEST (split_interpreterExecute_decVar)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;", tokens);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * astTree = Parser_buildNewAstTree(tokens);
    ck_assert(astTree != NULL);

    AstNode * astNode = astTree->value;
    ck_assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };      
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   

                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
        }
    }

    Value * val = Dict_get(program.variables, "tr");

    ck_assert(val->type == ValueType_NUMBER);
    ck_assert(*((double *)val->value) == 1);    

    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_free(program.functions);  
    
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(astTree);
}
END_TEST

START_TEST (split_interpreterExecute_assign)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;\ntr = 3;", tokens);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * astTree = Parser_buildNewAstTree(tokens);
    ck_assert(astTree != NULL);

    AstNode * astNode = astTree->value;
    ck_assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };      
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   

                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
        }
    }

    Value * val = Dict_get(program.variables, "tr");

    ck_assert(val->type == ValueType_NUMBER);
    ck_assert(*((double *)val->value) == 3);    

    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_free(program.functions);  

    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(astTree);
}
END_TEST

START_TEST (split_interpreterExecute_numtostr)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 1;\nlet str = numtostr(tr);", tokens);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * astTree = Parser_buildNewAstTree(tokens);
    ck_assert(astTree != NULL);

    AstNode * astNode = astTree->value;
    ck_assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };      
    Dict_add(program.functions, "numtostr", StdFunction_new(std_numtostr));
    
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   

                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
        }
    }

    Value * val1 = Dict_get(program.variables, "str");

    ck_assert(val1->type == ValueType_STRING);
    ck_assert(strcmp((char *)val1->value, "1"));    

    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_clear(program.functions);
    Dict_free(program.functions);  
    
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(astTree);
}
END_TEST

START_TEST (split_interpreterExecute_str)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = \"asdas\";", tokens);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * astTree = Parser_buildNewAstTree(tokens);
    ck_assert(astTree != NULL);

    AstNode * astNode = astTree->value;
    ck_assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };      
    Dict_add(program.functions, "numtostr", StdFunction_new(std_numtostr));
    
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   

                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
        }
    }

    Value * val1 = Dict_get(program.variables, "tr");

    ck_assert(val1->type == ValueType_STRING);
    ck_assert(strcmp((char *)val1->value, "asdas\\"));    
    
    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_clear(program.functions);
    Dict_free(program.functions);  

    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(astTree);
}
END_TEST

START_TEST (split_interpreterExecute_strlen)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = \"asdas\";\nlet len = strlen(tr);", tokens);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * astTree = Parser_buildNewAstTree(tokens);
    ck_assert(astTree != NULL);

    AstNode * astNode = astTree->value;
    ck_assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };      
    Dict_add(program.functions, "strlen", StdFunction_new(std_strlen));    
    
    
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   

                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
        }
    }

    Value * val1 = Dict_get(program.variables, "len");

    ck_assert(val1->type == ValueType_NUMBER);
    ck_assert(*((double *)val1->value) == 5);      

    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_clear(program.functions);
    Dict_free(program.functions);  
    
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(astTree);
}
END_TEST

START_TEST (split_interpreterExecute_mult)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 4;\ntr = tr * 4;", tokens);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * astTree = Parser_buildNewAstTree(tokens);
    ck_assert(astTree != NULL);

    AstNode * astNode = astTree->value;
    ck_assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };       
    
    
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   

                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
        }
    }

    Value * val1 = Dict_get(program.variables, "tr");

    ck_assert(val1->type == ValueType_NUMBER);
    ck_assert(*((double *)val1->value) == 16);     
    
    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_free(program.functions);  
    
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(astTree);
}
END_TEST

START_TEST (split_interpreterExecute_pow)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let tr = 4;\ntr = pow(tr, 3);", tokens);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_LET, "let" };
    ck_assert(Token_equals(firstToken, &testToken));
    Tree * astTree = Parser_buildNewAstTree(tokens);
    ck_assert(astTree != NULL);

    AstNode * astNode = astTree->value;
    ck_assert(astNode->type == AstNodeType_PROGRAM);
    Program program = 
    {
        .variables = Dict_new(),
        .functions = Dict_new(),        
        .error = NULL
    };       
    
    Dict_add(program.functions, "pow", StdFunction_new(std_pow));      
    
    for(int i = 0; i < List_count(astTree->children); i++)
    {
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        //
        switch(child->type)
        {
            case AstNodeType_DECLAREVAR:
            {
            
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   
                //AstNode * sChild = sChildNode->value;
                //
                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_ASSIGN:
            {     
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);   

                char * varName = strdup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) break; 
                //
                if(!Dict_contains(program.variables, varName))
                {
                    program.error = strdup("Duplicate variable id");
                    Value_free(varValue);
                }
                Value * oldValue = Dict_set(program.variables, varName, varValue);
                Value_free(oldValue);
                break;
            }
        }
    }

    Value * val1 = Dict_get(program.variables, "tr");

    ck_assert(val1->type == ValueType_NUMBER);
    ck_assert(*((double *)val1->value) == 64);  
    
    List * keys = List_new();
    Dict_keys(program.variables, keys);
    int len = List_count(keys);
    for(int i = 0; i < len; i++)
    {
        char * key = List_at(keys, i);
        Value * val = Dict_get(program.variables, key); 
        Value_free(val);
        free(key);     
    }
    List_free(keys);

    Dict_clear1(program.variables);
    Dict_free(program.variables);
    Dict_clear(program.functions);
    Dict_free(program.functions);  
    
    Lexer_clearTokens(tokens);
    List_free(tokens);
    Tree_clear(astTree);
}
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Lexer");
    TCase *tc_sample = tcase_create("SplitTest");
    //
    tcase_add_test(tc_sample, split_interpreterExecute_decVar);  
    tcase_add_test(tc_sample, split_interpreterExecute_assign);
    tcase_add_test(tc_sample, split_interpreterExecute_numtostr);
    tcase_add_test(tc_sample, split_interpreterExecute_str);   
    tcase_add_test(tc_sample, split_interpreterExecute_strlen);        
    tcase_add_test(tc_sample, split_interpreterExecute_mult);      
    tcase_add_test(tc_sample, split_interpreterExecute_pow);                
          
    
    
    //
    suite_add_tcase(s, tc_sample);
    return s;
}
   
int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);
   
    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}
   