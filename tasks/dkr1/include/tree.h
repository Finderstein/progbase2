#pragma once

#include <list.h>

typedef struct __Tree Tree;
struct __Tree
{
    void * value;
    List * children;
};

Tree * Tree_new(void * value);
Tree * Tree_newDegree(void * value, size_t degree);
void Tree_free(Tree * self);

void Tree_clear(Tree * self);