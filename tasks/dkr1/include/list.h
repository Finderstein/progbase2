#pragma once
#include<stdlib.h>
#include<iterator.h>


typedef struct __List List;

struct __List
{
    void **tokens;
    int length;
    int capacity;
};

List * List_new(void);
List * List_newCapacity(size_t initialCapacity);
void List_free(List * self);

void List_insert(List * self, void * value, size_t index);
void List_add(List * self, void * value);
void * List_at(List * self, size_t index);
void * List_set(List * self, size_t index, void * value);
void * List_removeAt(List * self, size_t index);
size_t List_count(List * self);

//iterator

Iterator * List_getNewBeginIterator (List * self);
Iterator * List_getNewEndIterator   (List * self);
