#pragma once

#include <tree.h>
#include <dict.h>

int Interpreter_execute(Tree * astTree);

typedef struct __Value Value;

typedef enum {
    ValueType_NUMBER,
    ValueType_BOOL,
    ValueType_STRING,
    ValueType_ARR,    
    ValueType_UNDEFINED
} ValueType;
 

struct __Value {
    ValueType type;
    void * value;
};

typedef struct __Program Program;

struct __Program
{
    Dict * variables;
    Dict * functions;    
    char * error;
};

void Program_setError(Program * self, char * error);

typedef Value * (*Function)(Program * program, List * values);

void ValueType_print(ValueType self);
void Value_print(Value * self);
Value * Value_newNumber(double number);
Value * Value_newArray(List * self);
Value * Value_newString(char * str);
Value * Value_newBool(bool item);
Value * Value_newUndefined(void);

char * Value_string(Value * self);
double Value_number(Value * self);
bool Value_bool(Value * self);
List * Value_arr(Value * self);

void Value_free(Value * self);

typedef struct 
{
    Function ptr;
}StdFunction;

StdFunction * StdFunction_new(Function ptr);

void StdFunction_free(StdFunction * self);

Value * eval(Program * program, Tree * node);