#pragma once
#include <list.h>

typedef enum
{
    TokenType_PLUS,
    TokenType_MINUS,
    TokenType_MULT,
    TokenType_DIV,
    TokenType_POW,
    TokenType_LET,
    TokenType_NUMBER, 
    TokenType_STR,     
    TokenType_ID,  
    TokenType_BOOL,       
    TokenType_OPBRACKET,
    TokenType_CLBRACKET,    
    TokenType_OPBLOCK,
    TokenType_CLBLOCK,    
    TokenType_OPSQBRACKET,
    TokenType_CLSQBRACKET,        
    TokenType_COMMA,    
    TokenType_SEMICOLON,
    TokenType_WS,
    TokenType_NEWLINE,    
    TokenType_IF,
    TokenType_WHILE,
    TokenType_ELSE,
    TokenType_SQRT,
    TokenType_STRLEN,
    TokenType_COUNT,
    TokenType_PRINT,
    TokenType_SCAN,
    TokenType_STRTONUM,
    TokenType_NUMTOSTR,
    TokenType_AND,
    TokenType_OR,
    TokenType_NOTEQUAL,
    TokenType_EQUAL,
    TokenType_MORE,
    TokenType_LESS,
    TokenType_NOT,
    TokenType_ASSIGN,
    TokenType_EQMORE,
    TokenType_EQLESS,
    TokenType_MOD,
    TokenType_UNKNOWN
} TokenType;

typedef struct __Token Token;

struct __Token
{
    TokenType type;
    char * name;
};

Token * Token_new(TokenType type, char * name);
void Token_free(Token * self);
bool Token_equals(Token * self, Token * other);

void TokenType_print(TokenType type);

int Lexer_splitTokens(const char * input, List * tokens);
void Lexer_clearTokens(List * tokens);

void Lexer_printTokensToFile(List * tokens);
void Lexer_printInConsoleTokens(List * self);


int readBufferToFile(const char *fileName, Token *tok);
