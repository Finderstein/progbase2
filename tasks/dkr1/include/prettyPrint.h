#pragma once

#include <tree.h>
#include <stdio.h>

//Print to console

void AstTree_prettyPrint(Tree * astTree);

static void PrintPretty(Tree * node, const char * indent, int root, int last);

static char * str_append(const char * str, const char * append);




//Write to File

void AstTree_prettyWriteToFile(Tree * astTree, const char * newFile);

char * str_appendToFile(const char * str, const char * append);

void WritePrettyToFile(Tree * node, const char * indent, int root, int last, FILE *f);