
#pragma once 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <list.h>


typedef struct __Dict Dict;

typedef struct
{
     char * key;
     void * value;
} KeyValuePair1;

Dict * Dict_new(void);
void Dict_free(Dict * self);

void   Dict_add      (Dict * self, char * key, void * value);
bool   Dict_contains (Dict * self, char * key);
void * Dict_get      (Dict * self, char * key);
void * Dict_set      (Dict * self, char * key, void * value);
void * Dict_remove   (Dict * self, char * key);
void   Dict_clear    (Dict * self);
size_t Dict_count    (Dict * self);

void   Dict_keys     (Dict * self, List * keys);
void   Dict_values   (Dict * self, List * values);

void   Dict_clear1    (Dict * self);