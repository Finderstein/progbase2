#include <stdio.h>
#include <progbase.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <string_ext.h>
#include <assert.h>
#include <stdbool.h>

#include <list.h>
#include <lexer.h>
#include <fs.h>
#include <parser.h>
#include <ast.h>
#include <dict.h>
#include <interpreter.h>
#include <prettyPrint.h>


int main(void)
{

    char *fileName = "../main.crtch";  
    char *fileNameAst = "../main_ast.txt";    
    if(fileExists(fileName))
    {
        long lenOfFile = getFileSize(fileName) + 1; 
        char text[lenOfFile];
        int bytesRead = readFileToBuffer(fileName, &text[0], (int)lenOfFile);
        text[bytesRead] = '\0';
        //puts(text);                        WORKING!!!


        List * tokens = List_new();
        if (0 != Lexer_splitTokens(text, tokens)) {
            Lexer_clearTokens(tokens);
            List_free(tokens);
            printf("Error! Tokens not found!");
            return EXIT_FAILURE;   
        }



        char command1[] = "touch ../main_tokens.txt";
        if(!fileExists("../main_tokens.txt"))
        {
            system(command1);        
        }                         
        Lexer_printTokensToFile(tokens); 

        // puts("======================================================\n");
        // Lexer_printInConsoleTokens(tokens);                         WORKING!!!
        // puts("\n\n======================================================\n");              

        Lexer_clearTokens(tokens);
        List_free(tokens);      
    }
    return EXIT_SUCCESS;
}