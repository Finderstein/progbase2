#pragma once

#include <stdlib.h>
#include <stdbool.h>

/*
    A set of unique integer values
    Ignore duplicates in operations
*/
typedef struct __Set Set;

struct __Set
{
    int * nums;
    size_t len;
    size_t cap;
};

Set * Set_new(void);
Set * Set_newFromArray(int array[], size_t arrayLen);
void Set_free(Set * self);

void Set_insert(Set * self, int value);
void Set_insertAll(Set * self, int value[], size_t arrayLen);

void Set_remove(Set * self, int value);
void Set_removeAll(Set * self, int value[], size_t arrayLen);

bool Set_contains(Set * self, int value);
bool Set_containsAll(Set * self, int value[], size_t arrayLen);

size_t Set_size(Set * self);
/*
    use qsort() here, ascending order
*/
void Set_copyToArraySorted(Set * self, int array[]);

// set operations
Set * Set_newIntersect(Set * self, Set * other);

bool Set_equals(Set * self, Set * other);
/*
    position independent
*/
bool Set_equalsArray(Set * self, int array[], size_t arrayLen);