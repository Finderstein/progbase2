#include "module.h"

Set * Set_new(void)
{
    Set * self = malloc(sizeof(Set));
    self->nums = malloc(sizeof(int) * 32);
    self->len = 0;
    self->cap = 32;

    return self;
}

Set * Set_newFromArray(int array[], size_t arrayLen)
{
    Set * self = malloc(sizeof(Set));
    self->nums = malloc(sizeof(int) * arrayLen);
    self->len = 0;
    self->cap = arrayLen;
    for(int i = 0; i < arrayLen; i++)
    {
        self->nums[i] = array[i];
    }

    return self;
}
void Set_free(Set * self)
{
    free(self->nums);
    free(self);
}

void Set_insert(Set * self, int value)
{
    if(Set_contains(self, value)) return;

    if(self->len == self->cap)
    {
        self->cap *= 2;
        self->nums = realloc(self->nums, sizeof(int) * self->cap);
    }

    self->nums[self->len] = value;
    self->len++;
}

void Set_insertAll(Set * self, int value[], size_t arrayLen)
{
    for(int i = 0; i < arrayLen; i++)
    {
        Set_insert(self, value[i]);
    }
}

void Set_remove(Set * self, int value)
{
    for(int i = 0; i < self->len; i++)
    {
        if(self->nums[i] == value)
        {
            for(int j = i; i < self->len - 1; i++)
            {
                self->nums[i] = self->nums[i + 1];
            }
            self->len -= 1;
        }
    }
}

void Set_removeAll(Set * self, int value[], size_t arrayLen)
{
    for(int i = 0; i < arrayLen; i++)
    {
        Set_remove(self, value[i]);
    }
}

bool Set_contains(Set * self, int value)
{
    for(int i = 0; i < self->len; i++)
    {
        if(self->nums[i] == value)
        {
            return true;
        }
    }
    return false;
}

bool Set_containsAll(Set * self, int value[], size_t arrayLen)
{
    bool t = true;
    for(int i = 0; i < arrayLen && t; i++)
    {
        t = Set_contains(self, value[i]);
    }

    return t;
}

size_t Set_size(Set * self)
{
    return self->len;
}
/*
    use qsort() here, ascending order
    
*/

static int compare(const void * x1, const void * x2)   //http://cppstudio.com/post/891/
{
  return ( *(int*)x1 - *(int*)x2 );              
}

void Set_copyToArraySorted(Set * self, int array[])
{
    for(int i = 0; i < self->len; i++)
    {
        array[i] = self->nums[i];
    }

    qsort(array, self->len, sizeof(int), compare);
}

// set operations
Set * Set_newIntersect(Set * self, Set * other)
{
    int count = 0;
    for(int i = 0; i < self->len; i++)
    {
        if(self->nums[i] == other->nums[i])
        {
            count++;
        }
    }
    
    int arr[count];
    int index = 0;

    for(int i = 0; i < self->len; i++)
    {
        if(self->nums[i] == other->nums[i])
        {
            arr[index] = self->nums[i];
        }
    }

    return Set_newFromArray(arr, count);
}

bool Set_equals(Set * self, Set * other)
{
    if(self->len != other->len) return false;

    bool t = true;
    for(int i = 0; i < self->len && t; i++)
    {
        if(self->nums[i] != other->nums[i])
        {
            t = false;
        }
    }

    return t;
}
/*
    position independent
*/
bool Set_equalsArray(Set * self, int array[], size_t arrayLen)
{
    if(arrayLen > self->len) return false;

    bool t = true;

    for(int i = 0; i < arrayLen && t; i++)
    {
        if(!Set_contains(self, array[i]))
        {
            t = false;
        }
    }

    return t;  
}