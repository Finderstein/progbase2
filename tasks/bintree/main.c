#include <stdio.h>
#include <stdlib.h>
#include <bintree.h>
#include <bstree.h>

int main(void)
{
    BSTree *bst = BSTree_new();
    
    BSTree_insert(bst, -12);
    BSTree_insert(bst, -1);
    BSTree_insert(bst, 4);
    BSTree_insert(bst, -15); // there was -1 but I change it because it was duplicate
    BSTree_insert(bst, -11);
    BSTree_insert(bst, -8);
    BSTree_insert(bst, 0);    

    BSTree_printFormat(bst);
    BSTree_printTraverse(bst);    

    BSTree_clear(bst);
    BSTree_free(bst);

    return 0;
}