#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <bstree.h>
#include <bintree.h>
#include <assert.h>

#define NDEBUG

struct __BSTree
{
    BinTree *root;
};

BSTree * BSTree_new(void)
{
    BSTree *self = malloc(sizeof(BSTree));
    self->root = NULL;
    return self;
}
void BSTree_free(BSTree * self)
{
    free(self);
}

static void insert(BinTree *node, int value)
{
    if (value < node->key)
    {
        if(node->left != NULL) 
        {
            insert(node->left, value);
        }
        else 
        {
            node->left = BinTree_new(value);
        }
    }
    else if(value > node->key)
    {
        if(node->right != NULL) 
        {
            insert(node->right, value);
        }
        else 
        {
            node->right = BinTree_new(value);
        }
    }
    else
    {
        assert(0 && "Duplicate key");
    }
}

void BSTree_insert(BSTree * self, int key)
{
    if (self->root == NULL)
    {
        BinTree *node = BinTree_new(key);
        self->root = node;
    }
    else
    {
        insert(self->root, key);
    }
}

static void clear(BinTree* node)
{
    if (node->left != NULL)
    {
        clear(node->left);
    }

    if (node->right != NULL)
    {
        clear(node->right);
    }
    
    BinTree_free(node);
}

void BSTree_clear(BSTree * self)
{
    clear(self->root);
}

static void printFormat(BinTree *node, int level)
{
    for(int i = 0; i < level; i++)
    {
        printf("..");
    }
    if(node == NULL)
    {
        printf("{null}");
        puts("");
        return;
    }

    printf("%i ", node->key);
    puts("");
    
    if(node->left || node->right)
    {
        printFormat(node->left, level + 1);
        printFormat(node->right, level + 1);
    }
        
}

void BSTree_printFormat(BSTree * self)
{
    printFormat(self->root, 0);
    puts("");
}

static void printTraverse(BinTree *node)
{
    if(node->left)
    {
        printTraverse(node->left);
    }
    printf("%i ", node->key);
    if(node->right)
    {
        printTraverse(node->right);
    }
}

void BSTree_printTraverse(BSTree * self)
{
    printTraverse(self->root);
    puts("");
} 

BinTree * BSTree_root(BSTree * self)
{
    return self->root;
}