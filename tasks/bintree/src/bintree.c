#include <bintree.h>
#include <stdlib.h>

BinTree *BinTree_new(int value)
{
    BinTree *self = malloc(sizeof(BinTree));
    self->key = value;
    self->left = NULL;
    self->right = NULL;
    return self;
}

void BinTree_free(BinTree * self)
{
    free(self);
}