#pragma once
#include <progbase/events.h>

typedef enum {
    // emitter events 
    EmitterCommandEventId,          // Command
    EmitterResultRequestEventId,    // ResultRequest
    // handler events
    HandlerErrorResponseEventId,    // --no data--
    HandlerResultResponseEventId    // Result
} AppEvents;

typedef struct Command Command;
typedef struct ResultRequest ResultRequest;
typedef struct Result Result;

#define EventSystemEventTypeData(EVENTID, DATATYPE)     static inline DATATYPE EVENTID##_data(Event * event) { return *(DATATYPE*)event->data; }

struct Command {
    int varId;  // 0 - 9
    enum {
        OpSum,
        OpMult,
        OpDiv
    } operation;
    int value;
};

struct ResultRequest {
    int varId;  // 0 - 9
};

struct Result {
    int value;
};

// creates 3 helper functions to retrieve dereferenced typed event data
EventSystemEventTypeData(EmitterCommandEventId, Command)  // EmitterCommandEventId_data()
EventSystemEventTypeData(EmitterResultRequestEventId, ResultRequest)  // EmitterResultRequestEventId_data()
EventSystemEventTypeData(HandlerResultResponseEventId, Result)  // HandlerResultResponseEventId_data()

