#include "handler.h"
#include "eventtypes.h"
#include <stdlib.h>
#include <assert.h>

struct Handler {
    int arr[10];  // @todo you can modify this
};

Handler * Handler_new(void) {
    Handler * self = malloc(sizeof(Handler));
    for(int i = 0; i < 10; i++)
    {
        self->arr[i] = 0;
    }

    return self;
}
void Handler_free(Handler * self) {
    //  @todo cleanup
    free(self);
}

void Handler_onEvent(EventHandler * self, Event * event) {
    Handler * handler = self->data;
    switch (event->type) {
        case EmitterCommandEventId:
        {
            Command * data = (Command *)event->data;
            if(data->varId > 9 || data->varId < 0)
            {
                EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));                
            }
            else
            {
                switch (data->operation)
                {
                    case OpSum:
                    {
                        handler->arr[data->varId] = handler->arr[data->varId] + data->value;
                        break;
                    }
                    case OpMult:
                    {
                        handler->arr[data->varId] = handler->arr[data->varId] * data->value;
                        break;                        
                    }
                    case OpDiv:                    
                    {
                        if(data->value == 0)
                        {
                            EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
                            break;                        
                        }

                        handler->arr[data->varId] = handler->arr[data->varId] / data->value;
                        break;                        
                    }
                }              
            }
            break;            
        }
        case EmitterResultRequestEventId:
        {
            ResultRequest * data = (ResultRequest *)event->data;
            if(data->varId > 9 || data->varId < 0)
            {
                EventSystem_emit(Event_new(self, HandlerErrorResponseEventId, NULL, NULL));
            }
            else
            {
                int * numb = malloc(sizeof(int));
                *numb = handler->arr[data->varId];
                EventSystem_emit(Event_new(self, HandlerResultResponseEventId, numb, free));
            }
            break;            
        }
    }
}