#include <stdio.h>
#include <progbase.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include <numlist.h>
#include <numlist_ext.h>

void removeNeg(NumList * self);

int main(void)
{
    srand(time(0));
    NumList * list = NumList_new();

    for(int i = 0; i < 10; i++)
    {
        int value = rand() % 201 - 100;
        NumList_add(list, value);
    }

    NumList_print(list);
    
    removeNeg(list);
    
    NumList_print(list);

    NumList_free(list);

    return 0;
}

void removeNeg(NumList * self)
{
    assert(self != NULL && "NULL POINTER(removeNeg)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(removeNeg)");
        return;
    }

    for(int i = 0; i < NumList_count(self); i++)
    {
        if(NumList_at(self, i) < 0)
        {
            NumList_removeAt(self, i);
            i--;
        }
    }
}