#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <numlist_ext.h>

void NumList_print(NumList * self)
{
    assert(self != NULL && "NULL POINTER(NumList_print)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_print)");
        return;
    }

    for(int i = 0; i < NumList_count(self); i++)
    {
        printf("%i ", NumList_at(self, i));
    }

    puts("");
    
}

double NumList_average(NumList * self)
{
    assert(self != NULL && "NULL POINTER(NumList_average)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_average)");
        return 0;
    }

    double aver = 0;
    for(int i = 0; i < NumList_count(self); i++)
    {
        aver += NumList_at(self, i);
    }

    return aver/NumList_count(self);
}

int NumList_minIndex(NumList * self)
{
    assert(self != NULL && "NULL POINTER(NumList_minIndex)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_minIndex)");
        return 0;
    }

    int minIndex = 0;
    for(int i = 0; i < NumList_count(self); i++)
    {
        if(NumList_at(self, i) < NumList_at(self, minIndex))
        {
            minIndex = i;
        }
    }

    return minIndex;
}

int NumList_maxIndex(NumList * self)
{
    assert(self != NULL && "NULL POINTER(NumList_maxIndex)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_maxIndex)");
        return 0;
    }

    int maxIndex = 0;

    for(int i = 0; i < NumList_count(self); i++)
    {
        if(NumList_at(self, i) > NumList_at(self, maxIndex))
        {
            maxIndex = i;
        }
    }

    return maxIndex;
}