#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <numlist.h>

struct __NumList
{
    int *items;
    int capacity;
    int length;
};

NumList * NumList_new(void)
{
    NumList *self = malloc(sizeof(NumList));
    if(self == NULL)
    {
        fprintf(stderr, "Out of memory[self] (NumList_new)");
        abort();
    }

    self->length = 0;
    self->capacity = 10;

    self->items = malloc(sizeof(int) * self->capacity);
    if(self->items == NULL)
    {
        fprintf(stderr, "Out of memory[self->items] (NumList_new)");
        abort();
    }

    return self;
}

void NumList_free(NumList * self)
{
    assert(self != NULL && "NULL POINTER(NumList_free)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_free)");
        return;
    }
    
    free(self->items);
    free(self);
}

void NumList_add(NumList * self, int value)
{
    assert(self != NULL && "NULL POINTER(NumList_add)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_add)");
        return;
    }

    assert(self->length < self->capacity && "Max length of list(NumList_add)");
    if(self->length >= self->capacity)
    {
        fprintf(stderr, "Max length of list(NumList_add)");
        return;
    }

    self->items[self->length] = value;
    self->length += 1;
}

void NumList_insert(NumList * self, size_t index, int value)
{
    assert(self != NULL && "NULL POINTER(NumList_insert)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_insert)");
        return;
    }

    assert(self->length < self->capacity && "Max length of list(NumList_insert)");
    if(self->length >= self->capacity)
    {
        fprintf(stderr, "Max length of list(NumList_insert)");
        return;
    }

    assert(index >= 0 && index < 10 && index < self->length && "Invalid index(NumList_insert)");
    if(index < 0 || index > 9)
    {
        fprintf(stderr, "Invalid index(NumList_insert)");
        return;
    }

    for(int i = self->length; i > index; i--)
    {
        self->items[i] = self->items[i - 1];
    }
    self->items[index] = value;

    return;
}

int NumList_at(NumList * self, size_t index)
{
    assert(self != NULL && "NULL POINTER(NumList_at)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_at)");
        return 0;
    }

    assert(index >= 0 && index < 10 && index < self->length && "Invalid index(NumList_at)");
    if(index < 0 || index > 9 || index >= self->length)
    {
        fprintf(stderr, "Invalid index(NumList_at)");
        return 0;
    }

    return self->items[index];
}

int NumList_set(NumList * self, size_t index, int value)
{
    assert(self != NULL && "NULL POINTER(NumList_set)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_set)");
        return 0;
    }

    assert(index >= 0 && index < 10 && index < self->length && "Invalid index(NumList_set)");
    if(index < 0 || index > 9 || index >= self->length)
    {
        fprintf(stderr, "Invalid index(NumList_set)");
        return 0;
    }

    int old = self->items[index];
    self->items[index] = value;
    return old;
}
 
int NumList_removeAt(NumList * self, size_t index)
{
    assert(self != NULL && "NULL POINTER(NumList_removeAt)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_removeAt)");
        return 0;
    }

    assert(index >= 0 && index < 10 && index < self->length && "Invalid index(NumList_removeAt)");
    if(index < 0 || index > 9)
    {
        fprintf(stderr, "Invalid index(NumList_removeAt)");
        return 0;
    }

    int removed = self->items[index];
    for(int i = index; i < self->length - 1; i++)
    {
        self->items[i] = self->items[i + 1]; 
    }
    self->length -= 1;

    return removed;
}
 
size_t NumList_count(NumList * self)
{
    assert(self != NULL && "NULL POINTER(NumList_count)");
    if(self == NULL)
    {
        fprintf(stderr, "NULL POINTER(NumList_count)");
        return 0;
    }

    return self->length;
}