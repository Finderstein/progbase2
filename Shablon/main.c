#include <stdio.h>
#include <progbase.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "include/numlist.h"
#include "include/numlist_ext.h"


int main(void)
{
    NumList * list = NumList_new();

    for(int i = 0; i < 10; i++)
    {
        int value = rand() % 201 - 100;
        NumList_add(list, value);
    }

    NumList_print(list);

    NumList_free(list);

    return 0;
}

