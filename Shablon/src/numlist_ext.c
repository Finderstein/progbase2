#include <stdio.h>
#include <stdlib.h>

#include "../include/numlist_ext.h"

void NumList_print(NumList * self)
{
    //@TODO
    //check
    puts("List of numbers");
    for(int i = 0; i < NumList_count(self); i++)
    {
        printf("%i ", NumList_at(self, i));
    }

    puts("");
    
}

double NumList_average(NumList * self)
{
    //@TODO
    //check

    double aver = 0;
    for(int i = 0; i < NumList_count(self); i++)
    {
        aver += NumList_at(self, i);
    }

    return aver;
}

int NumList_minIndex(NumList * self)
{
    //@TODO
    //check

    int minIndex = 0;
    for(int i = 0; i < NumList_count(self); i++)
    {
        if(NumList_at(self, i) < NumList_at(self, minIndex))
        {
            minIndex = i;
        }
    }

    return minIndex;
}

int NumList_maxIndex(NumList * self)
{
    //@TODO
    //check

    int maxIndex = 0;

    for(int i = 0; i < NumList_count(self); i++)
    {
        if(NumList_at(self, i) > NumList_at(self, maxIndex))
        {
            maxIndex = i;
        }
    }

    return maxIndex;
}