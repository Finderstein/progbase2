#include <stdlib.h>
#include <assert.h>
#include "../include/numlist.h"

struct __NumList
{
    int *items;
    int capacity;
    int length;
};

NumList * NumList_new(void)
{
    NumList *self = malloc(sizeof(NumList));

    self->length = 0;
    self->capacity = 10;

    self->items = malloc(sizeof(int) * self->capacity);

    return self;
}

void NumList_free(NumList * self)
{
    //@TODO
    //check
    
    free(self->items);
    free(self);
}

void NumList_add(NumList * self, int value)
{
    //@TODO
    //Check capacity and length

    self->items[self->length] = value;
    self->length += 1;
}

void NumList_insert(NumList * self, size_t index, int value)
{
    //@TODO
    //check

    for(int i = index; i < self->length; i++)
    {
        self->items[i + 1] = self->items[i];
    }
    self->items[index] = value;

    return;
}

int NumList_at(NumList * self, size_t index)
{
    //@TODO
    //check

    assert(index < self->length);

    return self->items[index];
}

int NumList_set(NumList * self, size_t index, int value)
{
    //@TODO
    //check

    int old = self->items[index];
    self->items[index] = value;
    return old;
}
 
int NumList_removeAt(NumList * self, size_t index)
{
    //@TODO
    //check

    int removed = self->items[index];
    for(int i = index; i < self->length - 1; i++)
    {
        self->items[i] = self->items[i + 1]; 
    }
    self->length -= 1;

    return removed;
}
 
size_t NumList_count(NumList * self)
{
    //@TODO
    //check

    return self->length;
}