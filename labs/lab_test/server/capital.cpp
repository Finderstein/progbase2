#include "capital.h"

Capital::Capital(QObject *parent) : QObject(parent)
{
    name = "Name";
    square = 0;
    population = 0;
    longtitude = 0;
    latitude = 0;
}

Capital::Capital(QString name, int square, float population, float longtitude, float latitude, QObject *parent) : QObject(parent)
{
    this->name = name;
    this->square = square;
    this->population = population;
    this->longtitude = longtitude;
    this->latitude = latitude;
}

/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

int Capital::getId()
{
    return this->id;
}
QString Capital::getName()
{
    return this->name;
}
int Capital::getSquare()
{
    return this->square;
}
float Capital::getPopulation()
{
    return this->population;
}
float Capital::getLongtitude()
{
    return this->longtitude;
}
float Capital::getLatitude()
{
    return this->latitude;
}

/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

void Capital::setId(int id)
{
    this->id = id;
}
void Capital::setName(QString name)
{
    this->name = name;
}
void Capital::setSquare(int square)
{
    this->square = square;
}
void Capital::setPopulation(float pop)
{
    this->population = pop;
}
void Capital::setLongtitude(float longt)
{
    this->longtitude = longt;
}
void Capital::setLatitude(float lat)
{
    this->latitude = lat;
}

