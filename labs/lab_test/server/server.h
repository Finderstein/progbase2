#ifndef SERVER_H
#define SERVER_H
#include <QObject>
#include <QTcpServer>
#include <storage.h>

class Server : public QObject
{
    Q_OBJECT
    QString _req;
    QString _resp;
    Storage * _storage;
    QTcpServer * _tcpserver;
public:
    explicit Server(QObject *parent = nullptr);

signals:

public slots:
    void onNewConnection();
    void onReadyRead();
    void onClientDisconnected();
};

#endif // SERVER_H
