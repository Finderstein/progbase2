QT += core network
QT -= gui

CONFIG += c++11

TARGET = server
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    capital.cpp \
    server.cpp \
    storage.cpp \
    serialization.cpp

HEADERS += \
    capital.h \
    server.h \
    storage.h \
    serialization.h

