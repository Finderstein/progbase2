#include "server.h"
#include "serialization.h"
#include <iostream>
#include <QTcpSocket>



using namespace std;

Server::Server(QObject *parent) : QObject(parent)
{
    _tcpserver = new QTcpServer(parent);
    _req = "";
    _storage = new Storage(parent);

    connect(_tcpserver, SIGNAL(newConnection()),
            this, SLOT(onNewConnection()));

    const int PORT = 6731;
    if(!_tcpserver->listen(QHostAddress::Any, PORT)) {
        cout << "Server couldn't start at PORT" << PORT << endl;
    } else {
        cout << "Server started at PORT " << PORT << endl;
    }
}

void Server::onNewConnection()
{
    cout << "New client is connected!" << endl;
    QTcpSocket * socket = _tcpserver->nextPendingConnection();
    connect(socket, SIGNAL(readyRead()), SLOT(onReadyRead()));
    connect(socket, SIGNAL(disconnected()), SLOT(onClientDisconnected()));

    Serialization json;
    if (socket->waitForReadyRead(2000)) {
        QByteArray data = _resp.toUtf8();
        cout << "Sending: " << _resp.toStdString() << endl;
        socket->write(data);
        socket->flush();
    }
}

void Server::onReadyRead()
{
    QTcpSocket * socket = static_cast<QTcpSocket *>(sender());
    QByteArray bytes = socket->readAll();
    cout << "Received: " << bytes.toStdString() << endl;

    _req = QString::fromStdString(bytes.toStdString());
    Serialization json;
    int req = json.getReqStatus(&_req);
    switch (req)
    {
        case CREATE_REQ:
        {
            _storage->clearStorage();
            _resp = json.createResp();
            break;
        }
        case ADD_REQ:
        {
            _storage->addToStorage(json.jsonToCapital(&_req));
            _resp = json.addResp(_storage->lastAtStorage());
            break;
        }
        case EDIT_REQ:
        {
            _storage->editAtStorage(json.jsonToCapital(&_req));
            _resp = json.editResp(json.jsonToCapital(&_req));
            break;
        }
        case REMOVE_REQ:
        {
            _storage->removeFromStorage(json.jsonToCapital(&_req));
            _resp = json.remResp(json.jsonToCapital(&_req));
            break;
        }
        case ALLFILES_REQ:
        {
            _resp = json.fileResp();
            break;
        }
        case SERVERLIST_REQ:
        {
            _resp = json.listResp(_storage->getList());
            break;
        }
        case SAVE_REQ:
        {
            QString fileName = json.getFileName(&_req);
            _resp = json.saveResp(fileName, _storage->getList());
            break;
        }
        case LOAD_REQ:
        {
            QString fileName = json.getFileName(&_req);
            _storage->setList(json.load(fileName));
            _resp = json.listResp(_storage->getList());
            break;
        }
        default: break;
    }
}

void Server::onClientDisconnected()
{
    cout << "Client disconnected!" << endl;
}



