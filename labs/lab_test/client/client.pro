#-------------------------------------------------
#
# Project created by QtCreator 2018-05-28T18:12:57
#
#-------------------------------------------------

QT       += core gui
QT += network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    client.cpp \
    serialization.cpp \
    capital.cpp \
    adddialog.cpp \
    editdialog.cpp

HEADERS  += mainwindow.h \
    client.h \
    serialization.h \
    capital.h \
    adddialog.h \
    editdialog.h

FORMS    += mainwindow.ui \
    client.ui \
    adddialog.ui \
    editdialog.ui
