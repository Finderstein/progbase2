#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <client.h>
#include <QTcpSocket>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_con_btn_clicked()
{
    Client * client = new Client();

    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), client, SLOT(Connection()));
    QHostAddress adr(ui->Ip_line->text());
    socket->connectToHost(adr, ui->port_line->text().toInt());

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
    else
    {
        hide();
        client->show();
    }
}
