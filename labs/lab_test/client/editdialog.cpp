#include "editdialog.h"
#include "ui_editdialog.h"

EditDialog::EditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);
}

EditDialog::~EditDialog()
{
    delete ui;
}

void EditDialog::getCapital(Capital *cap)
{
    ui->edit_name_lineEdit->setText(cap->getName());
    ui->edit_square_spinBox->setValue(cap->getSquare());
    ui->edit_pop_doubleSpinBox->setValue(cap->getPopulation());
    ui->edit_longt_doubleSpinBox->setValue(cap->getLongtitude());
    ui->edit_lat_doubleSpinBox->setValue(cap->getLatitude());
    ui->id_box->setValue(cap->getId());
}

void EditDialog::on_buttonBox_accepted()
{
    Capital *cap = new Capital(ui->edit_name_lineEdit->text(),
                                   ui->edit_square_spinBox->value(),
                                   ui->edit_pop_doubleSpinBox->value(),
                                   ui->edit_longt_doubleSpinBox->value(),
                                   ui->edit_lat_doubleSpinBox->value());
    cap->setId(ui->id_box->text().toInt());
    emit sendCapital(cap);
}
