#include "client.h"
#include "ui_client.h"
#include <QTcpSocket>
#include <QMessageBox>
#include <serialization.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariant>
#include <QListWidgetItem>
#include <QJsonArray>
#include <adddialog.h>
#include <editdialog.h>

Client::Client(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Client)
{
    ui->setupUi(this);
}

Client::~Client()
{
    delete ui;
}

static Capital * takeCapital(QListWidgetItem * item)
{
    QVariant var = item->data(Qt::UserRole);
    Capital * cap = var.value<Capital *>();

    return cap;
}


void Client::Connection()
{
    QTcpSocket * socket = static_cast<QTcpSocket *> (sender());
    _ip = socket->peerAddress();
    _hostPort = socket->peerPort();
}

void Client::onConnected()
{
    QTcpSocket * socket = static_cast<QTcpSocket *> (sender());
    connect(socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));

    QByteArray data = _req.toUtf8();
    socket->write(data);
    socket->flush();
}

void Client::onReadyRead()
{
    QTcpSocket * socket = static_cast<QTcpSocket *> (sender());
    QByteArray bytes;
    bytes += socket->readAll();
    QString respStr = QString::fromStdString(bytes.toStdString());

    Serialization json;
    int resp = json.getRespStatus(&respStr);

    switch (resp)
    {
        case ADD_RESP:
        {
            Capital * cap = json.jsonToCapital(&respStr);
            QListWidgetItem * item = new QListWidgetItem();
            QVariant var;
            var.setValue(cap);
            item->setData(Qt::UserRole, var);
            item->setText(cap->getName() + "(id:" + QString::number(cap->getId()) + ")");
            ui->cap_list->addItem(item);
            break;
        }
        case REMOVE_RESP:
        {
            Capital * cap = json.jsonToCapital(&respStr);
            QList<QListWidgetItem *> items = ui->cap_list->selectedItems();

            for(QListWidgetItem * item : items)
            {
                int row = ui->cap_list->row(item);
                QListWidgetItem * i = ui->cap_list->item(row);
                Capital * remCap = takeCapital(i);

                if(remCap->getId() == cap->getId())
                {
                    i = ui->cap_list->takeItem(row);
                    delete i;
                }
            }
            break;
        }
        case EDIT_RESP:
        {
            Capital * cap = json.jsonToCapital(&respStr);
            QList<QListWidgetItem *> items = ui->cap_list->selectedItems();

            for(QListWidgetItem * item : items)
            {
                int row = ui->cap_list->row(item);
                QListWidgetItem * i = ui->cap_list->item(row);
                Capital * editCap = takeCapital(i);

                if(editCap->getId() == cap->getId())
                {
                    editCap->setName(cap->getName());
                    editCap->setSquare(cap->getSquare());
                    editCap->setPopulation(cap->getPopulation());
                    editCap->setLongtitude(cap->getLongtitude());
                    editCap->setLatitude(cap->getLatitude());
                    item->setText(editCap->getName() + "(id:" + QString::number(editCap->getId()) + ")");
                    emit(on_cap_list_itemSelectionChanged());
                }
            }
            break;
        }
        case ALLFILES_RESP:
        {
        QStringList list = json.getFileResp(&respStr);

        for (int i = 0; i < list.size(); i++)
        {
           ui->files_list->addItem(list.at(i));
        }
            break;
        }
        case CREATE_RESP:
        {
            if(ui->cap_list->count() > 0)
            {
                for (int i = 0; i < ui->cap_list->count(); ++i)
                {
                   QListWidgetItem * it = ui->cap_list->takeItem(i);
                   delete(it);
                   i--;
                }
            }
            break;
        }
        case SERVERLIST_RESP:
        {
            if(ui->cap_list->count() > 0)
            {
                for (int i = 0; i < ui->cap_list->count(); ++i)
                {
                   QListWidgetItem * it = ui->cap_list->takeItem(i);
                   delete(it);
                   i--;
                }
            }

            QList<Capital *> list = json.jsonToCapitals(&respStr);
            for (int i = 0; i < list.count(); ++i)
            {
                Capital *cap = list.at(i);
                QListWidgetItem * item = new QListWidgetItem();
                QVariant var;
                var.setValue(cap);
                item->setData(Qt::UserRole, var);
                item->setText(cap->getName() + "(id:" + QString::number(cap->getId()) + ")");
                ui->cap_list->addItem(item);
            }

            break;
        }
        case SAVE_RESP:
        {

            break;
        }
        default:
        {
            ui->err_label->setText("Error Status!!!");
            break;
        }
    }
    socket->disconnectFromHost();
}

void Client::onDisconnected()
{

}


void Client::getCapital(Capital * cap)
{
    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    _req = json.addReq(cap);
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
}



void Client::getEdited(Capital * cap)
{
    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    _req = json.editReq(cap);
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
}


void Client::on_files_btn_clicked()
{
    ui->files_list->clear();
    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    _req = json.fileReq();
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
}

void Client::on_create_btn_clicked()
{
    ui->add_btn->setEnabled(true);
    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    _req = json.createReq();
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
}

void Client::on_edit_btn_clicked()
{
    EditDialog * dialog = new EditDialog();

    connect(dialog, SIGNAL(sendCapital(Capital *)), this, SLOT(getEdited(Capital *)));

    connect(this, SIGNAL(sendEditedCapital(Capital *)), dialog, SLOT(getCapital(Capital *)));
    QListWidgetItem * item = ui->cap_list->selectedItems().at(0);

    emit sendEditedCapital(takeCapital(item));

    dialog->show();
}

void Client::on_del_btn_clicked()
{
    QListWidgetItem * item = ui->cap_list->selectedItems().at(0);
    Capital * cap = takeCapital(item);

    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    _req = json.remReq(cap);
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
}

void Client::on_cap_list_itemSelectionChanged()
{
    QList<QListWidgetItem *> items = ui->cap_list->selectedItems();

    if(items.count() == 0)
    {
        ui->name->setText("");
        ui->square->setText("");
        ui->population->setText("");
        ui->latitude->setText("");
        ui->longtitude->setText("");
        ui->edit_btn->setEnabled(false);
        ui->del_btn->setEnabled(false);
    }
    else
    {
        QListWidgetItem * item = items.at(0);
        QVariant var = item->data(Qt::UserRole);
        Capital * cap = var.value <Capital *>();
        ui->name->setText(cap->getName());
        ui->square->setText(QString::number(cap->getSquare()));
        ui->population->setText(QString::number(cap->getPopulation()));
        ui->longtitude->setText(QString::number(cap->getLongtitude()));
        ui->latitude->setText(QString::number(cap->getLatitude()));
        ui->edit_btn->setEnabled(true);
        ui->del_btn->setEnabled(true);
    }
}

void Client::on_add_btn_clicked()
{
    AddDialog * dialog = new AddDialog();

    connect(dialog, SIGNAL(sendCapital(Capital *)), this, SLOT(getCapital(Capital*)));

    dialog->exec();
}

void Client::on_serverList_btn_clicked()
{

    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    _req = json.listReq();
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
    else
    {
        ui->add_btn->setEnabled(true);
    }
}

void Client::on_load_btn_clicked()
{
    ui->cap_list->clear();
    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    QListWidgetItem *item = ui->files_list->selectedItems().at(0);
    QString fileName = item->text();
    _req = json.loadReq(fileName);
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
    else
    {
        ui->add_btn->setEnabled(true);
    }
}

void Client::on_save_btn_clicked()
{
    QTcpSocket * socket = new QTcpSocket();
    connect(socket, SIGNAL(connected()), this, SLOT(onConnected()));

    Serialization json;
    QListWidgetItem *item = ui->files_list->selectedItems().at(0);
    QString fileName = item->text();
    _req = json.saveReq(fileName);
    socket->connectToHost(_ip,_hostPort);

    if(!socket->waitForConnected(2000))
    {
        ui->err_label->setText("Can't connect to the server");
    }
}
