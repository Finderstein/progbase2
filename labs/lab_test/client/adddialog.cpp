#include "adddialog.h"
#include "ui_adddialog.h"

AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_buttonBox_accepted()
{
    Capital * cap = new Capital(ui->add_name_lineEdit->text(),
                                ui->add_square_spinBox->value(),
                                ui->add_pop_doubleSpinBox->value(),
                                ui->add_longt_doubleSpinBox->value(),
                                ui->add_lat_doubleSpinBox->value());
    emit sendCapital(cap);
}
