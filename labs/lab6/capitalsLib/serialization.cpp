#include "serialization.h"
#include <iostream>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDir>
#include <cstdio>
#include <cstdlib>

Serialization::Serialization()
{

}

static int fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    fclose(f);
    return 1;  // true: exists
}

static long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

static int readFileToBuffer(const char *fileName, char *buffer, int bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  // number of bytes read
}

static int readBufferToFile(char *fileName, const char *buffer)
{
    FILE *f = fopen(fileName, "w");
    if (!f) return 0;  // read 0 bytes from file
    long writeBytes = fwrite(buffer, strlen(buffer), 1, f);
    fclose(f);
    return writeBytes;  // number of bytes read
}

int get_file_size(std::string filename) // path to file
{
    FILE *p_file = NULL;
    p_file = fopen(filename.c_str(),"rb");
    fseek(p_file,0,SEEK_END);
    int size = ftell(p_file);
    fclose(p_file);
    return size;
}

void Serialization::status(QJsonDocument * doc, int status)
{
    QJsonObject docObj;
    if(!doc->isEmpty()) {
        docObj = doc->object();
    }
    docObj.insert("Status", status);
    doc->setObject(docObj);
}

void Serialization::file(QJsonDocument * doc, QString fileName)
{
    QJsonObject docObj;
    if(!doc->isEmpty()) {
        docObj = doc->object();
    }
    docObj.insert("File", fileName);
    doc->setObject(docObj);
}

QString Serialization::createReq()
{
    QJsonDocument doc;
    status(&doc, CREATE_REQ);

    return QString::fromStdString(doc.toJson().toStdString());
}

QString Serialization::addReq(Capital * cap)
{
    QJsonDocument doc;
    status(&doc, ADD_REQ);

    return capitalToJson(cap, &doc);
}

QString Serialization::editReq(Capital * cap)
{
    QJsonDocument doc;
    status(&doc, EDIT_REQ);

    return capitalToJson(cap, &doc);
}

QString Serialization::remReq(Capital * cap)
{
    QJsonDocument doc;
    status(&doc, REMOVE_REQ);

    return capitalToJson(cap, &doc);
}

QString Serialization::fileReq()
{
    QJsonDocument doc;
    status(&doc, ALLFILES_REQ);

    return QString::fromStdString(doc.toJson().toStdString());
}

QString Serialization::listReq()
{
    QJsonDocument doc;
    status(&doc, SERVERLIST_REQ);

    return QString::fromStdString(doc.toJson().toStdString());
}

QString Serialization::saveReq(QString fileName)
{
    QJsonDocument doc;
    status(&doc, SAVE_REQ);
    file(&doc, fileName);

    return QString::fromStdString(doc.toJson().toStdString());
}

QString Serialization::loadReq(QString fileName)
{
    QJsonDocument doc;
    status(&doc, LOAD_REQ);
    file(&doc, fileName);

    return QString::fromStdString(doc.toJson().toStdString());
}

/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


QString Serialization::createResp()
{
    QJsonDocument doc;
    status(&doc, CREATE_RESP);
    QJsonObject obj = doc.object();
    QJsonArray arr;
    obj.insert("list", arr);
    doc.setObject(obj);

    return QString::fromStdString(doc.toJson().toStdString());
}

QString Serialization::addResp(Capital *cap)
{
    QJsonDocument doc;
    status(&doc, ADD_RESP);

    return capitalToJson(cap, &doc);
}

QString Serialization::editResp(Capital *cap)
{
    QJsonDocument doc;
    status(&doc, EDIT_RESP);

    return capitalToJson(cap, &doc);
}

QString Serialization::remResp(Capital *cap)
{
    QJsonDocument doc;
    status(&doc, REMOVE_RESP);

    return capitalToJson(cap, &doc);
}

QStringList Serialization::getFileResp(QString * str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str->toStdString()));
    QStringList list;

    if(doc.isEmpty())
    {
        return list;
    }

    QJsonArray arr = doc.object().value("Files").toArray();

    for (int i = 0; i < arr.size(); ++i)
    {
        QJsonObject obj = arr.at(i).toObject();
        QString file = obj.value("filename").toString() + "(capitals:" + QString::number(obj.value("Number of capitals").toInt()) + ")";
        list.push_back(file);
    }

    return list;
}

QString Serialization::fileResp()
{

    QDir directin ("../server/data");
    directin.cd("data");

    directin.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QFileInfoList infoList = directin.entryInfoList();
    QStringList fileList;

    for (int i = 0; i < infoList.size(); ++i)
    {
       QString fileName = infoList.at(i).fileName();
       QString format = fileName.right(5);
       if(format == ".json")
       fileList.push_back(infoList.at(i).fileName());
    }

    QJsonDocument doc;
    status(&doc, ALLFILES_RESP);
    QJsonArray arr;
    QJsonObject docObj = doc.object();

    for (int i = 0; i < fileList.size(); i++)
    {
        QJsonObject obj;
        obj.insert("filename", fileList.at(i));
        QString file = fileList.at(i);
        obj.insert("size", (int)getFileSize((char *)("../server/data/" + file).toStdString().c_str()) + 1);
        QList<Capital *> capitals = load(file);
        obj.insert("Number of capitals", capitals.count());
        arr.append(obj);
    }

    docObj.insert("Files", arr);
    doc.setObject(docObj);

    return QString::fromStdString(doc.toJson().toStdString());
}

QString Serialization::files()
{

    QDir directin ("../server/data");
    directin.cd("data");

    directin.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QFileInfoList infoList = directin.entryInfoList();
    QStringList fileList;

    for (int i = 0; i < infoList.size(); ++i)
    {
       QString fileName = infoList.at(i).fileName();
       QString format = fileName.right(5);
       if(format == ".json")
       fileList.push_back(infoList.at(i).fileName());
    }

    QJsonDocument doc;
    status(&doc, SAVE_RESP);
    QJsonArray arr;
    QJsonObject docObj = doc.object();

    for (int i = 0; i < fileList.size(); i++)
    {
        QJsonObject obj;
        obj.insert("filename", fileList.at(i));
        QString file = fileList.at(i);
        obj.insert("size", (int)getFileSize((char *)("../server/data/" + file).toStdString().c_str()) + 1);
        QList<Capital *> capitals = load(file);
        obj.insert("Number of capitals", capitals.count());
        arr.append(obj);
    }

    docObj.insert("Files", arr);
    doc.setObject(docObj);

    return QString::fromStdString(doc.toJson().toStdString());
}

QString Serialization::listResp(QList<Capital *> list)
{
    QJsonDocument doc;
    status(&doc, SERVERLIST_RESP);

    return capitalsToJson(list, &doc);
}



QString Serialization::saveResp(QString fileName, QList<Capital *> list)
{
    Serialization json;
    QJsonDocument file;
    QString capitals = json.capitalsToJson(list, &file);

    readBufferToFile((char *)("../server/data/" + fileName).toStdString().c_str(), capitals.toStdString().c_str());

    return files();
}

QList<Capital *> Serialization::load(QString fileName)
{

    long lenOfFile = getFileSize((char *)("../server/data/" + fileName).toStdString().c_str()) + 1;
    char text[lenOfFile];
    int bytesRead = readFileToBuffer((char *)("../server/data/" + fileName).toStdString().c_str(), &text[0], (int)lenOfFile);
    text[bytesRead] = '\0';

    QString str = QString::fromStdString(text);

    return jsonToCapitals(&str);
}

/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


QString Serialization::capitalToJson(Capital * cap)
{
    QJsonDocument doc;

    return Serialization::capitalToJson(cap, &doc);
}


QString Serialization::capitalToJson(Capital * cap, QJsonDocument * doc)
{
    QJsonObject obj = doc->object();
    QJsonObject capObj;

    capObj.insert("Name", cap->getName());
    capObj.insert("Square", cap->getSquare());
    capObj.insert("Population", cap->getPopulation());
    capObj.insert("Id", cap->getId());
    capObj.insert("Longtitude", cap->getLongtitude());
    capObj.insert("Latitude", cap->getLatitude());

    obj.insert("Capital", capObj);

    doc->setObject(obj);

    return QString::fromStdString(doc->toJson().toStdString());
}

QString Serialization::capitalsToJson(QList<Capital *> list, QJsonDocument * doc)
{
    QJsonObject obj;

    if(!doc->isEmpty())
    {
        obj = doc->object();
    }

    QJsonArray arr;

    for(int i = 0; i < list.size(); i++)
    {
        QJsonObject capObj;
        Capital *cap = list.at(i);
        capObj.insert("Name", cap->getName());
        capObj.insert("Square", cap->getSquare());
        capObj.insert("Population", cap->getPopulation());
        capObj.insert("Id", cap->getId());
        capObj.insert("Longtitude", cap->getLongtitude());
        capObj.insert("Latitude", cap->getLatitude());
        arr.append(capObj);
    }

    obj.insert("Capitals", arr);
    doc->setObject(obj);

    return QString::fromStdString(doc->toJson().toStdString());
}

QList<Capital *> Serialization::jsonToCapitals(QString *str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str->toStdString()));
    QList<Capital *> list;

    if(!doc.isEmpty())
    {
        QJsonObject obj = doc.object();
        QJsonArray arr = obj.value("Capitals").toArray();

        for (int i = 0; i < arr.size(); ++i)
        {
           QJsonObject capObj = arr.at(i).toObject();
           list.push_back(new Capital(capObj.value("Name").toString(),
                                      capObj.value("Square").toInt(),
                                      capObj.value("Population").toDouble(),
                                      capObj.value("Longtitude").toDouble(),
                                      capObj.value("Latitude").toDouble()));

           list.back()->setId(capObj.value("Id").toInt());
        }
    }

    return list;
}

/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


int Serialization::getReqStatus(QString *str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str->toStdString()));
    int reqStatus = doc.object().value("Status").toInt();

    return reqStatus;
}

int Serialization::getRespStatus(QString *str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str->toStdString()));
    int respStatus = doc.object().value("Status").toInt();

    return respStatus;
}

QString Serialization::getFileName(QString *str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str->toStdString()));
    QString fileName = doc.object().value("File").toString();

    return fileName;
}

/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


Capital * Serialization::jsonToCapital(QString *str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str->toStdString()));
    QJsonObject capObj = doc.object().value("Capital").toObject();

    Capital * cap = new Capital(capObj.value("Name").toString(),
                                 capObj.value("Square").toInt(),
                                 capObj.value("Population").toDouble(),
                                 capObj.value("Longtitude").toDouble(),
                                 capObj.value("Latitude").toDouble());

    cap->setId(capObj.value("Id").toInt());

    return cap;
}


