#ifndef SERIALIZATION_H
#define SERIALIZATION_H
#include <QJsonDocument>
#include <capital.h>

enum
{
    CREATE_REQ,
    ADD_REQ,
    EDIT_REQ,
    REMOVE_REQ,
    ALLFILES_REQ,
    SERVERLIST_REQ,
    SAVE_REQ,
    LOAD_REQ,

    CREATE_RESP,
    ADD_RESP,
    EDIT_RESP,
    REMOVE_RESP,
    ALLFILES_RESP,
    SERVERLIST_RESP,
    SAVE_RESP,
    LOAD_RESP
};

class Serialization
{
public:
    Serialization();

    QString createReq();
    QString addReq(Capital *cap);
    QString editReq(Capital *cap);
    QString remReq(Capital *cap);
    QString fileReq();
    QString listReq();
    QString saveReq(QString fileName);
    QString loadReq(QString fileName);

    QString createResp();
    QString addResp(Capital *cap);
    QString editResp(Capital *cap);
    QString remResp(Capital *cap);
    QString fileResp();
    QString listResp(QList<Capital *> capitals);
    QString saveResp(QString fileName, QList<Capital *> list);

    void status(QJsonDocument * doc, int status);
    void file(QJsonDocument * doc, QString fileName);
    QString files();

    QList<Capital *> load(QString fileName);

    int getReqStatus(QString *str);
    int getRespStatus(QString *str);
    QString getFileName(QString *str);

    QStringList getFileResp(QString *str);

    QString capitalsToJson(QList<Capital *> capitals, QJsonDocument *doc);
    QList <Capital *> jsonToCapitals(QString *str);

    QString capitalToJson(Capital *cap);
    QString capitalToJson(Capital *cap, QJsonDocument *doc);
    Capital * jsonToCapital(QString *str);
};

#endif // SERIALIZATION_H
