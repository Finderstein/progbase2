#-------------------------------------------------
#
# Project created by QtCreator 2018-05-30T19:33:19
#
#-------------------------------------------------

QT       -= gui

TARGET = capitalsLib
TEMPLATE = lib

DEFINES += CAPITALSLIB_LIBRARY

SOURCES += serialization.cpp \
    capital.cpp

HEADERS += serialization.h\
        capitalslib_global.h \
    capital.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
