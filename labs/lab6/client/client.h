#ifndef CLIENT_H
#define CLIENT_H

#include <QWidget>
#include <QHostAddress>
#include <capital.h>

namespace Ui
{
class Client;
}

class Client : public QWidget
{
    Q_OBJECT

public:
    explicit Client(QWidget *parent = 0);
    ~Client();

private:
    Ui::Client *ui;
    QHostAddress _ip;
    quint16 _hostPort;
    QString _req;

    void listFile(QString);
    void clearCapital();

public slots:
    void Connection();
    void onConnected();
    void onReadyRead();
    void onDisconnected();

    void getCapital(Capital *cap);
    void getEdited(Capital *cap);

private slots:


    void on_files_btn_clicked();

    void on_create_btn_clicked();

    void on_edit_btn_clicked();

    void on_del_btn_clicked();

    void on_cap_list_itemSelectionChanged();

    void on_add_btn_clicked();

    void on_serverList_btn_clicked();

    void on_load_btn_clicked();

    void on_save_btn_clicked();

    void on_savefile_line_editingFinished();

signals:
    void sendEditedCapital(Capital *cap);
};

#endif // CLIENT_H
