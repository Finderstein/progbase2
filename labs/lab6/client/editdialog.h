#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include "capital.h"

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(QWidget *parent = 0);
    ~EditDialog();

private slots:
    void on_buttonBox_accepted();
public slots:
    void getCapital(Capital *cap);
signals:
    void sendCapital(Capital *cap);
private:
    Ui::EditDialog *ui;
};

#endif // EDITDIALOG_H
