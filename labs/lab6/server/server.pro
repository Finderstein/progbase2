QT += core network
QT -= gui

CONFIG += c++11

TARGET = server
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    server.cpp \
    storage.cpp

HEADERS += \
    server.h \
    storage.h


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-capitalsLib-Desktop-Debug/release/ -lcapitalsLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-capitalsLib-Desktop-Debug/debug/ -lcapitalsLib
else:unix: LIBS += -L$$PWD/../build-capitalsLib-Desktop-Debug/ -lcapitalsLib

INCLUDEPATH += $$PWD/../capitalsLib
DEPENDPATH += $$PWD/../capitalsLib
