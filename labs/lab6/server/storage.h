#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <QList>
#include "capital.h"

class Storage : public QObject
{
    Q_OBJECT
    QList<Capital *> list;
public:
    explicit Storage(QObject *parent = 0);

    void addToStorage(Capital *cap);
    void editAtStorage(Capital *cap);
    void removeFromStorage(Capital *cap);
    void clearStorage();
    QList<Capital *> getList();
    void setList(QList<Capital *>);

    Capital * lastAtStorage();

signals:

public slots:
};

#endif // STORAGE_H
