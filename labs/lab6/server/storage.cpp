#include "storage.h"

Storage::Storage(QObject *parent) : QObject(parent)
{
}

void Storage::addToStorage(Capital * cap)
{
    int id = 0;
    if(!list.isEmpty()) {
      id = list.back()->getId();
    }
    id++;
    cap->setId(id);
    list.push_back(cap);
}

void Storage::editAtStorage(Capital * cap)
{
    for (int i = 0; i < list.size(); ++i) {
       Capital * editCap = list.at(i);
       if(editCap->getId() == cap->getId()) {
           list.replace(i, cap);
       }
    }
}

void Storage::removeFromStorage(Capital * cap)
{
    for (int i = 0; i < list.size(); ++i) {
        Capital * delCap = list.at(i);
        if(delCap->getId() == cap->getId()) {
            list.removeAt(i);
        }
    }
}

void Storage::clearStorage()
{
    if(!list.isEmpty()) {
        list.clear();
    }
}

Capital * Storage::lastAtStorage()
{
    if(!list.isEmpty()) {
        Capital * cap = list.back();
        return cap;
    }
    return nullptr;
}

QList<Capital *> Storage::getList()
{
    return this->list;
}

void Storage::setList(QList<Capital *> list)
{
    this->list = list;
}


