#include <stdio.h>
#include <progbase.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include <cui.h>


void frame(void)
{
    Console_setCursorAttribute(BG_GREEN);

    for(int i = 1; i < 48; i++)
    {
        Console_setCursorPosition(i, 70);
        puts(" ");
    } 

    Console_setCursorPosition(30, 1);    
    for(int i = 1; i < 71; i++)
    {
        printf(" ");
    }
    Console_setCursorAttribute(BG_DEFAULT);
}

void frame1(int error, List *array)
{
    Console_clear();
    Console_reset();
    Console_setCursorPosition(1, 1);
    Console_setCursorAttribute(BG_BLUE);
    puts("Choose how you will modify the array:");
    Console_setCursorAttribute(BG_DEFAULT);    
    puts("1.Add a new entity to the end of the list with the specified \nuser data.\n"
        "\n2.Delete the entity from the specified position in the list.\n"
        "\n3.Overwrite all entity field data in the specified position on \nthe user-initiated (full entity update).\n"
        "\n4.Overwrite only the selected data field of the entity from the \nspecified position in the list (partial refresh of the entity).\n"
        "\n5.Find all the capital cities in which the longitude is greater \nthan X.\n"
        "\n6.Save the current list of entities to the file system, prompting \nthe user to enter the name of the disk file, which will store all \ndata.\n"
        "\n7.Exit"
    );

    printCapitals(10, array);
    frame();

    if(error == 1)
    {
        Console_setCursorAttribute(BG_RED);
        Console_setCursorPosition(31, 1);
        puts("There is nothing to delete!");
        Console_setCursorAttribute(BG_DEFAULT);        
        Console_setCursorPosition(32, 1);
        puts("Put the number of task:");
    }
    else if(error == 2)
    {
        Console_setCursorAttribute(BG_RED);
        Console_setCursorPosition(31, 1);
        puts("There is nothing to change!");
        Console_setCursorAttribute(BG_DEFAULT);        
        Console_setCursorPosition(32, 1);
        puts("Put the number of task:");
    }
    else if(error == 3)
    {
        Console_setCursorAttribute(BG_RED);
        Console_setCursorPosition(31, 1);
        puts("The array is already full!");
        Console_setCursorAttribute(BG_DEFAULT);        
        Console_setCursorPosition(32, 1);
        puts("Put the number of task:");
    }
    else if(error == 4)
    {
        Console_setCursorAttribute(BG_RED);
        Console_setCursorPosition(31, 1);
        puts("Invalid name of field!");
        Console_setCursorAttribute(BG_DEFAULT);        
        Console_setCursorPosition(32, 1);
        puts("Put the number of task:");
    }
    else
    {
        Console_setCursorPosition(31, 1);
        puts("Put the number of task:");
    }
}

void printCapitals(int len, List *array)
{
    int count = List_count(array);
    int line = 0;
    for (int i = 0; i < count; i++)
    {
        Capital *cur = List_at(array, i);
        
        Console_setCursorPosition(1 + line, 71);
        Console_setCursorAttribute(BG_YELLOW);
        printf("Capital[%i]\n", i + 1);  
        Console_setCursorAttribute(BG_DEFAULT);        
        Console_setCursorPosition(2 + line, 71);        
        printf("Name:%s\n", cur->name);
        Console_setCursorPosition(3 + line, 71);        
        printf("Square:%i\n", cur->square);
        Console_setCursorPosition(4 + line, 71);        
        printf("Population:%.2f\n", cur->population);
        Console_setCursorPosition(5 + line, 71);        
        printf("Geographical latitude:%i\n", cur->coordinates->geoLatitude); 
        Console_setCursorPosition(6 + line, 71);        
        printf("Geographical longitude:%i\n", cur->coordinates->geoLongitude);      
        
        Console_setCursorPosition(7 + line, 71);        
        puts("================================================================");
        line += 7;
        
    }
}