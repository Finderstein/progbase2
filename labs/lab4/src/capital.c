#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <capital.h>

static const int LENGTH = 10;

void Capital_free(Capital *self)
{
    free(self->name);
    free(self->coordinates);
    free(self);    
}

//if structs are equal
bool capitalEquals(Capital *a,  Capital *b)
{
    if(a == NULL || b == NULL) return false;
    return !strcmp(a->name, b->name) 
        && (a->square == b->square) 
        && fabs(a->population - b->population) < 1e-4 
        && (a->coordinates->geoLatitude == b->coordinates->geoLatitude) 
        && (a->coordinates->geoLongitude == b->coordinates->geoLongitude);
}

//creating new struct whith new values
Capital *newCapital(const char *name, int square, float population, int geoLatitude, int geoLongitude)
{
    Capital *newCapital = malloc(sizeof(Capital));
    newCapital->coordinates = malloc(sizeof(Coordinates));  
    newCapital->name = malloc(strlen(name) + 1);  
    strcpy(newCapital->name, name);
    newCapital->square = square;
    newCapital->population = population;
    newCapital->coordinates->geoLatitude = geoLatitude;
    newCapital->coordinates->geoLongitude = geoLongitude;    

    return newCapital;
}

//search structs with geoLongtitude bigger than X
void searchLongtitude(int x, List *array, List *arrayLon)
{
    int count = List_count(array);      
    
    for (int i = 0; i < count; i++)
    {
        Capital *cur = List_at(array, i);
        if(cur->coordinates->geoLongitude > x)
        {
            List_add(arrayLon, newCapital(cur->name, cur->square, cur->population, cur->coordinates->geoLatitude, cur->coordinates->geoLongitude));
        }
    }
}

//overwriting some field in the struct
int overwritingSelectedField(Capital *cap, const char *field, const char *newValue)
{
    if(!strcmp("name", field))
    {
        free(cap->name);
        cap->name = malloc(strlen(newValue) + 1);
        strcpy(cap->name, newValue);
    }
    else if(!strcmp("square", field))
    {
        cap->square = atoi(newValue);
    }
    else if(!strcmp("population", field))
    {
        cap->population = atof(newValue);
        
    }
    else if(!strcmp("latitude", field))
    {
        cap->coordinates->geoLatitude = atoi(newValue);        
    }
    else if(!strcmp("longitude", field))
    {
        cap->coordinates->geoLongitude = atoi(newValue);                
    }
    else
    {
        return 4;
    }

    return 0;
}