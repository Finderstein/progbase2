 #include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h>

#include <csv.h>
#include <capital.h>
#include <string_buffer.h>

static const int LENGTH = 10;






struct __CsvTable 
{
    List *rows;
};

struct __CsvRow 
{
    List *value;
};


CsvRow * CsvRow_new(void)
{
    CsvRow * self = malloc(sizeof(CsvRow));
    self->value = List_new();

    return self;
}

void CsvRow_free(CsvRow * self)
{
    List * l = self->value;
    for(int i = 0; i < List_count(l); i++)
    {
        free(List_at(l, i));
    }
    List_free(l);

    free(self->value);
    free(self);
}

void CsvRow_add(CsvRow * self, char * value)
{
    List_add(self->value, value);
}

static void List_copyTo(List *self, List * dest)
{
    List * vals = self;
    for(int i = 0; i < List_count(vals); i++)
    {
        //char???
        void * value = List_at(vals, i);
        List_add(dest, value);
    }
}

void CsvRow_values(CsvRow * self, List * values)
{
    List_copyTo(self->value, values);
}


CsvTable * CsvTable_new(void)
{
    CsvTable * self = malloc(sizeof(CsvTable));
    self->rows = List_new();

    return self;
}

void CsvTable_free(CsvTable * self)
{
    List *l = self->rows;
    for(int i = 0; i < List_count(l); i++)
    {
        CsvRow *t = List_at(l, i);
        CsvRow_free(t);
    }
    List_free(l);
    
    free(self->rows);
    free(self);
}

void CsvTable_add (CsvTable * self, CsvRow * row)
{
    List_add(self->rows, row);    
}

void CsvTable_rows(CsvTable * self, List * rows)
{
    List_copyTo(self->rows, rows);
}


CsvTable * CsvTable_newFromString(const char * csvString)
{
    CsvTable* t = CsvTable_new();
    CsvRow *row = CsvRow_new();
    StringBuffer * value = StringBuffer_new();
    while(1)
    {
        char ch = *csvString;
        if(ch == '\0')
        {
           
            CsvRow_add(row, StringBuffer_toNewString(value));
            StringBuffer_clear(value);
            //
            CsvTable_add(t, row);

            break;
        }
        else if(ch == ',')
        {
            
            CsvRow_add(row, StringBuffer_toNewString(value));
            StringBuffer_clear(value);
        }
        else if(ch == '\n')
        {
            
            CsvRow_add(row, StringBuffer_toNewString(value));
            StringBuffer_clear(value);
            //
            CsvTable_add(t, row);
            row = CsvRow_new();
        }
        else
        {
            StringBuffer_appendChar(value, ch);
        }

        csvString++;
    }
    StringBuffer_free(value);


    return t;
}

char * CsvTable_toNewString  (CsvTable * self)
{
    StringBuffer *sb = StringBuffer_new();

    for(int ri = 0; ri < List_count(self->rows); ri++)
    {
        if(ri != 0)
        {
            StringBuffer_appendChar(sb, '\n');
        }
        CsvRow *row = List_at(self->rows, ri);
        int nvalues = List_count(row->value);

        for(int vi = 0; vi < nvalues; vi++)
        {
            if(vi != 0)
            {
                StringBuffer_appendChar(sb, ',');
            }
            char * value = List_at(row->value, vi);
            StringBuffer_append(sb, value);
        }
    }

    char * result = StringBuffer_toNewString(sb); 
    
    StringBuffer_free(sb);
    CsvTable_free(self);

    return result;
}























































//saving values of struct into the string
int saveCapitalToString(char *buffer, int bufferLength, Capital *capital)
{
    int nwrite = 0;
    if(capital == NULL)
    {
        strcpy(buffer, "");
        return 0;
    } 
    snprintf(buffer, bufferLength, "%s,%i,%.2f,%i,%i%n", capital->name, capital->square, capital->population, capital->coordinates->geoLatitude, capital->coordinates->geoLongitude, &nwrite);
    buffer[strlen(buffer)] = '\0';    
    strcat(buffer, "\n");    
    nwrite++;    
    return nwrite;
}

//saving values of whole array into the string
int saveArrayToString(char *buffer, int bufferLength, List *array)
{
    int nwrite = 0;
    int count = List_count(array);
    for (int i = 0; i < count; i++)
    {
        Capital * capital = List_at(array, i);
        nwrite += saveCapitalToString(buffer + nwrite, bufferLength - nwrite, capital);
    }

    return nwrite;
}

//making new struct whith values from string
Capital *saveStringToCapital(const char *str)
{
    char name[100];
    int square = 2;
    float population = 0;
    int x = 0;
    int y = 0;
    sscanf(str, "%99[^,]%*c %i,%f,%i,%i", name, &square, &population, &x, &y);
    Capital *newCap = newCapital(name, square, population, x, y);
    /* printf("%i", square);
    assert(0); */

    return newCap;
}

//making new array with values from string
int saveStringToArray(const char *str, List *array)
{
    int nread = 0;
    int totalRead = 0;
    //int len = List_count(array);
    int lenOfStr = strlen(str);  
    for(int i = 0; i < LENGTH && totalRead < lenOfStr - 1; i++)
    {
        Capital *cap = saveStringToCapital(str + totalRead);
        List_add(array, cap);
        char buf[1000];
        sscanf(str + totalRead, "%[^\n]%*c%n", buf, &nread);
        totalRead += nread;
    }
    
    return totalRead;
}