#include <stdio.h>
#include <stdlib.h>

#include <list.h>
#include <capital.h>

struct __List
{
    void **capitals;
    int length;
    int capacity;
};

List * List_new(void)
{
    List *new = malloc(sizeof(List));
    new->capitals = malloc(sizeof(void *) * 10);
    new->length = 0;
    new->capacity= 10;

    return new;
}

void List_clear(List *self)
{
    for(int i = self->length - 1; i >= 0; i--)
    {
        Capital_free(self->capitals[i]);
        self->length = self->length - 1;
    }

}

void List_free(List * self)
{
    for(int i = 0; i < self->length; i++)
    {
        Capital_free(self->capitals[i]);
    }
    free(self->capitals);
    free(self);    
}

void List_insert(List * self, void * value, size_t index)
{
    for(int i = self->length; i > index; i--)
    {
        self->capitals[i] = self->capitals[i - 1];
    }
    self->capitals[index] = value;

    return;
}

void List_add(List * self, void * value)
{
    if(self->length == self->capacity) 
    {
        
        self->capacity *=2;
        self->capitals = realloc(self->capitals, self->capacity * sizeof(void *));
    }

    self->capitals[self->length] = value;
    self->length += 1;
}

void * List_at(List * self, size_t index)
{
    return self->capitals[index];
}

void * List_set(List * self, size_t index, void * value)
{
    Capital_free(self->capitals[index]);
    void * prev = self->capitals[index];
    self->capitals[index] = value;
    return prev;
}

void List_removeAt(List * self, size_t index)
{
    Capital_free(self->capitals[index]);
    for(int i = index; i < self->length - 1; i++)
    {
        self->capitals[i] = self->capitals[i + 1]; 
    }
    self->length -= 1;
  
}

size_t List_count(List * self)
{
    return self->length;
}
