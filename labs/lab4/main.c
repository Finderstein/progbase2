#include <stdio.h>
#include <progbase.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include <file_sys.h>
#include <cui.h>
#include <csv.h>
#include <list.h>
#include <capital.h>


const int LENGTH = 10;

//tests
//void tests(void);

//modify array of capitals
void modifyArray(List *array);

int main(int argc, char *argv[])
{
    if(argc == 2 && !strcmp(argv[1], "-t"))
    {
        //tests();
        return 0;
    }
    else if (argc == 1)
    {
        Console_clear();
        Console_reset();
    
        int mode = 0;
        int error = 0;
    
        List *array = List_new();
    
        while(mode != 3)
        {
            Console_clear();
            Console_reset();
    
            Console_setCursorAttribute(BG_BLUE);
            puts("Select mode in which you would like to work:");
            Console_setCursorAttribute(BG_DEFAULT);
            puts("1.Create new array.\n"
                "\n2.Create array from the text file.\n"
                "\n3.Exit\n"
            );
            if(error == 1)
            {
                Console_setCursorAttribute(BG_RED);
                puts("This file doesn't exists!");
                Console_setCursorAttribute(BG_DEFAULT);                
            }
            error = 0;
    
            puts("Select mode: ");
            if(!scanf("%i", &mode))
            {
                getchar();
                continue;
            }    
         
            if(mode == 1)
            {
                modifyArray(array);
            }
            else if(mode == 2)
            {
                char fileName[100];
                puts("Put the name of the file:");
                getchar();
                fgets(fileName, 99, stdin);
                fileName[strlen(fileName) - 1] = '\0';        
                if(fileExists(fileName))
                {
                    long lenOfFile = getFileSize(fileName) + 1; 
                    char text[lenOfFile];
                    int bytesRead = readFileToBuffer(fileName, &text[0], (int)lenOfFile);
                    text[bytesRead] = '\0';
    
                    saveStringToArray(text, array);
    
                    modifyArray(array);
                }
                else
                {
                    error = 1;
                }
            }
        }

        List_free(array);
    
    
        Console_clear();
        Console_reset();
    }

    return 0;
}

void modifyArray(List *array)
{
    Console_clear();
    Console_reset();

    int task = 0;
    int error = 0;

    List *arrayLon = List_new();
    

    while(task != 7)
    {
        if(error == 5)
        {
            frame1(error, arrayLon);
            List_clear(arrayLon);
        }
        else
        {
            frame1(error, array);
        }
        error = 0;

        if(!scanf("%i", &task))
        {
            getchar();
            continue;
        }  
     
        if(task == 1)
        {
            int length = List_count(array);
            if (length == LENGTH)
            {
                error = 3;
            }
            else
            {
                char name[100];
                puts("Put the name of the capital:");
                getchar();
                fgets(name, 99, stdin);
                name[strlen(name) - 1] = '\0';           
                int square = 0;
                puts("Put the square of the capital:");            
                if(!scanf("%i", &square))
                {
                    getchar();
                    continue;
                }
                float population = 0;
                puts("Put the population of the capital:");            
                if(!scanf("%f", &population))
                {
                    getchar();
                    continue;
                }    
                int geoLatitude = 0;
                puts("Put the geographical latitude of the capital:");            
                if(!scanf("%i", &geoLatitude))
                {
                    getchar();
                    continue;
                }
                int geoLongtitude = 0;
                puts("Put the geographical longitude of the capital:");                        
                if(!scanf("%i", &geoLongtitude))
                {
                    getchar();
                    continue;
                } 

                char cont[100];
                puts("You sure whant to change array?");
                while(1)
                {
                    getchar();
                    fgets(cont, 4, stdin);
                    puts(cont);
                    if(!strcmp(cont, "yes"))
                    {
                        List_add(array, newCapital(name, square, population, geoLatitude, geoLongtitude));
                        break;
                    }
                    if(!strcmp(cont, "no\n"))
                    {
                        break;
                    }
                }
                
                
                
            }    
        }
        else if(task == 2)
        {
            int index = 0;
            puts("Put the index of capital you would like to delete:");
            if(!scanf("%i", &index))
            {
                getchar();
                continue;
            }
            index--;

            int length = List_count(array);
            if (index < 0 || index >= length)
            {
                error = 1;
            }
            else
            {
                char cont[100];
                puts("You sure whant to change array?");
                while(1)
                {
                    getchar();
                    fgets(cont, 4, stdin);
                    if(!strcmp(cont, "yes"))
                    {
                        List_removeAt(array, index); 
                        break;
                    }
                    if(!strcmp(cont, "no\n"))
                    {
                        break;
                    }
                }
                   
            }
        }
        else if(task == 3)
        {
            int index = 0;
            puts("Put the index of capital you would like to change:");
            if(!scanf("%i", &index))
            {
                getchar();
                continue;
            }
            index--;

            int length = List_count(array); 
            if (index < 0 || index >= length)
            {
                error = 2;
            }
            else
            {
                char name[100];
                puts("Put the name of the capital:");
                getchar();
                fgets(name, 99, stdin);
                name[strlen(name) - 1] = '\0';           
                int square = 0;
                puts("Put the square of the capital:");            
                if(!scanf("%i", &square))
                {
                    getchar();
                    continue;
                }
                float population = 0;
                puts("Put the population of the capital:");            
                if(!scanf("%f", &population))
                {
                    getchar();
                    continue;
                }    
                int geoLatitude = 0;
                puts("Put the geographical latitude of the capital:");            
                if(!scanf("%i", &geoLatitude))
                {
                    getchar();
                    continue;
                }
                int geoLongtitude = 0;
                puts("Put the geographical longitude of the capital:");                        
                if(!scanf("%i", &geoLongtitude))
                {
                    getchar();
                    continue;
                } 
                
                char cont[100];
                puts("You sure whant to change array?");
                while(1)
                {
                    getchar();
                    fgets(cont, 4, stdin);
                    if(!strcmp(cont, "yes"))
                    {
                        List_set(array, index, newCapital(name, square, population, geoLatitude, geoLongtitude));
                        break;
                    }
                    if(!strcmp(cont, "no\n"))
                    {
                        break;
                    }
                }

                
            }          
        }
        else if(task == 4)
        {
            int index = 0;
            puts("The index of capital you wouldlike to change:");
            if(!scanf("%i", &index))
            {
                getchar();
                continue;
            } 
            index--;
            
            int length = List_count(array);
            if (index < 0 || index >= length)
            {
                error = 2;
            }
            else
            {
                char field[100];
                puts("Put the name of field you would like to change:\n1)name\n2)square\n3)population\n4)latitude\n5)longitude");
                getchar();
                fgets(field, 99, stdin);
                field[strlen(field) - 1] = '\0';                                   
                char newValue[100];
                puts("Put the new value:");
                fgets(newValue, 99, stdin);
                newValue[strlen(newValue) - 1] = '\0';

                char cont[100];
                puts("You sure whant to change array?");
                while(1)
                {
                    getchar();
                    fgets(cont, 4, stdin);
                    if(!strcmp(cont, "yes"))
                    {
                        error = overwritingSelectedField(List_at(array, index), field, newValue);
                        break;
                    }
                    if(!strcmp(cont, "no\n"))
                    {
                        break;
                    }
                }

                
            }    
        }
        else if(task == 5)
        {
            int x = 0;
            puts("Put the longitude:");
            if(!scanf("%i", &x))
            {
                getchar();
                continue;
            }
            searchLongtitude(x, array, arrayLon);      
            
            error = 5;
        }
        else if(task == 6)
        {
            puts("Put the name of file you would like to open or \ncreate(fileName.txt)");
            getchar();
            char fileName[100];
            char command[] = "touch ";
            fgets(fileName, 99, stdin);
            fileName[strlen(fileName) - 1] = '\0';
            if(!fileExists(fileName))
            {
                int bufLen = 1000;
                char buffer[bufLen];
                saveArrayToString(buffer, bufLen, array);
                char *csvString = CsvTable_toNewString(CsvTable_newFromString(buffer));
                //
                system(strcat(command, fileName));
                readBufferToFile(fileName, csvString);     
                free(csvString);   
            }                         
            else
            {
                int bufLen = 1000;
                char buffer[bufLen];
                saveArrayToString(buffer, bufLen, array);

                char *csvString = CsvTable_toNewString(CsvTable_newFromString(buffer));
                readBufferToFile(fileName, buffer);
                free(csvString);
            }
        }
    }

    //List_free(array);
    List_free(arrayLon);    
}
