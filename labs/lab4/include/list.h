#pragma once 

#include <stdio.h>
#include <stdlib.h>

typedef struct __List List;
typedef struct __Capital Capital;
typedef struct __Coordinates Coordinates;



struct __Capital
{
    char *name;
    int square;
    float population;
    int length;
    Coordinates * coordinates;
};

struct __Coordinates
{
    int geoLatitude;
    int geoLongitude;
};


List * List_new(void);
void List_clear(List *self);
void List_free(List * self);

void List_insert(List * self, void * value, size_t index);
void List_add(List * self, void * value);
void * List_at(List * self, size_t index);
void * List_set(List * self, size_t index, void * value);
void List_removeAt(List * self, size_t index);

size_t List_count(List * self);
