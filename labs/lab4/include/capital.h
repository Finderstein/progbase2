#pragma once
#include <stdbool.h>

#include <list.h>

void Capital_free(Capital *self);

//if structs are equal
bool capitalEquals(Capital *a, Capital *b);

//creating new struct whith new values
Capital *newCapital(const char *name, int square, float population, int geoLatitude, int geoLongitude);

//search structs with geoLongtitude bigger than X
void searchLongtitude(int x, List *array, List *arrayLon);

//overwriting some field in the struct
int overwritingSelectedField(Capital *cap, const char *field, const char *newValue);