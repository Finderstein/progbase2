#pragma once

#include <list.h>


typedef struct __CsvTable CsvTable;
typedef struct __CsvRow   CsvRow;

CsvRow * CsvRow_new(void);
void CsvRow_free(CsvRow * self);
void CsvRow_add(CsvRow * self, char * value);
void CsvRow_values(CsvRow * self, List * values);

CsvTable * CsvTable_new(void);
void CsvTable_free(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
void CsvTable_rows(CsvTable * self, List * rows);

CsvTable * CsvTable_newFromString(const char * csvString);
char * CsvTable_toNewString  (CsvTable * self);

//saving values of struct into the string
int saveCapitalToString(char *buffer, int bufferLength, Capital *capital);

//saving values of whole array into the string
int saveArrayToString(char *buffer, int bufferLength, List *array);

//making new struct whith values from string
Capital *saveStringToCapital(const char *str);

//making new array with values from string
int saveStringToArray(const char *str, List *array);
