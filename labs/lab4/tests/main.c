#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <check.h>

#include <file_sys.h>
#include <cui.h>
#include <csv.h>
#include <list.h>
#include <capital.h>

//List_add(array, newCapital(name, square, population, geoLatitude, geoLongtitude));

START_TEST (split_list_lenOne)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   ck_assert_int_eq(List_count(capitals), 1);
   List_free(capitals);
}
END_TEST

START_TEST (split_list_lenMany)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   List_add(capitals, newCapital("a", 13, 25, 135, 65));
   List_add(capitals, newCapital("d", 13, 25, 135, 65));
   List_add(capitals, newCapital("g", 13, 25, 135, 65));
   List_add(capitals, newCapital("u", 13, 25, 135, 65));
   List_add(capitals, newCapital("v", 13, 25, 135, 65));
   ck_assert_int_eq(List_count(capitals), 6);
   List_free(capitals);
}
END_TEST

START_TEST (split_list_capEquals)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   ck_assert_int_eq(List_count(capitals), 1);
   Capital * cap1 = newCapital("abc", 13, 25, 135, 65);
   ck_assert(capitalEquals(List_at(capitals, 0), cap1));
   Capital_free(cap1);
   List_free(capitals);
}
END_TEST

START_TEST (split_list_capNotEquals)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   List_add(capitals, newCapital("g", 13, 25, 135, 65));
   List_add(capitals, newCapital("s", 13, 25, 135, 65));
   ck_assert_int_eq(List_count(capitals), 3);
   Capital * cap1 = newCapital("abc", 13, 25, 135, 65);
   ck_assert(!capitalEquals(List_at(capitals, 1), cap1));
   Capital_free(cap1);
   List_free(capitals);
}
END_TEST

START_TEST (split_list_secCapEquals)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   List_add(capitals, newCapital("g", 13, 25, 135, 65));
   List_add(capitals, newCapital("s", 13, 25, 135, 65));
   List_add(capitals, newCapital("f", 13, 225, 6, 13));
   List_add(capitals, newCapital("k", 13, 25, 135, 65));
   List_add(capitals, newCapital("1s", 13, 25, 135, 65));   
   ck_assert_int_eq(List_count(capitals), 6);
   Capital * cap1 = newCapital("f", 13, 225, 6, 13);
   ck_assert(capitalEquals(List_at(capitals, 3), cap1));
   Capital_free(cap1);
   List_free(capitals);
}
END_TEST

START_TEST (split_list_writeField)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   List_add(capitals, newCapital("g", 13, 25, 135, 65));
   List_add(capitals, newCapital("s", 13, 25, 135, 65));
   List_add(capitals, newCapital("f", 13, 225, 6, 13));
   List_add(capitals, newCapital("k", 13, 25, 135, 65));
   List_add(capitals, newCapital("1s", 13, 25, 135, 65));   
   ck_assert_int_eq(List_count(capitals), 6);
   Capital * cap1 = newCapital("ktkj", 13, 225, 6, 13);
   ck_assert(!capitalEquals(List_at(capitals, 3), cap1));
   overwritingSelectedField(cap1, "name", "f");
   ck_assert(capitalEquals(List_at(capitals, 3), cap1));
   Capital_free(cap1);
   List_free(capitals);
}
END_TEST

START_TEST (split_list_clear)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   List_add(capitals, newCapital("g", 13, 25, 135, 65));
   List_add(capitals, newCapital("s", 13, 25, 135, 65));
   List_add(capitals, newCapital("f", 13, 225, 6, 13));
   List_add(capitals, newCapital("k", 13, 25, 135, 65));
   List_add(capitals, newCapital("1s", 13, 25, 135, 65));   
   ck_assert_int_eq(List_count(capitals), 6);
   List_clear(capitals);  
   ck_assert_int_eq(List_count(capitals), 0); 
   List_free(capitals);

}
END_TEST

START_TEST (split_list_toStr)
{
   List * capitals = List_new();
   List_add(capitals, newCapital("abc", 13, 25, 135, 65));
   int bufLen = 1000;
   char buffer[bufLen];
   saveArrayToString(buffer, bufLen, capitals);
   char *csvString = CsvTable_toNewString(CsvTable_newFromString(buffer));  
   ck_assert_int_eq(List_count(capitals), 1);
   ck_assert(!strcmp(csvString, "abc,13,25.00,135,65\n")); 
   free(csvString);    
   List_free(capitals);

}
END_TEST

START_TEST (split_list_fromStr)
{
   List * capitals = List_new();
   char * text = "ab,13,23.00,66,44\n";
   saveStringToArray(text, capitals);
   Capital * cap1 = newCapital("ab", 13, 23.00, 66, 44);   
   ck_assert_int_eq(List_count(capitals), 1);
   ck_assert(capitalEquals(List_at(capitals, 0), cap1));    
   Capital_free(cap1);   
   List_free(capitals);

}
END_TEST

START_TEST (split_list_lon)
{
    List * capitals = List_new();
    List_add(capitals, newCapital("abc", 13, 25, 135, 65));
    List * lon = List_new();   
    ck_assert_int_eq(List_count(capitals), 1);
    ck_assert_int_eq(List_count(lon), 0);   
    searchLongtitude(2, capitals, lon);
    ck_assert(capitalEquals(List_at(capitals, 0), List_at(lon, 0)));    
    List_free(lon);    
    List_free(capitals);

}
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Lexer");
    TCase *tc_sample = tcase_create("SplitTest");
    //
    tcase_add_test(tc_sample, split_list_lenOne);    
    tcase_add_test(tc_sample, split_list_lenMany); 
    tcase_add_test(tc_sample, split_list_capEquals);  
    tcase_add_test(tc_sample, split_list_capNotEquals); 
    tcase_add_test(tc_sample, split_list_secCapEquals);      
    tcase_add_test(tc_sample, split_list_writeField);      
    tcase_add_test(tc_sample, split_list_clear);     
    tcase_add_test(tc_sample, split_list_toStr);   
    tcase_add_test(tc_sample, split_list_fromStr);      
    tcase_add_test(tc_sample, split_list_lon);              
    
    
    
    //
    suite_add_tcase(s, tc_sample);
    return s;
}
   
int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);
   
    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}
   