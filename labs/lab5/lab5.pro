#-------------------------------------------------
#
# Project created by QtCreator 2018-04-22T17:55:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab5
TEMPLATE = app

CONFIG +=c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    capital.cpp \
    adddialog.cpp \
    editdialog.cpp

HEADERS  += mainwindow.h \
    capital.h \
    adddialog.h \
    editdialog.h

FORMS    += mainwindow.ui \
    adddialog.ui \
    editdialog.ui
