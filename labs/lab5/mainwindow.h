#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <capital.h>
#include <QListWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_add_btn_clicked();

    void on_capitals_widget_itemSelectionChanged();

    void on_rem_btn_clicked();

    void on_edit_btn_clicked();

    void on_exec_btn_clicked();

    void on_load_btn_clicked();

    void on_save_btn_clicked();

    void on_result_widget_itemClicked(QListWidgetItem *item);

private:
    Ui::MainWindow *ui;
signals:
    void sendCapital(Capital *);
public slots:
    void getCapital(Capital *);
    void editCapital(Capital *);
};

#endif // MAINWINDOW_H
