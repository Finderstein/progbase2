#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <capital.h>

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(QWidget *parent = 0);
    ~EditDialog();

private:
    Ui::EditDialog *ui;
public slots:
    void getCapital(Capital *);
signals:
    void returnCapital(Capital *);
private slots:
    void on_buttonBox_accepted();
};

#endif // EDITDIALOG_H
