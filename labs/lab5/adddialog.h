#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <capital.h>

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = 0);
    ~AddDialog();

private:
    Ui::AddDialog *ui;
signals:
    void sendCapital(Capital *);
private slots:
    void on_buttonBox_accepted();
};

#endif // ADDDIALOG_H
