#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <string>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonArray>
#include <fstream>

#include <adddialog.h>
#include <editdialog.h>
#include <capital.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getCapital(Capital *capital)
{
    QListWidgetItem *item = new QListWidgetItem();
    QVariant var;
    var.setValue(capital);
    item->setData(Qt::UserRole, var);
    item->setText(capital->caption());
    ui->capitals_widget->addItem(item);
}

void MainWindow::editCapital(Capital *cap)
{
    QList<QListWidgetItem *> list = ui->capitals_widget->selectedItems();
    QListWidgetItem * item = list.at(0);
    QVariant var = item->data(Qt::UserRole);
    Capital *capital = var.value<Capital *>();
    capital->setName(cap->getName());
    capital->setSquare(cap->getSquare());
    capital->setPopulation(cap->getPopulation());
    capital->setLongtitude(cap->getLongtitude());
    capital->setLatitude(cap->getLatitude());
    item->setText(capital->caption());
    ui->name->setText(capital->getName());
    ui->square->setText(QString::number(capital->getSquare()));
    ui->population->setText(QString::number(capital->getPopulation()));
    ui->longtitude->setText(QString::number(capital->getLongtitude()));
    ui->latitude->setText(QString::number(capital->getLatitude()));
}

void MainWindow::on_add_btn_clicked()
{
    AddDialog dialog;
    connect(&dialog, SIGNAL(sendCapital(Capital *)), this, SLOT(getCapital(Capital *)));
    dialog.exec();

}

void MainWindow::on_capitals_widget_itemSelectionChanged()
{
    QList<QListWidgetItem *> list = ui->capitals_widget->selectedItems();
    //ui->name_btn->setText();
    if(list.count() == 0)
    {
        ui->rem_btn->setEnabled(false);
        ui->edit_btn->setEnabled(false);
        ui->name->setText("");
        ui->square->setText("");
        ui->population->setText("");
        ui->longtitude->setText("");
        ui->latitude->setText("");
    }
    else
    {
        QListWidgetItem * item = list.at(0);
        QVariant var = item->data(Qt::UserRole);
        Capital *capital = var.value<Capital *>();
        ui->rem_btn->setEnabled(true);
        ui->edit_btn->setEnabled(true);
        ui->name->setText(capital->getName());
        ui->square->setText(QString::number(capital->getSquare()));
        ui->population->setText(QString::number(capital->getPopulation()));
        ui->longtitude->setText(QString::number(capital->getLongtitude()));
        ui->latitude->setText(QString::number(capital->getLatitude()));
    }
}

void MainWindow::on_rem_btn_clicked()
{
    QList<QListWidgetItem *> list = ui->capitals_widget->selectedItems();
    QListWidgetItem * item = list.at(0);
    int row = ui->capitals_widget->row(item);
    QListWidgetItem* i = ui->capitals_widget->takeItem(row);
    delete i;

}

void MainWindow::on_edit_btn_clicked()
{
    EditDialog dialog;
    connect(this, SIGNAL(sendCapital(Capital *)), &dialog, SLOT(getCapital(Capital *)));
    connect(&dialog, SIGNAL(returnCapital(Capital *)), this, SLOT(editCapital(Capital *)));
    QList<QListWidgetItem *> list = ui->capitals_widget->selectedItems();
    QListWidgetItem * item = list.at(0);
    QVariant var = item->data(Qt::UserRole);
    Capital *capital = var.value<Capital *>();
    emit sendCapital(capital);
    dialog.exec();
}

void MainWindow::on_exec_btn_clicked()
{
    ui->result_widget->setEnabled(true);
    for(int i = 0; i < ui->result_widget->count(); i++)
    {
        QListWidgetItem * item = ui->result_widget->takeItem(i);
        i--;
        delete item;

    }
    float longt = ui->res_doubleSpinBox->value();
    for(int i = 0; i < ui->capitals_widget->count(); i++)
    {
        QListWidgetItem * item = ui->capitals_widget->item(i);
        QVariant var = item->data(Qt::UserRole);
        Capital * cap = var.value<Capital *>();
        if(cap->getLongtitude() > longt)
        {
            QListWidgetItem *it = new QListWidgetItem();
            QVariant var;
            var.setValue(cap);
            it->setData(Qt::UserRole, var);
            it->setText(cap->caption());
            ui->result_widget->addItem(it);
        }
    }
}

std::string fromFileToStr(std::string fileName)
{
    std::ifstream fin(fileName);
    if(!fin.is_open()) {
        std::cout << "Not exist" << std::endl;
    }
    std::string s {std::string(std::istreambuf_iterator<char>(fin), std::istreambuf_iterator<char>())};
    return s;
}

void MainWindow::on_load_btn_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Open"), "", tr("Json file(*.json)"));

    std::string str = fromFileToStr(file.toStdString());

    QJsonParseError err;

    QJsonDocument doc = QJsonDocument::fromJson(
        QByteArray::fromStdString(str),
        &err);

    if (err.error != QJsonParseError::NoError) {
        std::cout << "Json file error" + err.errorString().toStdString();
    }


    QJsonArray capitalsArr = doc.array();

    if(ui->capitals_widget->count() > 0)
    {
        for(int i = 0; i < ui->capitals_widget->count(); i++)
        {
            QListWidgetItem * item = ui->capitals_widget->takeItem(i);
            i--;
            delete item;

        }
    }

    for (int i = 0; i < capitalsArr.size(); i++) {
          QJsonValue value1 = capitalsArr.at(i);
          QJsonObject capObj = value1.toObject();
          Capital *c = new Capital(capObj.value("name").toString(),
                                  capObj.value("square").toInt(),
                                  capObj.value("population").toDouble(),
                                  capObj.value("coordinates").toObject().value("longtitude").toDouble(),
                                  capObj.value("coordinates").toObject().value("longtitude").toDouble());

          QListWidgetItem *it = new QListWidgetItem();
          it->setText(c->caption());
          QVariant var;
          var.setValue(c);
          it->setData(Qt::UserRole, var);
          ui->capitals_widget->addItem(it);
    }

}

void MainWindow::on_save_btn_clicked()
{
    QString file = QFileDialog::getSaveFileName(this, tr("Save file"), "", tr("Json file(*.json)"));

    QJsonDocument doc;

    QJsonArray capitalsArr;
    for (int i = 0; i < ui->capitals_widget->count(); i++) {
        QListWidgetItem *it = ui->capitals_widget->item(i);
        QVariant var = it->data(Qt::UserRole);
        Capital *cap = var.value<Capital *>();
        QJsonObject capObj;
        capObj.insert("name", cap->getName());
        capObj.insert("square", cap->getSquare());
        capObj.insert("population", cap->getPopulation());
        QJsonObject coorObj;
        coorObj.insert("longtitude", cap->getLongtitude());
        coorObj.insert("latitude", cap->getLongtitude());
        capObj.insert("coordinates", coorObj);
        capitalsArr.append(capObj);

    }

    doc.setArray(capitalsArr);

    QString jsonString = doc.toJson();

    std::ofstream F(file.toStdString(), std::ios::out);
    F << jsonString.toStdString();
    F.close();

}

void MainWindow::on_result_widget_itemClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    Capital *capital = var.value<Capital *>();
    ui->name->setText(capital->getName());
    ui->square->setText(QString::number(capital->getSquare()));
    ui->population->setText(QString::number(capital->getPopulation()));
    ui->longtitude->setText(QString::number(capital->getLongtitude()));
    ui->latitude->setText(QString::number(capital->getLatitude()));
}
