#ifndef CAPITAL_H
#define CAPITAL_H

#include <QObject>

class Capital : public QObject
{
    Q_OBJECT
    QString name;
    int square;
    float population;
    float longtitude;
    float latitude;
public:
    QString getName();
    int getSquare();
    float getPopulation();
    float getLongtitude();
    float getLatitude();


    void setName(QString name);
    void setSquare(int square);
    void setPopulation(float pop);
    void setLongtitude(float longt);
    void setLatitude(float lat);


    explicit Capital(QObject *parent = 0);
    explicit Capital(QString name, int square, float population, float longtitude, float latitude, QObject *parent = 0);
    QString caption();

signals:

public slots:
};

#endif // CAPITAL_H
