#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>

class Student : public QObject
{
    Q_OBJECT
public:
    QString name;

    explicit Student(QObject *parent = 0);

signals:

public slots:
};

#endif // STUDENT_H
