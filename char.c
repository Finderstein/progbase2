#include <stdio.h>
#include <progbase.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

typedef struct __Tree Tree;
struct __Tree
{
    void * value;
    int * children;
};

Tree * Tree_new(void * value)
{
    Tree * self = malloc(sizeof(Tree));
    self->value = value;
    self->children = malloc(sizeof(int));
    
    return self;
}
void Tree_free(Tree * self)
{
    free(self->children);
    free(self);
}


int main(void)
{
    Tree * Tree_new(void * value)
{
    Tree * self = malloc(sizeof(Tree));
    self->value = value;
    self->children = List_new();
    
    return self;
}
void Tree_free(Tree * self)
{
    //List_free(self->children);
    free(self);
}
    
    return 0;
}