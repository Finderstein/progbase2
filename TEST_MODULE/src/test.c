#include <stdio.h>
#include "../include/test.h"

int neg(int a)
{
    return a > 0 ? -a : a;
}