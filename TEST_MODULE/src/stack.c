#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../include/stack.h"

struct __Stack 
{
    int *items;
    int length;
    int capacity;
};

Stack * Stack_new(void)
{
    Stack *self = malloc(sizeof(Stack));
    
    self->length = 0;
    self->capacity = 10;

    self->items = malloc(sizeof(int) * self->capacity);

    return self;
}

void Stack_free(Stack * self)
{
    assert(self != NULL);

    free(self->items);
    free(self);
}

void Stack_push(Stack * self, int val)//поставити на вершину
{
    assert(self != NULL);
    assert(self->length <= self->capacity);

    self->items[self->length] = val;
    self->length += 1;
}

int Stack_peek(Stack * self)//повернути значення вершини
{
    assert(self != NULL);

    return self->items[self->length - 1];
}

int Stack_pop(Stack * self)//видалити з вершини
{
    assert(self != NULL);

    int rem = self->items[self->length - 1];
    self->length -= 1;
    return rem;
}

bool Stack_isEmpty(Stack * self)
{
    assert(self != NULL);

    if(self->length == 0) return true;
    else return false;
}