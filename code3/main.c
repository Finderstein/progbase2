#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <list.h>
#include <lexer.h>
#include <string_buffer.h>
#include <bintree.h>

#define foreach(VARTYPE, VARNAME, LIST)   \
for (int VARNAME##_i = 0, VARNAME##_length = List_count(LIST); !VARNAME##_i; ++VARNAME##_i) \
    for (VARTYPE VARNAME = (VARTYPE)List_at(LIST, VARNAME##_i); \
        VARNAME##_i < VARNAME##_length; \
        VARNAME = (VARTYPE)List_at(LIST, ++VARNAME##_i))



void printTokens(List * tokens)
{
    foreach(Token *, token, tokens)
    {
        if(token->type == TokenType_NUMBER)
        {
            printf("<NUM,%s>", token->lexeme);
        }
        else
        {
            printf("<%s>", token->lexeme);
        }
    }
}


int eval(BinTree * node)
{
    Token * token = node->value;
    if(token->type == TokenType_NUMBER)
    {
        int number = atoi(token->lexeme);
        return number;
    }
    else
    {
        int leftVal = eval(node->left);
        int rightVal = eval(node->right);  
        
        switch(token->type)
        {
            case TokenType_PLUS: return leftVal + rightVal;
            case TokenType_MINUS: return leftVal - rightVal;
            case TokenType_MULT: return leftVal * rightVal;
            case TokenType_DIV: return leftVal / rightVal;
            default: return 0;
            
        }
    }
}


int main(void)
{
   const char * fileName = "../main.prog";
   int len = gitFileSize(fileName);
   buffer
    return 0;
}

