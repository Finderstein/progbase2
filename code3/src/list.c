#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <list.h>


List * List_new(void)
{
    List *new = malloc(sizeof(List));
    new->tokens = malloc(sizeof(void *) * 32);
    new->length = 0;
    new->capacity= 32;

    return new;
}

void List_free(List * self)
{
    free(self->tokens);
    free(self);    
}

void List_insert(List * self, void * value, size_t index)
{
    for(int i = self->length; i > index; i--)
    {
        self->tokens[i] = self->tokens[i - 1];
    }
    self->tokens[index] = value;

    return;
}

void List_add(List * self, void * value)
{
    if(self->length == self->capacity)
    {
        self->capacity *= 2;
        self->tokens = realloc(self->tokens, self->capacity * sizeof(void *));
    }

    self->tokens[self->length] = value;
    self->length += 1;
}

void * List_at(List * self, size_t index)
{
    return self->tokens[index];
}

void * List_removeAt(List * self, size_t index)
{
    void *removed = self->tokens[index];
    for(int i = index; i < self->length - 1; i++)
    {
        self->tokens[i] = self->tokens[i + 1]; 
    }
    self->length -= 1;

    return removed;
}

size_t List_count(List * self)
{
    return self->length;
}
